// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
let url = ""
if(window.location.href.includes('172.20.8')){
  url = 'http://172.20.8.69:8081'  

  }else if(window.location.href.includes('dm-pluris')){
      
      url = 'http://backendmondelez.dm-pluris.plurismidia.com.br:8081'  
      
  }else if(window.location.href.includes('localhost')){
      
    url = 'http://localhost:8081'  
  }else{
      url = 'https://backendmondelez.plurismidia.com.br'
  }
 
export const environment = {
  
  production: true,

  //ACESSOS DA APLICAÇÃO CADASTRADOS NO BACKEND (SPRING BOOT)
  //urlBackend: 'http://172.20.8.69:8081', // TESTES NO SERVER
  urlBackend: url,
  //urlBackend: 'http://127.0.0.1:8081', // TESTES LOCAIS
  clientId: 'portal-mondelez',
  clientSecret: 'PLURIS_789orensdhsads_dsdfdz_sd92dx', 
  
  //CHAMADAS (SEVIÇOS BACKEND SPRING BOOT)
  //POST s
    obterTokenURL:  '/oauth/token', //AUTENTICAÇÃO (OATH2 + JWT)
    cadastrarUsuario: '/portalpluris/criarusuario', //CADASTRA FUNCIONARIOS
    login: '/portalpluris/login', 
    postCreateUpdateUsuario : '/portalpluris/createupdateusers', 
    postCreateUpdateTransportadora : '/portalpluris/createupdatetran',
    postCreateUpdateFollowup : '/portalpluris/createupdatefoup',
    postCreateUpdateCliente : '/portalpluris/createupdatecliente',
    postCreateEmbarqueFollowUp : '/portalpluris/createembarquefoup',
    postUpdateLeadtime : '/portalpluris/updateleadtime',
    postCreateUpdateStatus : '/portalpluris/createupdatestatus',

    postCreateUpdateCanal : '/portalpluris/getcreateupdatecanal',

    postCreateUpdateUsuarioCanal : '/portalpluris/postcreatecanalbyusuario',
    createUpdateTipo: '/portalpluris/createupdatetipo',
    createUpdateMotivo: '/portalpluris/createupdatemotivo',
    createUpdateResponsavel: '/portalpluris/createupdateresponsavel',
    updateStatusEmbarque: '/portalpluris/updatestatusembarque',
    
  //GET s
    getLogo: '/portalpluris/getlogoempresa',
    getUsuarios: '/portalpluris/getallusers',
    getTransportadora: '/portalpluris/getlisttransportadora',
    getCliente:  '/portalpluris/getlistcliente',
    getClienteFiltro:  '/portalpluris/getlistclientefiltro',
    getFollowups: '/portalpluris/getlistfoup',
    getLstAgendamento: '/portalpluris/getlstagendamento',
    getAgendamento: '/portalpluris/getagendamento',
    createUpdateAgendamento: '/portalpluris/createupdateagendamento',
    getliststatus: '/portalpluris/getliststatus',
    getListEventosFoup: '/portalpluris/getlisteventosfoup',
    getListEventosFoupByStatus: '/portalpluris/getfoupbystatus',
    getListMotivos: '/portalpluris/getlistmotivos',
    getListResponsavel: '/portalpluris/getlistresponsaveis',
    getListFollowupsEmbarque : '/portalpluris/getembarquefoup',
    getClientesByIdEmbarque : '/portalpluris/getclientebyembarque',
    getEmbarque:'/portalpluris/getshipmentdelivery',
    getListSkus : '/portalpluris/getskuds',
    getGetEmbarqueTransportadoras : '/portalpluris/gettransportadora',
    getGetListContatosTransportadora : '/portalpluris/getlistcontatostransportadora',
    
    getEmbarquesByStatus: '/portalpluris/getembarquebystatus',
    getListStatus: '/portalpluris/getliststatus',
    getListStatusQtd: '/portalpluris/getliststatusqtd',
    setembarqueemuso: '/portalpluris/setembarqueemuso',
    getlistcanal : '/portalpluris/getlistcanal',
    getlstCanalByUsuario : '/portalpluris/getlstcanalbyusuario', 
    buscaListStatus: '/portalpluris/getstatusdelivery',
    getLisTipo: '/portalpluris/getlistipo',
    getLisMotivo: '/portalpluris/getlistmotivos',
    getLisResponsaveis: '/portalpluris/getlistresponsaveis',
    getPDF: '/portalpluris/getpdf',
    enviaEmail: '/portalpluris/enviaemail',
    buscaRelatorio: '/portalpluris/getrelatorio'
    
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
