export class ShipmentModel {

    constructor(
        public delivery_note: number,
        public shipment: number,
        public id_embarque: number,
        public cliente: string,
        public data_agendamento: string
    ){}
}