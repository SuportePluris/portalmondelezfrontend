import { Component, OnInit, Renderer2, ViewChild, ElementRef } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { UsuarioModel } from './usuario.model';
import { UsuarioService } from './usuario.service';
import { ToastrService } from 'ngx-toastr';
import { NotificationService } from '../notification.service';
import { ThemePalette } from '@angular/material/core';
import { MatDialog } from '@angular/material/dialog';
import { CanalModel } from '../canal/canal.model';
import { UsuarioCanalModel } from '../usuario/usuario-canal-model.model'
import { RegiaoModel } from '../regiao/regiao.model'
import { RegiaoService } from '../regiao/regiao.service'
import { RegiaoCanalModel } from './regiao-canal.model'

@Component({
  selector: 'app-usuario',
  templateUrl: './usuario.component.html',
  styleUrls: ['./usuario.component.css'],
  providers: [UsuarioService, RegiaoService]
})
export class UsuarioComponent implements OnInit {

  constructor(
    private toastr: ToastrService,
    private usuariosService: UsuarioService,
    private router: Router,
    private usuaarioService: UsuarioService,
    private notifyService : NotificationService,
    private route: ActivatedRoute, 
    public dialog: MatDialog,
    private clienteService: UsuarioService,
    private regiaoService: RegiaoService,
    private renderer: Renderer2

  ) { 
    
  }

    color: ThemePalette = 'accent';
    checked = false;
    disabled = false;
  
  panelOpenState = false;
  public isPiscar : boolean = false
  public mostrarTabela : boolean = false
  public lstUsuarios: UsuarioModel[] = []
  public loginError : boolean = false
  public msgError : string = "";

  public lstCanalModel : CanalModel[] = [];
  public lstRegiaoModel : RegiaoModel[] = [];

  public formCadastro: FormGroup = new FormGroup({
      'idUsuario': new FormControl(null, null),
      'nome': new FormControl(null, null),
      'login': new FormControl(null, null),
      'senha': new FormControl(null, null),
      'email': new FormControl(null, null),
      'permissao': new FormControl(null, null),
      'inativo': new FormControl(null, null),
      'user_id_cd_canal': new FormControl(null, null),
      'user_id_regi_cd_regiao': new FormControl(null, null)
  })

  public lstRegicaoCanalModel: RegiaoCanalModel[] = [];
  public indice:number = 0;

  ngOnInit(): void {
    this.preencheLista();
    this.preencheComboCanal();
    this.preencheComboRegiao();
  }

  preencheComboCanal(){
    
    this.clienteService.getLstCanal("N").then((retorno:CanalModel[])=>{
       this.lstCanalModel = retorno
    
    }, errorresponse => {
      console.log(errorresponse)
        this.toastr.error('Erro conexao backend\nContate o suporte.','TELA DE CLIENTE');
        this.router.navigate(['']);
    }).catch((retorno:any) => {
      // console.log(retorno);
      if(retorno.error.error_description.indexOf("Access token expired")>-1){
        alert("Sessão Expirada");
        this.router.navigate(['']);
      }               
    });
  }

  preencheComboRegiao(){
    
    this.regiaoService.getLstRegiao("N").then((retorno:RegiaoModel[])=>{
       this.lstRegiaoModel = retorno
    
    }, errorresponse => {
      console.log(errorresponse)
        this.toastr.error('Erro conexao backend\nContate o suporte.','TELA DE CLIENTE');
        this.router.navigate(['']);
    }).catch((retorno:any) => {
      // console.log(retorno);
      if(retorno.error.error_description.indexOf("Access token expired")>-1){
        alert("Sessão Expirada");
        this.router.navigate(['']);
      }               
    });
  }

  public preencheLista():void{
    this.mostrarTabela = false;

    this.usuariosService.getUsers().then((retorno:UsuarioModel[])=>{
        this.mostrarTabela = true;
        this.lstUsuarios = retorno
      
      }).catch((retorno:any) => {
        console.log(retorno);
        if(retorno.error.error_description.indexOf("Access token expired")>-1){
          alert("Sessão Expirada");
          this.router.navigate(['']);
        }               
    });

  }


  public limparCadastro(){
      this.formCadastro.reset()
  }

  openUsuarioCanalDialog(id_usuario : number) {
    const dialogRef = this.dialog.open(UsuarioCanalDialog,{width:'30%',});

     //instanciando parametro para o dialog
     dialogRef.componentInstance.id_usuario = id_usuario+'';

    dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog result: ${result}`);
    });
  }
   

   //PREENCHE NO FORM OS DADOS PARA ATUALIZAR
   public editar( user_id_cd_usuario: string, user_ds_nomeusuario: string, user_ds_login: string,
                  user_ds_senha: string, user_ds_email: string, user_ds_permissionamento: string,
                  user_in_inativo: string, lstRegicaoCanal: RegiaoCanalModel[]
                  ){

                     
        //this.toastr.success('Usuário atualizado.','CADASTRO');

        //this.isPiscar = true;
        // LIMPAR CLASS
        let element = document.getElementById('idBodyCadastro')
        if(!element.classList.contains('fa-blink')){
          element.classList.add('fa-blink'); 
        }else{
          element.classList.remove('fa-blink'); 
          if(!element.classList.contains('fa-blink')){
            element.classList.add('fa-blink'); 
          }else{
            element.classList.remove('fa-blink');  
          }
        }

        
        let inativoAuxiliar:boolean = false ;
        if(user_in_inativo.trim() == "SIM"){
           inativoAuxiliar = true
        }

        this.formCadastro.setValue({
          idUsuario: user_id_cd_usuario.trim(), 
          nome: user_ds_nomeusuario.trim(),
          login: user_ds_login.trim(),
          senha: user_ds_senha.trim(),
          email: user_ds_email.trim(),
          permissao: user_ds_permissionamento.trim(),
          inativo : inativoAuxiliar,
          user_id_cd_canal: "",
          user_id_regi_cd_regiao: +"" // "+string" converte o tipo da variavel de string para numeric
        })

        this.lstRegicaoCanalModel = lstRegicaoCanal
        this.UpdateIndice();
        
        this.toastr.warning('Modo de edição.','CADASTRO');
        
  }


  public criarUsuario(){


    let inativoAux = "N";
    if(this.formCadastro.value.inativo == true){
      inativoAux = "S"
    }
    
    let usuarioModel : UsuarioModel = new UsuarioModel(
      this.formCadastro.value.idUsuario, //user_id_cd_usuario
      "",                                //user_dh_registro
      this.formCadastro.value.nome,      //user_ds_nomeusuario
      this.formCadastro.value.login,     //user_ds_login
      this.formCadastro.value.senha,     //user_ds_senha
      this.formCadastro.value.email,     //user_ds_email
      this.formCadastro.value.permissao, //user_ds_permissionamento
      inativoAux,                               //user_in_inativo
      localStorage.getItem('idFuncionario'), //user_id_cd_usuarioatualizacao
      "",  //user_dh_atualizacao
      this.lstRegicaoCanalModel 
      //this.formCadastro.value.user_id_cd_canal,        //user_id_cd_canal
      //this.formCadastro.value.user_id_regi_cd_regiao  //user_id_regi_cd_regiao
    )

    this.usuariosService.createUpdateUser(usuarioModel).subscribe( response => {
              
      if(response.sucesso == true){
    
        this.preencheLista();
        this.limparCadastro(); 
        this.lstRegicaoCanalModel = []

        this.toastr.success('Sucesso (' + response.processo + ")",'CADASTRO')
        this.msgError='Usuário cadastrado.';
                  
      }else{
        
        if(response.processo == "validacao"){
          this.toastr.error('Erro Usuário não cadastrado\nFavor preencher os campos:\n'+response.msg,'CADASTRO')
          //this.msgError='Erro Usuário não cadastrado\nFavor preencher os campos\n: '+response.msg;
          this.loginError = true;
        }else{
          this.toastr.error('Erro Usuário não cadastrado\n'+response.msg,'CADASTRO')
          //this.msgError='Erro Usuário não cadastrado: '+response.msg;
          this.loginError = true;
        }

      }

    }, errorresponse => {
        this.toastr.error('Erro ao cadastrar Usuário','CADASTRO');
        this.msgError='Erro Usuário não cadastrado';
        this.loginError = true;

    }) 
                    
  }

  
  addElement(){

    let regiaoCanal: RegiaoCanalModel;

    const regiaoModel = this.lstRegiaoModel.find( regiao => regiao.id_regi_cd_regiao === this.formCadastro.value.user_id_regi_cd_regiao);
    
    if(this.formCadastro.value.user_id_cd_canal == "9999"){

      regiaoCanal = new RegiaoCanalModel (this.indice, ""+this.formCadastro.value.user_id_regi_cd_regiao, regiaoModel.regi_ds_regiao, this.formCadastro.value.user_id_cd_canal, "Todos");
    }else {
     
      const canalModel = this.lstCanalModel.find( canal => canal.cana_id_cd_canal === this.formCadastro.value.user_id_cd_canal);

      regiaoCanal = new RegiaoCanalModel (this.indice, ""+this.formCadastro.value.user_id_regi_cd_regiao, regiaoModel.regi_ds_regiao, this.formCadastro.value.user_id_cd_canal, canalModel.cana_ds_canal);
    }
    
    this.lstRegicaoCanalModel.push(regiaoCanal);
    
    ++this.indice;
  }

  rmElement(indice: number){

    this.lstRegicaoCanalModel.splice(indice, 1)

    this.UpdateIndice();
   }

   UpdateIndice(){

    for (let index = 0; index < this.lstRegicaoCanalModel.length; index++) {
      this.lstRegicaoCanalModel[index].indice = index;
    }

    this.indice = this.lstRegicaoCanalModel.length
   }

}


@Component({
  selector: 'dialog-usuario-canal',
  templateUrl: 'usuario-canal-dialog-component.html',
  styleUrls: ['./usuario.component.css'],
  providers : [UsuarioService]
})
export class UsuarioCanalDialog {
  
  constructor(
    private clienteService: UsuarioService,
    private toastr: ToastrService,
    private router: Router,
    private route: ActivatedRoute, 
  ) { }

  public id_usuario : string; //instanciado na chamada do dialog


  public canalmodel : CanalModel = new CanalModel('','','','',);
  public usuariomodel : UsuarioModel = new UsuarioModel('','','','','','','','','','',[]);

  public lstCanalModel : CanalModel[] = [];
  public lstusuariocanalmodel : UsuarioCanalModel[] = [];


  public formCadastro: FormGroup = new FormGroup({
      'idCanal': new FormControl(null, null)      
  })
  
  ngOnInit(): void {
    this.preencheComboCanal();
    this.preencheListaTela();
  }


  
  gravarUsuarioCanal(){
    this.criarUsuarioCanal()
  }


  public criarUsuarioCanal(){
    

    this.canalmodel = new CanalModel(this.formCadastro.value.idCanal,'','','')
    this.usuariomodel = new UsuarioModel(this.id_usuario,'','','','','','','','','', [])
    

    //console.log(localStorage.getItem('idFuncionario'));
    let usuarioCanalModel : UsuarioCanalModel = new UsuarioCanalModel(
      this.usuariomodel, this.canalmodel
    )

    this.clienteService.createUpdateUsuarioCanal(usuarioCanalModel).subscribe( response => {
      
      if(response.sucesso == true){
          
        this.preencheListaTela(); 
        this.preencheComboCanal();
        this.formCadastro.reset();
        
        this.toastr.success('Canal Vinculado ao usuário.','CADASTRO')
        //this.msgError='Usuário cadastrado.';
                  
      }else{
        
        if(response.processo == "validacao"){
          this.toastr.error('Erro Canal não cadastrada\nFavor preencher os campos:\n'+response.msg,'CADASTRO')
         
        }else{
          this.toastr.error('Erro Canal não cadastrada\n'+response.msg,'CADASTRO')
          
        }

      }

    }, errorresponse => {
        this.toastr.error('Falha de comunicação com o Backend ao vincular Canal','CADASTRO');
       ;

    }) 
                    
  }


  preencheListaTela(){
    
    this.clienteService.getListUsuarioCanal(this.id_usuario).then((retorno:UsuarioCanalModel[])=>{
       this.lstusuariocanalmodel = retorno
      
    }, errorresponse => {
      
        this.toastr.error('Erro conexao backend\nContate o suporte.','TELA DE CLIENTE');
        this.router.navigate(['']);
    }).catch((retorno:any) => {
      // console.log(retorno);
      if(retorno.error.error_description.indexOf("Access token expired")>-1){
        alert("Sessão Expirada");
        this.router.navigate(['']);
      }               
  });
  }


  preencheComboCanal(){
    
    this.clienteService.preencheCmbCanal(this.id_usuario).then((retorno:CanalModel[])=>{
       this.lstCanalModel = retorno
    
    }, errorresponse => {
      console.log(errorresponse)
        this.toastr.error('Erro conexao backend\nContate o suporte.','TELA DE CLIENTE');
        this.router.navigate(['']);
    }).catch((retorno:any) => {
      // console.log(retorno);
      if(retorno.error.error_description.indexOf("Access token expired")>-1){
        alert("Sessão Expirada");
        this.router.navigate(['']);
      }               
  });
  }

}


