
import{CanalModel} from '../canal/canal.model'
import{UsuarioModel} from '../usuario/usuario.model'

export class UsuarioCanalModel {

    constructor(
        public csCdtbUsuarioUserBean: UsuarioModel,
        public csCdtbCanalCanaBean:CanalModel
    ){}
}
