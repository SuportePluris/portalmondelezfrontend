import { RegiaoCanalModel } from "./regiao-canal.model";

export class UsuarioModel {

    constructor(
        public user_id_cd_usuario:string,
        public user_dh_registro:string,
        public user_ds_nomeusuario:string,
        public user_ds_login:string,
        public user_ds_senha:string,
        public user_ds_email:string,
        public user_ds_permissionamento:string,
        public user_in_inativo:string,
        public user_id_cd_usuarioatualizacao:string,
        public user_dh_atualizacao:string,
        public lstRegiaoCanal: RegiaoCanalModel[]
    ){}
}
