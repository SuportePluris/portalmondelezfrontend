export class RegiaoCanalModel {

    constructor(
        public indice: number,
        public urcs_id_regi_cd_regiao: string,
        public regi_ds_regiao: string,
        public urcs_id_cd_canal: string,
        public cana_ds_canal: string
    ){}
}