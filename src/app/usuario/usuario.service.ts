import { Injectable } from '@angular/core'
import { environment } from 'src/environments/environment'
import { HttpClient} from '@angular/common/http'
import { Observable } from 'rxjs';
import { UsuarioModel } from './usuario.model';
import { UsuarioCanalModel } from './usuario-canal-model.model';

@Injectable()

export class UsuarioService {

    public dataAtual = new Date();
    private url: string = environment.urlBackend;
    private environmentGetUsers: string = environment.getUsuarios;
    private environmentPostCreateUpdatePessoa: string = environment.postCreateUpdateUsuario;
    private environmentGetListUsuarioCanal: string = environment.getlstCanalByUsuario;
    private environmentGetListCanal: string = environment.getlistcanal;
    private environmentPostCreateUpdateUsuarioCanal: string = environment.postCreateUpdateUsuarioCanal;
    
    
    

    constructor(
        private http: HttpClient
    ){ }

    public getUsers():Promise<any> {

        const tokenString = localStorage.getItem('access_token');
        const token = JSON.parse(tokenString);

        const headers = {
            'Authorization' : 'Bearer ' + token.access_token,
            'Content-Type' : 'application/json'
        }

        return this.http.get(this.url +this.environmentGetUsers, { headers : headers})
        .toPromise().then((resposta:any) => resposta);
        
    }

   
    public createUpdateUser(usuario : UsuarioModel):Observable<any> {
        
        const tokenString = localStorage.getItem('access_token');
        const token = JSON.parse(tokenString);

        let json = { 
                    "user_id_cd_usuario" : usuario.user_id_cd_usuario,
                    "user_dh_registro" : usuario.user_dh_registro,
                    "user_ds_nomeusuario" : usuario.user_ds_nomeusuario,
                    "user_ds_login" : usuario.user_ds_login,
                    "user_ds_senha" : usuario.user_ds_senha,
                    "user_ds_email" : usuario.user_ds_email,
                    "user_ds_permissionamento" : usuario.user_ds_permissionamento,
                    "user_in_inativo" : usuario.user_in_inativo,
                    "user_id_cd_usuarioatualizacao" : usuario.user_id_cd_usuarioatualizacao,
                    "user_dh_atualizacao" : usuario.user_dh_atualizacao,
                    "lstRegiaoCanal": usuario.lstRegiaoCanal
                }


        const headers = {
            'Authorization' : 'Bearer ' + token.access_token,
            'Content-Type' : 'application/json'
        }
        
        return this.http.post(this.url + this.environmentPostCreateUpdatePessoa, json, { headers : headers});
    }

    
    public createUpdateUsuarioCanal(usuarioCanal : UsuarioCanalModel):Observable<any> {
        
        const tokenString = localStorage.getItem('access_token');
        const token = JSON.parse(tokenString);

        console.log(usuarioCanal)

        let json = { 
            'csCdtbUsuarioUserBean' : usuarioCanal.csCdtbUsuarioUserBean, 
            'csCdtbCanalCanaBean' : usuarioCanal.csCdtbCanalCanaBean
        }
        console.log(json)

        const headers = {
            'Authorization' : 'Bearer ' + token.access_token,
            'Content-Type' : 'application/json'
        }
        
        return this.http.post(this.url + this.environmentPostCreateUpdateUsuarioCanal, json, { headers : headers});
    }


    
    public getListUsuarioCanal(idUsuario : string):Promise<any> {

        const tokenString = localStorage.getItem('access_token');
        const token = JSON.parse(tokenString);

        const headers = {
            'Authorization' : 'Bearer ' + token.access_token,
            'Content-Type' : 'application/json'
        }

        return this.http.get(this.url +this.environmentGetListUsuarioCanal +"/"+idUsuario, { headers : headers})
        .toPromise().then((resposta:any) => resposta);
        
    }



    
    public preencheCmbCanal(id_usuario : string):Promise<any> {

        const tokenString = localStorage.getItem('access_token');
        const token = JSON.parse(tokenString);

        const headers = {
            'Authorization' : 'Bearer ' + token.access_token,
            'Content-Type' : 'application/json'
        }
        
        return this.http.get(this.url +this.environmentGetListCanal+"/N/"+id_usuario, { headers : headers})
        .toPromise().then((resposta:any) => resposta);
        
    }

    public getLstCanal(inativo : string):Promise<any> {

        const tokenString = localStorage.getItem('access_token');
        const token = JSON.parse(tokenString);

        const headers = {
            'Authorization' : 'Bearer ' + token.access_token,
            'Content-Type' : 'application/json'
        }
        
        return this.http.get(this.url +this.environmentGetListCanal+"/"+inativo, { headers : headers})
        .toPromise().then((resposta:any) => resposta);
        
    }

    
}   
