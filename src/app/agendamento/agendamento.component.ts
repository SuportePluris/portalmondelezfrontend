import { Component, OnInit, Inject, OnDestroy, HostListener } from '@angular/core';
import { ShipmentModel } from '../shipment.model';
import { ActivatedRoute } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AgendamentoService } from './agendamento.service'
import { Router } from '@angular/router';
import { AgendaModel } from './agenda.model'
import { StatusService } from '../status/status.service'
import { StatusModel } from '../status/status.model'
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { EventoModel } from './evento.model';
import { MotivoModel } from './motivo.model';
import { ResponsavelModel } from './responsavel.model';
import { ListaEmbarqueFollowupModel } from './list-embarque-follow-up.model';
import { ClienteModel } from '../cliente/cliente.model';
import { ContatoModel } from '../cliente/contato.model';
import { SkuModel } from './sku.model';
import { NotasModel } from './notas.model';

import { ContatosTransportadoraModel } from './contato.transportadora.model';



import { ClienteService } from '../cliente/cliente.service';
import { ToastrService } from 'ngx-toastr';
import { EmbarqueFollowUpModel } from './embarque-follow-up.model';
import { throwToolbarMixedModesError } from '@angular/material/toolbar';
import { DeliveryModel } from './delivery.model'
import { EmbarqueModel } from './embarque.model'
import{RetornoShipmentBean} from './retorno-shipment-bean.model'
import{EmbarqueAgendamentoBean} from './embarque-agendamento-bean.model'
import{RetornoDeliveryBean} from './retorno-delivery-bean.model'
import{DetalhesEmailCompletoBean} from './detalhes-email-completo-bean'

 
import { DatePipe } from '@angular/common'
import { TransportadoraModel } from '../transportadora/transportadora.model';
import { TransportadoraService } from '../transportadora/transportadora.service';
import { LiteralMapEntry } from '@angular/compiler/src/output/output_ast';
import { DomSanitizer } from '@angular/platform-browser';


@Component({
  selector: 'app-agendamento',
  templateUrl: './agendamento.component.html',
  styleUrls: ['./agendamento.component.css'],
  providers: [AgendamentoService]
})



export class AgendamentoComponent implements OnInit, OnDestroy {
  
  
  public mostrarLoading: boolean = false
  public id_embarque:number
  public panelOpenState = false;
  public dadosShipment:any = {shipment: 1234567, peso_total: 120.50, cubagem_total: 53.3, valor_total: 'R$1000,00', volume_total: 67}
  public lstAgendaModel: AgendaModel[]
  public agendaModel: AgendaModel
  public mostrarShipment: boolean = false
  public dadosDelivery: DeliveryModel = new DeliveryModel('','','','','','','','','','','');
  public deliverys: DeliveryModel[] = []
  public urlDownload: any
  public layoutHtml: any

  public listDetalhesDoEmbarque:  EmbarqueAgendamentoBean[] = []
  public listDetalhesDoDelivery:  RetornoDeliveryBean[] = []

  public completoBean : DetalhesEmailCompletoBean = new DetalhesEmailCompletoBean('',
                                                                                    new RetornoShipmentBean('','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',''),
                                                                                    this.listDetalhesDoEmbarque,
                                                                                    this.listDetalhesDoDelivery
                                                                                    );

  public embarque: EmbarqueModel = new EmbarqueModel('','','','','','','','','','','','','','','','','','',[], '', '');
  constructor(
    private router: Router,
    private route: ActivatedRoute, 
    private toastr: ToastrService,
    public dialog: MatDialog,
    private agendamentoService: AgendamentoService,
    private sanitizer: DomSanitizer
    ) { }
  

  public listEmbarqueFollowup :  ListaEmbarqueFollowupModel[];
  // @HostListener('window:unload', [ '$event' ])
  // unloadHandler(event) {
  //   this.setEmbarqueEmuso(this.id_embarque, 'N')
  // }
  public nmFile: string;
  

  ngOnInit(): void {

    

     this.embarque = new EmbarqueModel('','','','','','','','','','','','','','','','','', '',[], '', '');
    // //console.log(this.embarque)

    this.route.params.subscribe((parametro: any)=>{
      
      this.id_embarque = parametro.id_embarque
      sessionStorage.setItem('id_embarque', parametro.id_embarque)

      
      this.preencheListaFollowups(this.id_embarque);

      this.carregarEmbarque(this.id_embarque);

      this.preencheListaAgendamento();
      //this.setEmbarqueEmuso(this.id_embarque, 'S')

      //this.getPDF(this.id_embarque);

    })
    
  }




  public enviaEmail(destinatario: string){
    
    this.mostrarLoading = true;

    this.agendamentoService.enviaEmail(destinatario, this.id_embarque).subscribe( response => {
      if(response.sucesso){
        this.toastr.success('Envio realizado', 'E-mail')
      }else{
        this.toastr.error('Erro ao enviar e-mail', 'E-mail')
      }
      this.mostrarLoading = false;
      
    }, errorresponse => {
      //console.log(errorresponse)
        this.toastr.error('Erro conexao backend\nContate o suporte.','ENVIAR EMAIL');
        this.mostrarLoading = false;
    })

    
  }

  public getPDF(id_embarque: number){

    this.nmFile = "Embarque "+id_embarque+".pdf";

    this.agendamentoService.getPdf(id_embarque).then((response:any)=>{

      const blob = this.b64toBlobs(response.bytes);
      let fileUrl = this.sanitizer.bypassSecurityTrustResourceUrl(window.URL.createObjectURL(blob));

      this.urlDownload = fileUrl;

      })
  }

  
  public getCopyPast(id_embarque: number){

      this.agendamentoService.getCopyPast(id_embarque).then((response:any)=>{
          this.layoutHtml = response.layoutEmail;
          this.completoBean = response;
          this.openDialogCopyPastDetalhes(this.layoutHtml, this.completoBean); 
      })

  }

  public b64toBlobs = (b64Data, contentType='', sliceSize=512) => {
    const byteCharacters = atob(b64Data);
    const byteArrays = [];
 
    for (let offset = 0; offset < byteCharacters.length; offset += sliceSize) {
        const slice = byteCharacters.slice(offset, offset + sliceSize);
 
        const byteNumbers = new Array(slice.length);
        for (let i = 0; i < slice.length; i++) {
            byteNumbers[i] = slice.charCodeAt(i);
        }
 
        const byteArray = new Uint8Array(byteNumbers);
        byteArrays.push(byteArray);
    }
 
    const blob = new Blob(byteArrays, {type: contentType});
    return blob;
  }

  ngOnDestroy():void {
    //this.setEmbarqueEmuso(this.id_embarque, 'N')
  }

  public setEmbarqueEmuso(id_embarque: number, in_uso:string){
    this.agendamentoService.setAgendamentoInUso(id_embarque, in_uso).then((response:any)=>{
    })
  }

  public carregarEmbarque(id_embarque:number){
    
    //chama o service para buscar os agendamentos
    this.agendamentoService.getEmbarque(id_embarque).then((response:EmbarqueModel)=>{
      
    this.embarque = response
    //console.log( "EMBARQUE: "+id_embarque)
    this.deliverys= response["retDelivery"]
    this.mostrarShipment = true
    })
  }

  
  openSkus(idDelivery:string){
    this.openFoupDetalheSkuDialog(idDelivery);
  }

  openNotas(skud_ds_pedidomondelez:string){
    this.openDetalheNotasDialog(skud_ds_pedidomondelez);
  }

  showDialogDetalheFollowUp(objFollowUp :ListaEmbarqueFollowupModel){
    this.openFoupDetalheHistDialog(objFollowUp);
    
  }


  public preencheListaFollowups(id_embarque: number ):void{
      
    this.agendamentoService.getListFollowupsEmbarque(id_embarque).then((retorno:ListaEmbarqueFollowupModel[])=>{
      this.listEmbarqueFollowup = retorno

      }, errorresponse => {
        //console.log(errorresponse)
          this.toastr.error('Erro conexao backend\nContate o suporte.','LISTA DE FOLLOWUP');
      
      }).catch((retorno:any) => {
        if(retorno.error.error_description.indexOf("Access token expired")>-1){
          alert("Sessão Expirada");
          this.router.navigate(['']);
        }               
    });

  }
  
  openFoupDialog(id_embarque:number, id_status:string) {

    const dialogRef = this.dialog.open(FoupDialog);

    //instanciando parametro para o dialog
    dialogRef.componentInstance.id_embarque = id_embarque+'';
    dialogRef.componentInstance.id_status = id_status+'';

    dialogRef.afterClosed().subscribe(result => {
      this.ngOnInit()
    });

  }


  openClienteDialog(id_embarque : number) {
    const dialogRef = this.dialog.open(ClienteDialog,{width:'80%',});

     //instanciando parametro para o dialog
     dialogRef.componentInstance.id_embarque = id_embarque+'';

    dialogRef.afterClosed().subscribe(result => {
      //console.log(`Dialog result: ${result}`);
    });
  }
  
  
  openTransportadoraDialog(idTansportadora : string) {
    const dialogRef = this.dialog.open(TransportadoraDialog,{width:'25%',});

     //instanciando parametro para o dialog
     dialogRef.componentInstance.idTansportadora = idTansportadora+'';

    dialogRef.afterClosed().subscribe(result => {
      //console.log(`Dialog result: ${result}`);
    });
  }




  

  openFoupHistDialog() {
    const dialogRef = this.dialog.open(FoupHistDialog,{
      width:'60%',
    });

    dialogRef.afterClosed().subscribe(() => this.ngOnInit());
  }

  openAgendaDialog(daag_id_cd_agendamento:string) {
    
    const dialogRef = this.dialog.open(AgendaDialog,{
      width:'30%',
    });

    dialogRef.componentInstance.daag_id_cd_agendamento = daag_id_cd_agendamento;
    
    dialogRef.afterClosed().subscribe(() => this.ngOnInit());

  }

  openStatusDialog(id_embarque:number) {
    
    const dialogRef = this.dialog.open(StatusDialog,{
      width:'30%',
    });
    dialogRef.componentInstance.id_embarque = id_embarque;
    
    dialogRef.afterClosed().subscribe(() => this.ngOnInit());

  }

  

  public preencheListaAgendamento():void{

    this.agendamentoService.getLstAgendamento(this.id_embarque).then((retorno:AgendaModel[])=>{
    this.lstAgendaModel = retorno;

    }, errorresponse => {
      //console.log(errorresponse)
        this.toastr.error('Erro conexao backend\nContate o suporte.','LISTA DE AGENDA');
    
    }).catch((retorno:any) => {
      //console.log(retorno);
      if(retorno.error.error_description.indexOf("Access token expired")>-1){
        alert("Sessão Expirada");
        this.router.navigate(['']);
      }  
      
    });
  }



  openFoupDetalheHistDialog(followup : ListaEmbarqueFollowupModel ) {
    const dialogRef = this.dialog.open(FoupDetalhearDialog,{
      width:'25%',
    });

    dialogRef.componentInstance.itemFollowup = followup;

    dialogRef.afterClosed().subscribe(result => {
      //console.log(`Dialog result: ${result}`);
    });
  }

  openAdiarLeadTimeDialog(id_embarque: string, emba_dh_leadtime: string){
    const dialogRef = this.dialog.open(AdiarLeadTimeComponent,{width:'40%',});

     //instanciando parametro para o dialog
     dialogRef.componentInstance.id_embarque = id_embarque;
     dialogRef.componentInstance.emba_dh_leadtime = emba_dh_leadtime;

     dialogRef.afterClosed().subscribe(result => {
      this.ngOnInit();
    });
  }

  openEmailDialog(id_embarque: number){
    const dialogRef = this.dialog.open(EmailDialog,{width:'70%',});

     //instanciando parametro para o dialog
     dialogRef.componentInstance.id_embarque = id_embarque;
     
     dialogRef.afterClosed().subscribe(result => {
      this.ngOnInit();
    });
  }
  
  openFoupDetalheSkuDialog(idDelivery : string ) {
    const dialogRef = this.dialog.open(SkuDetalhearDialog,{
      width:'60%',
    });

    dialogRef.componentInstance.idDelivery = idDelivery;

    dialogRef.afterClosed().subscribe(result => {
      //console.log(`Dialog result: ${result}`);
    });
  }

  openDetalheNotasDialog(skud_ds_pedidomondelez: string){
    const dialogRef = this.dialog.open(notasDetalhesDialog,{
      width:'60%',
    });

    dialogRef.componentInstance.skud_ds_pedidomondelez = skud_ds_pedidomondelez;

    dialogRef.afterClosed().subscribe(result => {
      //console.log(`Dialog result: ${result}`);
    });
  }

  openDialogCopyPastDetalhes(layoutHtml : string , completoBean : DetalhesEmailCompletoBean) {
    const dialogRef = this.dialog.open(CopyPastDialog,{
      width:'75%',
    });

    dialogRef.componentInstance.layoutHtml = layoutHtml;
    dialogRef.componentInstance.completoBean = completoBean;
    
    console.log(dialogRef.componentInstance.completoBean);

    dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog result: ${result}`);
    });
  }


}



@Component({
  selector: 'dialog-copypast',
  templateUrl: 'copyPast-dialog-component.html',
  styleUrls: ['./agendamento.component.css']
})

export class CopyPastDialog {

  public embarque: EmbarqueModel = new EmbarqueModel('','','','','','','','','','','','','','','','','','',[], '', '');

  constructor(
    private toastr: ToastrService,
    private router: Router
  ){}

  public layoutHtml : string;
  public completoBean : DetalhesEmailCompletoBean;
  
  ngOnInit(): void{
    
  }

}


@Component({
  selector: 'dialog-embarque-transportadora',
  templateUrl: 'transportadora-dialog-component.html',
  styleUrls: ['./agendamento.component.css'],
  providers : [TransportadoraService]
})
export class TransportadoraDialog {

  constructor(
    private transportadoraService: TransportadoraService,
    private toastr: ToastrService,
    private router: Router,
    private route: ActivatedRoute, 
  ) { }

  public idTansportadora : string; //instanciado na chamada do dialog

  public transportadora : TransportadoraModel = new TransportadoraModel('','','','','','','','')
  
  public listaContatosTransportadora :  ContatosTransportadoraModel[] = [];
  

  ngOnInit(): void {
    this.preencheTela();
    this.preencheLista();
  }


  preencheTela(){
    
    this.transportadoraService.getEmbarqueTransportadora(this.idTansportadora).then((retorno:TransportadoraModel)=>{
       this.transportadora = retorno

      //console.log(this.transportadora)

      }, errorresponse => {
      //console.log(errorresponse)
        this.toastr.error('Erro conexao backend\nContate o suporte.','TELA DE CLIENTE');
        
    }).catch((retorno:any) => {
      // //console.log(retorno);
      if(retorno.error.error_description.indexOf("Access token expired")>-1){
        alert("Sessão Expirada");
        this.router.navigate(['']);
      }               
  });
  }

  preencheLista(){
    
    this.transportadoraService.getListContatosByCodTransportadora(this.idTansportadora).then((retorno:ContatosTransportadoraModel[])=>{
       this.listaContatosTransportadora = retorno

      //console.log(this.transportadora)

     }, errorresponse => {
      //console.log(errorresponse)
        this.toastr.error('Erro conexao backend\nContate o suporte.','TELA DE CLIENTE');
        this.router.navigate(['']);
    }).catch((retorno:any) => {
      // //console.log(retorno);
      if(retorno.error.error_description.indexOf("Access token expired")>-1){
        alert("Sessão Expirada");
        this.router.navigate(['']);
      }               
  });
  }


}






@Component({
  selector: 'dialog-cliente',
  templateUrl: 'cliente-dialog-component.html',
  styleUrls: ['./agendamento.component.css'],
  providers : [AgendamentoService, ClienteService]
})
export class ClienteDialog {


  
  constructor(
    private clienteService: ClienteService,
    private toastr: ToastrService,
    private router: Router,
    private route: ActivatedRoute, 
  ) { }

  public contato : ContatoModel = new ContatoModel('','','','','','','','','','','','','','','','','')

  public id_embarque : string; //instanciado na chamada do dialog
  public formCliente : ClienteModel = new ClienteModel(
    '','','','','','','','','','','','','','','','','','','','',
    '','','','','','','','','','','','','','','','','','','','',
    '','','',this.contato);
  
  public lstContatos : ContatoModel[] = []

  ngOnInit(): void {
    this.preencheTela();
  }


  preencheTela(){
    
    this.clienteService.getClientePorIdEmbarque(this.id_embarque).then((retorno:ClienteModel)=>{
       this.formCliente = retorno
       this.lstContatos.push(retorno.csCdtbContatoclienteCoclBean)
       
       //console.log(this.lstContatos[0])
    
    }, errorresponse => {
      //console.log(errorresponse)
        this.toastr.error('Erro conexao backend\nContate o suporte.','TELA DE CLIENTE');
        this.router.navigate(['']);
    }).catch((retorno:any) => {
      // //console.log(retorno);
      if(retorno.error.error_description.indexOf("Access token expired")>-1){
        alert("Sessão Expirada");
        this.router.navigate(['']);
      }               
  });
  }


}





@Component({
  selector: 'dialog-detalhes-foup',
  
  templateUrl: 'foup-detalhar-dialog-component.html',
  styleUrls: ['./agendamento.component.css'],
  providers : [AgendamentoService]
})
export class FoupDetalhearDialog implements OnInit {

  constructor(
    private toastr: ToastrService,
    private router: Router,
    private route: ActivatedRoute, 
  ) { }

  public itemFollowup :  ListaEmbarqueFollowupModel = new ListaEmbarqueFollowupModel('','','','', '','','','','','','','');

  ngOnInit(): void {
    
  }

}

@Component({
  selector: 'dialog-adiar-lead-time',
  templateUrl: 'adiar-leadtime-component.html',
  styleUrls: ['./agendamento.component.css'],
  providers : [AgendamentoService, DatePipe]
})

export class AdiarLeadTimeComponent implements OnInit {
  public id_embarque: string;
  public emba_dh_leadtime: string
  public leadtime_convertido: Date;

  constructor(
    private agendamentoService: AgendamentoService, 
    public dialogRef: MatDialogRef<AgendaDialog>,
    private datePipe: DatePipe,
    private toastr: ToastrService){
      const currentYear = new Date().getFullYear();
      
    }

  

  public formAdiarLT: FormGroup = new FormGroup({
    'nova_data_lead_time': new FormControl(null, Validators.required)
  })

  ngOnInit(): void {
    this.leadtime_convertido = new Date(this.datePipe.transform(this.formataStringData(this.emba_dh_leadtime),"yyyy-MM-dd"))
  }

  public formataStringData(data: string): string {
    var dia  = data.split("/")[0];
    var mes  = data.split("/")[1];
    var ano  = data.split("/")[2];
  
    return ano + '-' + ("0"+mes).slice(-2) + '-' + ("0"+dia).slice(-2);
    // Utilizo o .slice(-2) para garantir o formato com 2 digitos.
  }
  


  public atualizarLeadtime(){
    if(this.formAdiarLT.get('nova_data_lead_time').value == null){
      this.toastr.error('Preencha uma nova data de Lead Time','Campos Inválidos')
    }else{
      let novo_lead_time = new Date (this.formAdiarLT.get('nova_data_lead_time').value)
      this.agendamentoService.updateLeadtime(this.datePipe.transform(novo_lead_time,"yyyy-MM-dd"), this.id_embarque).subscribe( response => {
        if(response.sucesso){
          this.toastr.success('Lead Time atualizado!', 'UPDATE')
        }else{
          this.toastr.error('Lead Time não atualizado!', 'UPDATE')
        }
      }, errorresponse => {
        //console.log(errorresponse)
          this.toastr.error('Erro conexao backend\nContate o suporte.','ATUALIZAR LEADTIME');
      
      })
    }

    
    
  }
}



@Component({
  selector: 'dialog-detalhes-sku',
  templateUrl: 'sku-detalhar-dialog-component.html',
  styleUrls: ['./agendamento.component.css'],
  providers : [AgendamentoService]
})
export class SkuDetalhearDialog implements OnInit {

  constructor(
    private agendamentoService: AgendamentoService,
    private toastr: ToastrService,
    private router: Router,
    private route: ActivatedRoute, 
  ) { }

  public idDelivery :  string;
  public listSkus :  SkuModel[] = [];
  

  ngOnInit(): void {
    this.preencheListaSkus();
  }

  public preencheListaSkus():void{
        
    this.agendamentoService.getlistSkusAgendamento(this.idDelivery).then((retorno:SkuModel[])=>{
      this.listSkus = retorno
     
      }, errorresponse => {
        //console.log(errorresponse)
          this.toastr.error('Erro conexao backend\nContate o suporte.','LISTA SKU');
      
      }).catch((retorno:any) => {
        //console.log(retorno);
        if(retorno.error.error_description.indexOf("Access token expired")>-1){
          alert("Sessão Expirada");
          this.router.navigate(['']);
        }               
    });

  }

}

@Component({
  selector: 'dialog-detalhes-notas',
  templateUrl: 'notas-detalhar-dialog-component.html',
  styleUrls: ['./agendamento.component.css'],
  providers : [AgendamentoService]
})
export class notasDetalhesDialog implements OnInit {

  constructor(
    private agendamentoService: AgendamentoService,
    private toastr: ToastrService,
    private router: Router,
    private route: ActivatedRoute, 
  ) { }

  public skud_ds_pedidomondelez :  string;
  public listNotas :  NotasModel[] = [];
  

  ngOnInit(): void {
    this.preencheListaNotas();
  }

  public preencheListaNotas():void{
        
    this.agendamentoService.getlistNotas(this.skud_ds_pedidomondelez).then((retorno:NotasModel[])=>{
      this.listNotas = retorno
     
      }, errorresponse => {
        //console.log(errorresponse)
          this.toastr.error('Erro conexao backend\nContate o suporte.','LISTA NOTAS');
      
      }).catch((retorno:any) => {
        //console.log(retorno);
        if(retorno.error.error_description.indexOf("Access token expired")>-1){
          alert("Sessão Expirada");
          this.router.navigate(['']);
        }               
    });

  }

}


/****** COMPONENTE POPUP GRAVAR FOLLOWUP *******/
@Component({
  selector: 'dialog-foup',
  templateUrl: 'foup-dialog-component.html',
  styleUrls: ['./agendamento.component.css'],
  providers : [AgendamentoService]
})
export class FoupDialog implements OnInit {

  constructor(
    private agendamentoService: AgendamentoService,
    private toastr: ToastrService,
    private router: Router,
    private route: ActivatedRoute, 
  ) { }

  public cmbevento: EventoModel[] = [];
  public cmbMotivo: MotivoModel[] = [];
  public cmbResponsavel: ResponsavelModel[] = [];
  public listEmbarqueFollowup :  EmbarqueFollowUpModel[] = [];
  public id_embarque : string; //instanciado na chamada do dialog
  public id_status: string; //instanciado na chamada do dialog
  
  public formFollowUp: FormGroup = new FormGroup({
      'idEventoFollowup': new FormControl(null, null),
      'idMotivo': new FormControl(null, null),
      'idResponsavel': new FormControl(null, null),
      'tx_followup': new FormControl(null, null)            
  })


  ngOnInit(): void {
    
    this.preencheComboEvento();
    this.preencheComboMotivo();
    this.preencheComboResponsavel();
  }

  
  public preencheComboEvento():void{
        //console.log(this.id_status)
    this.agendamentoService.getcmbEventoFollowByStatus(this.id_status).then((retorno:EventoModel[])=>{
        this.cmbevento = retorno
       
      }, errorresponse => {
        //console.log(errorresponse)
          this.toastr.error('Erro conexao backend\nContate o suporte.','COMBO EVENTO');
      
      }).catch((retorno:any) => {
        //console.log(retorno);
        if(retorno.error.error_description.indexOf("Access token expired")>-1){
          alert("Sessão Expirada");
          this.router.navigate(['']);
        }               
    });

  }


  public preencheComboMotivo():void{
        
    this.agendamentoService.getcmbMotivo("2", "N").then((retorno:MotivoModel[])=>{//2 = MOTIVO FOLLOWUP
        this.cmbMotivo = retorno
        
      }, errorresponse => {
        //console.log(errorresponse)
          this.toastr.error('Erro conexao backend\nContate o suporte.','COMBO MOTIVO');
      
      }).catch((retorno:any) => {
        //console.log(retorno);
        if(retorno.error.error_description.indexOf("Access token expired")>-1){
          alert("Sessão Expirada");
          this.router.navigate(['']);
        }               
    });

  }

  public preencheComboResponsavel():void{
        
    this.agendamentoService.getcmbResponsavel().then((retorno:ResponsavelModel[])=>{
        this.cmbResponsavel = retorno

      }, errorresponse => {
        //console.log(errorresponse)
          this.toastr.error('Erro conexao backend\nContate o suporte.','COMBO RESPONSÁVEL');
      
      }).catch((retorno:any) => {
        //console.log(retorno);
        if(retorno.error.error_description.indexOf("Access token expired")>-1){
          alert("Sessão Expirada");
          this.router.navigate(['']);
        }               
    });

  }

  
 

  
  public gravarEmbarqueFollowUp(){
  //  alert(this.formFollowUp.value.tx_followup);
    ////console.log(localStorage.getItem('idFuncionario'));
    let followupModel : EmbarqueFollowUpModel = new EmbarqueFollowUpModel(
      "0",
      this.id_embarque,
      this.formFollowUp.value.idEventoFollowup,
      this.formFollowUp.value.idMotivo,
      this.formFollowUp.value.idResponsavel,
      "",
      "",
      "",
      localStorage.getItem('idFuncionario'),
      this.formFollowUp.value.tx_followup
    )


    this.agendamentoService.createEmbarqueFollowUp(followupModel).subscribe( response => {
      
      if(response.sucesso == true){
        //this.preencheListaFollowup();

        this.toastr.success('Followup cadastrado.','CADASTRO')
        //this.msgError='Usuário cadastrado.';
        this.limparCadastro(); 

      }else{
        
        if(response.processo == "validacao"){
          this.toastr.error('Erro Followup não cadastrado\nFavor preencher os campos:\n'+response.msg,'CADASTRO')
          //this.msgError='Erro Usuário não cadastrado\nFavor preencher os campos\n: '+response.msg;
         
        }else{
          this.toastr.error('Erro Followup não cadastrado\n'+response.msg,'CADASTRO')
          //this.msgError='Erro Usuário não cadastrado: '+response.msg;
         
        }

      }

    }, errorresponse => {
      //console.log(errorresponse)
        this.toastr.error('Erro conexao backend\nContate o suporte.','CRIAR FOLLOWUP');
        //this.msgError='Erro Usuário não cadastrado';
       
    }) 
      

  }

  public limparCadastro(){
    this.formFollowUp.reset()
 }


}






@Component({
  selector: 'dialog-foup-hist',
  templateUrl: 'foup-hist-dialog-component.html',
  styleUrls: ['./agendamento.component.css']
})
export class FoupHistDialog {}


@Component({
  selector: 'dialog-status',
  templateUrl: 'status-dialog-component.html',
  styleUrls: ['./agendamento.component.css'],
  providers: [AgendamentoService, StatusService]
})

export class StatusDialog {
  public id_embarque : number; //instanciado na chamada do dialog
  public dialogRef: MatDialogRef<AgendaDialog>
  public lstStatusModel: StatusModel[] = []
  constructor(
    private toastr: ToastrService,
    private router: Router,
    private route: ActivatedRoute,
    private agendamentoService: AgendamentoService,
    private statusService: StatusService
  ){}

  public formAlterarStatus: FormGroup = new FormGroup({
    'status': new FormControl(null, Validators.required)
  })

  ngOnInit(): void{
    this.getListStatus()
  }

  public atualizarStatus(){
    this.agendamentoService.updateStatus(this.formAlterarStatus.get('status').value, this.id_embarque).subscribe(response=>{
      if(response.sucesso){
        this.toastr.success(response.msg, response.processo)
      }else{
        this.toastr.error(response.msg, response.processo)
      }
    })
  }

  public getListStatus(){

    this.statusService.getListStatus('N').then((retorno:StatusModel[])=>{
      this.lstStatusModel = retorno;
      //console.log(this.lstStatusModel)
   
    }, errorresponse => {
      //console.log(errorresponse)
        this.toastr.error('Erro conexao backend\nContate o suporte.','COMBO STATUS');
    
    }).catch((retorno:any) => {
      if(retorno.error.error_description.indexOf("Access token expired")>-1){
        alert("Sessão Expirada");
        this.router.navigate(['']);
      }               
    });

  }

}

@Component({
  selector: 'dialog-email',
  templateUrl: 'email-dialog-component.html',
  styleUrls: ['./agendamento.component.css'],
  providers: [AgendamentoService, StatusService]
})

export class EmailDialog{
  public id_embarque : number; //instanciado na chamada do dialog
  public formalizarPara: string = ""
  public contatos: string = ""
  public clienteSelecionado: boolean = false
  public transpSelecionado: boolean = false

  public formEmails: FormGroup = new FormGroup({
    'emails': new FormControl(this.contatos, Validators.required),
  })

  constructor(
    private router: Router,
    private agendamentoService: AgendamentoService,
    private statusService: StatusService,
    private toastr: ToastrService,
    private route: ActivatedRoute,
    public dialogRef: MatDialogRef<AgendaDialog> 
    ) { }

    ngOnInit(): void{}

    public EnviarEmail(): void{
      console.log('oi')
    }

    public buscarEmailsTransportadora(){
      if(this.formalizarPara == "Cliente"){
        this.contatos = ""
      }
      this.formalizarPara = "Transportadora"
      this.clienteSelecionado = false
      this.transpSelecionado = true
      
      this.agendamentoService.getContatosTransportadora(this.id_embarque).then((retorno: any)=>{
        if(this.contatos == ""){
          this.contatos += retorno.contatos
        }else{
          this.contatos += "," + retorno.contatos
        }

        this.formEmails.get("emails").setValue(this.contatos)
      })
    }

    public buscarEmailsCliente(){
      this.clienteSelecionado = true
      this.transpSelecionado = false
      
      if(this.formalizarPara == "Transportadora"){
        this.contatos = ""
      }
      
      this.formalizarPara = "Cliente"

      

      this.agendamentoService.getContatosCliente(this.id_embarque).then((retorno: any)=>{
        if(this.contatos == ""){
          this.contatos += retorno.contatos
        }else{
          this.contatos += "," + retorno.contatos
        }

        this.formEmails.get("emails").setValue(this.contatos)
      })
    }


    public buscarEmailsFocal(){
      
      

      this.agendamentoService.getContatosFocal(this.id_embarque).then((retorno: any)=>{
        
        if(this.contatos == ""){
          this.contatos += retorno.contatos
        }else{
          this.contatos += "," + retorno.contatos
        }
        

        this.formEmails.get("emails").setValue(this.contatos)
      })
    }


    
}

@Component({
  selector: 'dialog-agendamento',
  templateUrl: 'agenda-dialog-component.html',
  styleUrls: ['./agendamento.component.css'],
  providers: [AgendamentoService, StatusService, DatePipe]
})

export class AgendaDialog {
  daag_id_cd_agendamento: string;
  private datePipe: DatePipe
  constructor(
    private router: Router,
    private agendamentoService: AgendamentoService,
    private statusService: StatusService,
    private toastr: ToastrService,
    private route: ActivatedRoute,
    public dialogRef: MatDialogRef<AgendaDialog> 
    ) { }

    ngOnInit(): void{
      
      this.getListStatus();
      this.getListMotivoAgendamento();
      this.getListMotivoCutoff();
      this.preencheComboResponsavelAgendamento();
      this.preencheComboResponsavelCutoff();
      this.getAgendamento();
  }
  
  public agendaModel: AgendaModel;
  public lstStatusModel: StatusModel[];
  public lstMotivoModelAgendamento: MotivoModel[];
  public lstMotivoModelCutoff: MotivoModel[];
  public lstResposavelAgendamento: ResponsavelModel[];
  public lstResposavelCutoff: ResponsavelModel[];
  public cutoff: boolean = false;
    
  public formulario: FormGroup = new FormGroup({
    'daag_dh_agendamento': new FormControl(null, Validators.required),
    'daag_id_cd_motivo': new FormControl(null, Validators.required),
    'daag_id_cd_responsavel': new FormControl(null, Validators.required),
    'emba_in_cutoff': new FormControl(null, Validators.required),
    'emba_id_cd_motivo': new FormControl(null),
    'emba_id_cd_responsavel': new FormControl(null),
    'daag_ds_tipoagendamento': new FormControl(null, Validators.required),
    'emba_id_cd_status': new FormControl(null, Validators.required),
    'daag_ds_senhaagendamento': new FormControl(null),
    'daag_ds_horaagendamento': new FormControl("08:00"),
  })

  public desativarRequired(){
    if(this.formulario.get("daag_ds_tipoagendamento").value == "Agendamento"){
      this.formulario.get("daag_id_cd_motivo").clearValidators()
      this.formulario.get("daag_id_cd_responsavel").clearValidators()

      this.formulario.get("daag_id_cd_motivo").updateValueAndValidity()
      this.formulario.get("daag_id_cd_responsavel").updateValueAndValidity()
    }else{
      this.formulario.get("daag_id_cd_motivo").setValidators(Validators.required)
      this.formulario.get("daag_id_cd_responsavel").setValidators(Validators.required)
    }
  }

  public getAgendamento(){

    if(this.daag_id_cd_agendamento != "0"){

      this.agendamentoService.getAgendamento(this.daag_id_cd_agendamento).then((retorno:AgendaModel)=>{
        this.agendaModel = retorno;
        let parts:any[] = this.agendaModel.daag_dh_agendamento.split('/')
        console.log(parts)
        let data: Date = new Date(parts[2], parts[1] - 1, parts[0]);
        this.formulario.setValue({
          'daag_dh_agendamento': data,
          
          //'daag_dh_agendamento': this.datePipe.transform(this.agendaModel.daag_dh_agendamento,"dd-MM-yyyy"),
          
          //'daag_id_cd_motivo': this.agendaModel.daag_id_cd_motivo,
         // 'daag_dh_agendamento': new Date(this.agendaModel.daag_dh_agendamento),
          'daag_id_cd_motivo': +this.agendaModel.daag_id_cd_motivo,// "+string" converte o tipo da variavel de string para numeric
          'daag_id_cd_responsavel': this.agendaModel.daag_id_cd_responsavel,
          'emba_in_cutoff': this.agendaModel.emba_in_cutoff,
          'emba_id_cd_motivo': +this.agendaModel.emba_id_cd_motivo,// "+string" converte o tipo da variavel de string para numeric
          'emba_id_cd_responsavel': this.agendaModel.emba_id_cd_responsavel,
          'daag_ds_tipoagendamento': this.agendaModel.daag_ds_tipoagendamento,
          'emba_id_cd_status': this.agendaModel.emba_id_cd_status,
          'daag_ds_senhaagendamento': this.agendaModel.daag_ds_senhaagendamento,
          'daag_ds_horaagendamento': this.agendaModel.daag_ds_horaagendamento
        })

        
    
        this.formulario.get("daag_dh_agendamento").disable();
        this.formulario.get("daag_id_cd_motivo").disable();
        this.formulario.get("daag_id_cd_responsavel").disable();
        this.formulario.get("daag_ds_tipoagendamento").disable();

        if(this.formulario.get("emba_in_cutoff").value == "S"){
          this.cutoff = true;
        }
    
        }).catch((retorno:any) => {
          ////console.log(retorno);
          if(retorno.error.error_description.indexOf("Access token expired")>-1){
            alert("Sessão Expirada");
            this.router.navigate(['']);
          }               
        });
      
    }    
  }

  public getListStatus(){

    this.statusService.getListStatus('N').then((retorno:StatusModel[])=>{
      this.lstStatusModel = retorno;

      ////console.log("status "+retorno);
   
    }, errorresponse => {
      ////console.log(errorresponse)
        this.toastr.error('Erro conexao backend\nContate o suporte.','COMBO STATUS');
    
    }).catch((retorno:any) => {
      ////console.log(retorno);
      if(retorno.error.error_description.indexOf("Access token expired")>-1){
        alert("Sessão Expirada");
        this.router.navigate(['']);
      }               
    });

  }

  public getListMotivoAgendamento(){

  this.agendamentoService.getcmbMotivo("1", "N").then((retorno:MotivoModel[])=>{
      this.lstMotivoModelAgendamento = retorno;
   
    }, errorresponse => {
      ////console.log(errorresponse)
        this.toastr.error('Erro conexao backend\nContate o suporte.','COMBO MOTIVO');
    
    }).catch((retorno:any) => {
      ////console.log(retorno);
      if(retorno.error.error_description.indexOf("Access token expired")>-1){
        alert("Sessão Expirada");
        this.router.navigate(['']);
      }               
    });

  }

  public getListMotivoCutoff(){

    this.agendamentoService.getcmbMotivo("3", "N").then((retorno:MotivoModel[])=>{
        this.lstMotivoModelCutoff = retorno;
     
      }, errorresponse => {
        ////console.log(errorresponse)
          this.toastr.error('Erro conexao backend\nContate o suporte.','COMBO MOTIVO');
      
      }).catch((retorno:any) => {
        ////console.log(retorno);
        if(retorno.error.error_description.indexOf("Access token expired")>-1){
          alert("Sessão Expirada");
          this.router.navigate(['']);
        }               
      });
  
    }

    public preencheComboResponsavelAgendamento():void{
        
      this.agendamentoService.getcmbResponsavel().then((retorno:ResponsavelModel[])=>{
          this.lstResposavelAgendamento = retorno
          ////console.log("resp "+retorno)
  
        }, errorresponse => {
          ////console.log(errorresponse)
            this.toastr.error('Erro conexao backend\nContate o suporte.','COMBO RESPONSAVEL');
        
        }).catch((retorno:any) => {
          ////console.log(retorno);
          if(retorno.error.error_description.indexOf("Access token expired")>-1){
            alert("Sessão Expirada");
            this.router.navigate(['']);
          }               
      });
  
    }

    public preencheComboResponsavelCutoff():void{
        
      this.agendamentoService.getcmbResponsavel().then((retorno:ResponsavelModel[])=>{
          this.lstResposavelCutoff = retorno
  
        }, errorresponse => {
          //console.log(errorresponse)
            this.toastr.error('Erro conexao backend\nContate o suporte.','COMBO RESPONSAVEL');
        
        }).catch((retorno:any) => {
          //console.log(retorno);
          if(retorno.error.error_description.indexOf("Access token expired")>-1){
            alert("Sessão Expirada");
            this.router.navigate(['']);
          }               
      });
  
    }

  public createAgendamento(){

    if(this.formulario.get("daag_dh_agendamento").value == null){
      this.toastr.info('Preencha os campos obrigatórios','AGENDAMENTO');
      return;
    }
    if(this.formulario.get("daag_id_cd_motivo").value == null && this.formulario.get("daag_ds_tipoagendamento").value != "Agendamento"){
      this.toastr.info('Preencha os campos obrigatórios','AGENDAMENTO');
      return;
    }
    if(this.formulario.get("daag_id_cd_responsavel").value == null && this.formulario.get("daag_ds_tipoagendamento").value != "Agendamento"){
      this.toastr.info('Preencha os campos obrigatórios','AGENDAMENTO');
      return;
    }
    if(this.formulario.get("emba_in_cutoff").value == null){
      this.toastr.info('Preencha os campos obrigatórios','AGENDAMENTO');
      return;
    }
    if(this.formulario.get("daag_ds_tipoagendamento").value == null){
      this.toastr.info('Preencha os campos obrigatórios','AGENDAMENTO');
      return;
    }
    if(this.formulario.get("emba_id_cd_status").value == null){
      this.toastr.info('Preencha os campos obrigatórios','AGENDAMENTO');
      return;
    }

      let agendaModel : AgendaModel = new AgendaModel(
        this.daag_id_cd_agendamento,
        sessionStorage.getItem('id_embarque'),
        this.formulario.get("emba_id_cd_status").value,
        this.formulario.get("daag_dh_agendamento").value,
        this.formulario.get("daag_id_cd_motivo").value,
        this.formulario.get("daag_id_cd_responsavel").value,
        this.formulario.get("daag_ds_tipoagendamento").value,
        this.formulario.get("daag_ds_senhaagendamento").value,
        this.formulario.get("emba_in_cutoff").value,
        this.formulario.get("emba_id_cd_motivo").value,
        this.formulario.get("emba_id_cd_responsavel").value,
        "",//DS_STATUS
        sessionStorage.getItem('idFuncionario'),
        "",//DS_MOTIVO
        "",//DS_RESPONSAVEL,
        this.formulario.get("daag_ds_horaagendamento").value
      ) 
  
      this.agendamentoService.createUpdateAgendamento(agendaModel).subscribe( response => {
                
        if(response.sucesso == true){
          this.toastr.success('Solicitação realizada.','AGENDAMENTO')
          this.dialogRef.close();
        }else{
          
          this.toastr.error('Erro solicitação não realizada\n'+response.msg,'AGENDAMENTO')
          this.dialogRef.close();
        }
      }, errorresponse => {
          this.toastr.error('Erro conexao backend\nContate o suporte.','CRIAR AGENDAMENTO');
          this.dialogRef.close();
      })                 
    }

  public updateCutoff(){
    
    if(this.formulario.get("emba_in_cutoff").value == "S"){
      this.cutoff = true;
      this.formulario.get("emba_id_cd_motivo").setValidators(Validators.required);
      this.formulario.get("emba_id_cd_responsavel").setValidators(Validators.required);

    }else{
      this.cutoff = false;
      this.formulario.get("emba_id_cd_motivo").clearValidators();
      this.formulario.get("emba_id_cd_responsavel").clearValidators();

    }
    
  }
}










