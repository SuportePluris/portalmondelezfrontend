export class ListaEmbarqueFollowupModel {

    constructor(
        public emfo_id_cd_embafollow: string ,
        public emfo_id_cd_embarque: string ,
        public emfo_id_cd_usuariocriacao: string ,
        public user_ds_nomeusuario: string ,
        public emfo_id_cd_followup: string ,
        public foup_ds_followup: string ,
        public emfo_id_cd_motivo: string ,
        public moti_ds_motivo: string ,
        public emfo_id_cd_responsavel: string ,
        public resp_nm_responsavel: string ,
        public emfo_dh_registro: string ,
        public emfo_tx_followup: string 
    ){}
    
}
