export class EmbarqueFollowUpModel {

    constructor(
        public emfo_id_cd_embafollow : string,
        public emfo_id_cd_embarque : string,
        public emfo_id_cd_followup : string,
        public emfo_id_cd_motivo : string,        
        public emfo_id_cd_responsavel : string,
        public emfo_dh_registro : string,
        public emfo_id_cd_usuarioatualizacao : string,
        public emfo_dh_atualizacao : string,
        public emfo_id_cd_usuariocriacao : string,
        public emfo_tx_followup : string
    ){}
    
}
