export class SkuModel {

    constructor(
        public skud_id_cd_shipment : string,
        public skud_id_cd_delivery : string,
        public skud_ds_pesobruto : string,
        public skud_ds_pesoliquido : string,
        public skud_ds_codsku : string,
        public skud_ds_itemsku : string,
        public skud_ds_volumecubico : string,
        public skud_nr_quantidade : string,
        public skud_dh_delivery : string,
        public skud_ds_pedidomondelez : string,
        public skud_ds_pedidocliente : string,
        public skud_ds_tipovenda : string,
        public skud_dh_registro : string,
        public skud_dh_atualizacao : string,
        public skud_ds_valor : string
    ){}
    
}
