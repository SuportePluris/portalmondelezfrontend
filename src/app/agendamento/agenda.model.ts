export class AgendaModel {

    constructor(
        public daag_id_cd_agendamento: string,
        public daag_id_cd_embarque: string,
        public emba_id_cd_status: string,
        public daag_dh_agendamento: string,
        public daag_id_cd_motivo: string,
        public daag_id_cd_responsavel: string,
        public daag_ds_tipoagendamento: string,
        public daag_ds_senhaagendamento: string,
        public emba_in_cutoff: string,
        public emba_id_cd_motivo: string,
        public emba_id_cd_responsavel: string,
        public stat_ds_status: string,
        public daag_id_cd_usuario: string,
        public moti_ds_motivo: string,
        public resp_nm_responsavel: string,
        public daag_ds_horaagendamento: string
    ){}
    
}
