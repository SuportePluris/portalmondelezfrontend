import { Injectable } from '@angular/core'
import { environment } from 'src/environments/environment'
import { HttpClient} from '@angular/common/http'
import { Observable } from 'rxjs';
import { AgendaModel } from './agenda.model';
import { EmbarqueFollowUpModel } from './embarque-follow-up.model';

@Injectable()

export class AgendamentoService {

    public dataAtual = new Date();
    private url: string = environment.urlBackend;
    private environmentLstGetAgendamento: string = environment.getLstAgendamento;
    private environmentGetAgendamento: string = environment.getAgendamento;
    private environmentCreateUpdateAgendamento: String = environment.createUpdateAgendamento;
  
    private environmentGetListEventos: string = environment.getListEventosFoup;
    private environmentGetListEventosByStatus: string = environment.getListEventosFoupByStatus;
    private environmentGetListMotivos: string = environment.getListMotivos;
    private environmentGetListResponsaveis: string = environment.getListResponsavel;
    private environmentGetCreateEmbarqueFollowUp: string = environment.postCreateEmbarqueFollowUp;
    private environmentGetFollowupsEmbarque: string = environment.getListFollowupsEmbarque;
    private environmentGetEmbarque: string = environment.getEmbarque;
    private environmentUpdateLEadtime: string = environment.postUpdateLeadtime
    private environmentGetListSkus: string = environment.getListSkus;
    private environmentGetListNotas: string = environment.getListNotas;
    private environmentSetEmbarqueEmUso: string = environment.setembarqueemuso;
    private environmentUpdateStatusEmbarque: string = environment.updateStatusEmbarque;
    private environmentGetPDF: string = environment.getPDF;
    private environmentCopyPast: string = environment.getCopyPast;
    private environmentEnviaEmail: string = environment.enviaEmail;
    private environmentGetContatosTransp: string = environment.getContatosTransportadora
    private environmentGetContatosCliente: string = environment.getContatosCliente
    private environmentGetContatosFocal: string = environment.getContatosFocal

    constructor(
        private http: HttpClient
    ){ }

    public getPdf(id_embarque: number){

        const tokenString = localStorage.getItem('access_token');
        const token = JSON.parse(tokenString);

        const headers = {
            'Authorization' : 'Bearer ' + token.access_token,
            'Content-Type' : 'application/json'
        }

        return this.http.get(this.url +this.environmentGetPDF+"/"+id_embarque, { headers : headers})
        .toPromise().then((resposta:any) => resposta);
    }

    public getContatosTransportadora(id_embarque: number): Promise<any>{
        const tokenString = localStorage.getItem('access_token');
        const token = JSON.parse(tokenString);

        const headers = {
            'Authorization' : 'Bearer ' + token.access_token,
            'Content-Type' : 'application/json'
        }
        return this.http.get(this.url +this.environmentGetContatosTransp+"/"+id_embarque, { headers : headers})
        .toPromise().then((resposta:any) => resposta);
    }

    public getContatosCliente(id_embarque: number): Promise<any>{

        const tokenString = localStorage.getItem('access_token');
        const token = JSON.parse(tokenString);

        const headers = {
            'Authorization' : 'Bearer ' + token.access_token,
            'Content-Type' : 'application/json'
        }
        return this.http.get(this.url +this.environmentGetContatosCliente+"/"+id_embarque, { headers : headers})
        .toPromise().then((resposta:any) => resposta);
    }

    public getContatosFocal(id_embarque: number): Promise<any>{

        const tokenString = localStorage.getItem('access_token');
        const token = JSON.parse(tokenString);

        const headers = {
            'Authorization' : 'Bearer ' + token.access_token,
            'Content-Type' : 'application/json'
        }
        return this.http.get(this.url +this.environmentGetContatosFocal+"/"+id_embarque, { headers : headers})
        .toPromise().then((resposta:any) => resposta);
    }
    
    public getCopyPast(id_embarque: number){

        const tokenString = localStorage.getItem('access_token');
        const token = JSON.parse(tokenString);

        const headers = {
            'Authorization' : 'Bearer ' + token.access_token,
            'Content-Type' : 'application/json'
        }
        //console.log(this.url +this.environmentCopyPast+"/"+id_embarque);
        return this.http.get(this.url +this.environmentCopyPast+"/"+id_embarque, { headers : headers})
        .toPromise().then((resposta:any) => resposta);
    }

    public teste(){

        const tokenString = localStorage.getItem('access_token');
        const token = JSON.parse(tokenString);

        const headers = {
            'Authorization' : 'Bearer ' + token.access_token,
            'Content-Type' : 'application/json'
        }

        return this.http.get(this.url +'/portalpluris/testebyte', { headers : headers})
        .toPromise().then((resposta:any) => resposta);
    }

    public setAgendamentoInUso(id_embarque:number, in_uso:string):Promise<any>{
        const tokenString = localStorage.getItem('access_token');
        const token = JSON.parse(tokenString);

        const headers = {
            'Authorization' : 'Bearer ' + token.access_token,
            'Content-Type' : 'application/json'
        }

        let id_funcionario = localStorage.getItem('idFuncionario')

        return this.http.get(this.url +this.environmentSetEmbarqueEmUso+`/${id_embarque}/${in_uso}/${id_funcionario}`, { headers : headers})
        .toPromise().then((resposta:any) => resposta);
    }

    public getLstAgendamento(daag_id_cd_embarque: number):Promise<any> {

        const tokenString = localStorage.getItem('access_token');
        const token = JSON.parse(tokenString);

        const headers = {
            'Authorization' : 'Bearer ' + token.access_token,
            'Content-Type' : 'application/json'
        }

        return this.http.get(this.url +this.environmentLstGetAgendamento+"/"+daag_id_cd_embarque, { headers : headers})
        .toPromise().then((resposta:any) => resposta);
        
    }

    public getEmbarque(emba_id_cd_embarque: number):Promise<any>{
        const tokenString = localStorage.getItem('access_token');
        const token = JSON.parse(tokenString);

        const headers = {
            'Authorization' : 'Bearer ' + token.access_token,
            'Content-Type' : 'application/json'
        }

        return this.http.get(this.url +this.environmentGetEmbarque+"/"+emba_id_cd_embarque, { headers : headers})
        .toPromise();

    }

    public getAgendamento(daag_id_cd_agendamento: string):Promise<any> {

        const tokenString = localStorage.getItem('access_token');
        const token = JSON.parse(tokenString);

        const headers = {
            'Authorization' : 'Bearer ' + token.access_token,
            'Content-Type' : 'application/json'
        }

        return this.http.get(this.url +this.environmentGetAgendamento+"/"+daag_id_cd_agendamento, { headers : headers})
        .toPromise().then((resposta:any) => resposta);
    }

    public createUpdateAgendamento(agendaModel : AgendaModel):Observable<any> {
        
        const idFuncionario = localStorage.getItem('idFuncionario');
        agendaModel.daag_id_cd_usuario = idFuncionario;

        const tokenString = localStorage.getItem('access_token');
        const token = JSON.parse(tokenString);

        let json = JSON.stringify(agendaModel);
        console.log(json)
        const headers = {
            'Authorization' : 'Bearer ' + token.access_token,
            'Content-Type' : 'application/json'
        }
        
        return this.http.post(this.url + this.environmentCreateUpdateAgendamento, json, { headers : headers});

    }

    public getListFollowupsEmbarque(idEmbarque : number):Promise<any> {

        const tokenString = localStorage.getItem('access_token');
        const token = JSON.parse(tokenString);

        const headers = {
            'Authorization' : 'Bearer ' + token.access_token,
            'Content-Type' : 'application/json'
        }
        
        return this.http.get(this.url +this.environmentGetFollowupsEmbarque+"/"+idEmbarque, { headers : headers})
        .toPromise().then((resposta:any) => resposta);
        
    }
    
    public getcmbEventoFollow():Promise<any> {

        const tokenString = localStorage.getItem('access_token');
        const token = JSON.parse(tokenString);

        const headers = {
            'Authorization' : 'Bearer ' + token.access_token,
            'Content-Type' : 'application/json'
        }

        return this.http.get(this.url +this.environmentGetListEventos+"/N", { headers : headers})
        .toPromise().then((resposta:any) => resposta);
        
    }

    public getcmbEventoFollowByStatus(id_status:string):Promise<any> {

        const tokenString = localStorage.getItem('access_token');
        const token = JSON.parse(tokenString);

        const headers = {
            'Authorization' : 'Bearer ' + token.access_token,
            'Content-Type' : 'application/json'
        }

        return this.http.get(this.url +this.environmentGetListEventosByStatus+"/"+id_status, { headers : headers})
        .toPromise().then((resposta:any) => resposta);
        
    }

    public getcmbMotivo(param: string, inativo: string):Promise<any> {

        const tokenString = localStorage.getItem('access_token');
        const token = JSON.parse(tokenString);

        const headers = {
            'Authorization' : 'Bearer ' + token.access_token,
            'Content-Type' : 'application/json'
        }
         console.log(this.url +this.environmentGetListMotivos+"/"+param+"/"+inativo)
        return this.http.get(this.url +this.environmentGetListMotivos+"/"+param+"/"+inativo, { headers : headers})
        .toPromise().then((resposta:any) => resposta);
        
    } 

    public getlistSkusAgendamento(idDelivery : string ):Promise<any> {

        const tokenString = localStorage.getItem('access_token');
        const token = JSON.parse(tokenString);

        const headers = {
            'Authorization' : 'Bearer ' + token.access_token,
            'Content-Type' : 'application/json'
        }

        return this.http.get(this.url +this.environmentGetListSkus+"/"+ idDelivery, { headers : headers})
        .toPromise().then((resposta:any) => resposta);
        
    }

    public getlistNotas(skud_ds_pedidomondelez : string ):Promise<any> {

        const tokenString = localStorage.getItem('access_token');
        const token = JSON.parse(tokenString);

        const headers = {
            'Authorization' : 'Bearer ' + token.access_token,
            'Content-Type' : 'application/json'
        }

        return this.http.get(this.url +this.environmentGetListNotas+"/"+ skud_ds_pedidomondelez, { headers : headers})
        .toPromise().then((resposta:any) => resposta);
        
    }

    public getcmbResponsavel():Promise<any> {

        const tokenString = localStorage.getItem('access_token');
        const token = JSON.parse(tokenString);

        const headers = {
            'Authorization' : 'Bearer ' + token.access_token,
            'Content-Type' : 'application/json'
        }

        return this.http.get(this.url +this.environmentGetListResponsaveis+"/N", { headers : headers})
        .toPromise().then((resposta:any) => resposta);
        
    }


    

    public createEmbarqueFollowUp(followup : EmbarqueFollowUpModel):Observable<any> {
      
        const tokenString = localStorage.getItem('access_token');
        const token = JSON.parse(tokenString);
        
        let json = { 
            
            "emfo_id_cd_embafollow" : followup.emfo_id_cd_embafollow,
            "emfo_id_cd_embarque" : followup.emfo_id_cd_embarque,
            "emfo_id_cd_followup" : followup.emfo_id_cd_followup,
            "emfo_id_cd_motivo" : followup.emfo_id_cd_motivo,
            "emfo_id_cd_responsavel" : followup.emfo_id_cd_responsavel,
            "emfo_dh_registro" : followup.emfo_dh_registro,
            "emfo_id_cd_usuarioatualizacao" : followup.emfo_id_cd_usuarioatualizacao,
            "emfo_dh_atualizacao" : followup.emfo_dh_atualizacao,
            "emfo_id_cd_usuariocriacao" : followup.emfo_id_cd_usuariocriacao,
            "emfo_tx_followup" : followup.emfo_tx_followup
            
        }
        // console.log(json);

        const headers = {
            'Authorization' : 'Bearer ' + token.access_token,
            'Content-Type' : 'application/json'
        }
        
        return this.http.post(this.url + this.environmentGetCreateEmbarqueFollowUp, json, { headers : headers});

    }

    public updateLeadtime(dh_leadtime:string, emba_id_cd_embarque):Observable<any>{
        
        let json = {
            "emba_id_cd_usuarioatualizacao": localStorage.getItem('idFuncionario'),
            "emba_id_cd_embarque": emba_id_cd_embarque,
            "updt_dh_leadtime": dh_leadtime
        }


        const tokenString = localStorage.getItem('access_token');
        const token = JSON.parse(tokenString);
        const headers = {
            'Authorization' : 'Bearer ' + token.access_token,
            'Content-Type' : 'application/json'
        }
        return this.http.post(this.url + this.environmentUpdateLEadtime, json, { headers : headers});
    }

    public updateStatus(stat_id_cd_status:number, emba_id_cd_embarque: number): Observable<any>{
        let json = {
            "emba_id_cd_embarque": emba_id_cd_embarque,
            "emba_id_cd_usuarioatualizacao": localStorage.getItem('idFuncionario'),
            "stat_id_cd_status": stat_id_cd_status
        }

        const tokenString = localStorage.getItem('access_token');
        const token = JSON.parse(tokenString);
        const headers = {
            'Authorization' : 'Bearer ' + token.access_token,
            'Content-Type' : 'application/json'
        }
        return this.http.post(this.url + this.environmentUpdateStatusEmbarque, json, { headers : headers});
    }

    public enviaEmail(destinatario:string, id_embarque:number):Observable<any>{
        
        const tokenString = localStorage.getItem('access_token');
        const token = JSON.parse(tokenString);
        const headers = {
            'Authorization' : 'Bearer ' + token.access_token,
            'Content-Type' : 'application/json'
        }

        return this.http.get(this.url +this.environmentEnviaEmail+"/"+ destinatario+"/"+id_embarque+"/"+localStorage.getItem('idFuncionario'), { headers : headers})

    }
}
