export class DeliveryModel {

    constructor(
        skud_id_cd_shipment:string,
        skud_id_cd_delivery:string,
        nofi_nr_notafiscal: string,
        skud_ds_pesobruto:string,
        skud_ds_pesoliquido:string,
        skud_ds_volumecubico:string,
        skud_ds_valor:string,
        skud_nr_quantidade: string,
        skud_in_cancelado: string,
        skud_ds_pedidomondelez: string,
        skud_ds_pedidocliente: string
    ){}
}