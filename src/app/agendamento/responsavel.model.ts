export class ResponsavelModel {

    constructor(
        public resp_id_cd_responsavel : string,
        public resp_nm_responsavel : string,
        public resp_in_inativo : string,
        public resp_dh_registro : string
    ){}
    
}
