
export class EmbarqueAgendamentoBean {

    constructor(
        daag_id_cd_agendamento : string ,
        daag_id_cd_embarque : string ,
        emba_id_cd_status : string ,
        daag_dh_agendamento : string ,
        daag_id_cd_motivo : string ,
        daag_id_cd_responsavel : string ,
        daag_ds_tipoagendamento : string ,
        daag_ds_senhaagendamento : string ,
        emba_in_cutoff : string ,
        emba_id_cd_motivo : string ,
        emba_id_cd_responsavel : string ,
        stat_ds_status : string ,
        daag_id_cd_usuario : string ,
        emba_dh_leadtime : string ,
        moti_ds_motivo : string ,
        resp_nm_responsavel : string ,
        daag_ds_horaagendamento : string 
    ){}
}