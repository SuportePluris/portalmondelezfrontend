export class ContatosTransportadoraModel {

    constructor(
        public trml_id_transportadoraemail : string,
        public trml_ds_codigotransportadora : string,
        public  trml_ds_razaoscoail : string,
        public trml_ds_cnpj : string,
        public  trml_ds_email : string,
        public trml_ds_inativo : string,
        public  trml_dh_importacao : string,
        public trml_id_usuarioatualizacao : string,
        public trml_nm_arquivoprocessado : string,
    ){}
    
}
