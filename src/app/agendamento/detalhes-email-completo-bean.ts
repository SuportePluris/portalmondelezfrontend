import{RetornoShipmentBean} from './retorno-shipment-bean.model'
import{EmbarqueAgendamentoBean} from './embarque-agendamento-bean.model'
import{RetornoDeliveryBean} from './retorno-delivery-bean.model'

export class DetalhesEmailCompletoBean {

    constructor(
        layoutEmail : string,
        detalhesShip : RetornoShipmentBean,
        listDetalhesDoEmbarque : EmbarqueAgendamentoBean[] = [],
        listDetalhesDoDelivery : RetornoDeliveryBean[] = []
    ){}

}