import { Injectable } from '@angular/core'
import { environment } from 'src/environments/environment'
import { HttpClient} from '@angular/common/http'
import {FormControl} from '@angular/forms';

@Injectable()

export class HomeServices {

    private getLogo: string = environment.urlBackend + environment.getLogo;
    private url: string = environment.urlBackend;
    private envinromentGetliststatus : string = environment.getListStatusQtd;
    constructor(
        private http: HttpClient
    ){ }

    /*public obterLogo(empresa:string):Promise<any> {

        const tokenString = localStorage.getItem('access_token');
        const token = JSON.parse(tokenString);

        const headers = {
            'Authorization' : 'Bearer ' + token.access_token,
            'Content-Type' : 'application/json'
        }

        return this.http.get(this.getLogo+"/"+empresa, { headers : headers})
        .toPromise().then();
    }*/

    public getlistStatus():Promise<any>{
        const tokenString = localStorage.getItem('access_token');
        const token = JSON.parse(tokenString);
        const idFuncionario = localStorage.getItem('idFuncionario')
        
        const headers = {
            'Authorization' : 'Bearer ' + token.access_token,
            'Content-Type' : 'application/json'
        }
        return this.http.get(this.url +this.envinromentGetliststatus+"/N/" + idFuncionario, { headers : headers})
        .toPromise().then((resposta:any) => resposta);
    }
}
