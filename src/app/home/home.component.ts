import { Component, OnInit } from '@angular/core';
import { Autenticacao } from '../autenticacao.service'
import { Observable } from 'rxjs';
import { map, shareReplay } from 'rxjs/operators';
import { HomeServices } from './home.services';
import { FormControl } from '@angular/forms';
import { MAT_DATE_LOCALE } from '@angular/material/core';
import { AfterViewInit } from '@angular/core';
import { Router } from '@angular/router';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { fadeInOnEnterAnimation, fadeOutOnLeaveAnimation } from 'angular-animations';
import { formatDate } from '@angular/common';
import { StatusModel } from '../status/status.model'
import { StatusModelQtd } from '../status/statusqtd.model'


@Component({
  animations: [fadeInOnEnterAnimation(), fadeOutOnLeaveAnimation()],
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
  providers: [{provide: MAT_DATE_LOCALE, useValue: 'pt-BR'}, HomeServices ] 
})
export class HomeComponent implements  OnInit {
  isFavorite: boolean;
  public lstStatus: StatusModelQtd[] = []
  public lstStatusTemp: StatusModelQtd[] = []
  public toggleFavorite() {
    this.isFavorite = !this.isFavorite;
  }

  public strBase64:string;
  public nomeFuncionario = "";
  public canalOperador = localStorage.getItem('dsCanal');
  public regiaoOperador = localStorage.getItem('dsRegiao');
  public permissao = localStorage.getItem('permissao').trim();
  public frasedoDia:string;

  public dataAtual : any;
  
  date:Date;
  serializedDate = new FormControl((new Date()).toISOString());
  
  public imageLogo:string = "../../assets/img/pluris_midia_logo.png";
  public ativarRefresh: boolean = true
  //Este objeto virá do banco de dados a partir de um select na tabela de status onde titulo = ds_status e idStatus = id
  public opcoesStatus:any[] = [
    {'titulo':'Não Agendado', 'path': '/home/status', 'idStatus': 1},
    {'titulo':'Agendados', 'path': '/home/status', 'idStatus': 2},
    {'titulo':'Transportadora', 'path': '/home/status', 'idStatus': 3},
    {'titulo':'Não Formalizados', 'path': '/home/status', 'idStatus': 4},
    {'titulo':'Formalizados', 'path': '/home/status', 'idStatus': 5},
    {'titulo':'Entregues', 'path': '/home/status', 'idStatus': 6},
  ]

  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
    .pipe(
      map(result => result.matches),
      shareReplay()
    );
    
  constructor(
    private auth: Autenticacao, 
    private homeServices: HomeServices,
    private breakpointObserver: BreakpointObserver,
    private router: Router,) { 

      setInterval(() => {
        this.date = new Date();
      }, 1000)

    }


  ngOnInit(): void {

    //NOME DO FUNCIONARIO NA TELA 
    if(localStorage.getItem('nomeFuncionario').indexOf(" ") > 0){
      this.nomeFuncionario = localStorage.getItem('nomeFuncionario').substring(0,localStorage.getItem('nomeFuncionario').indexOf(" "));
    }else{
      this.nomeFuncionario = localStorage.getItem('nomeFuncionario')
    }

    this.dataAtual = formatDate(new Date(), 'HH', 'en');

    if( this.dataAtual >= 0 && this.dataAtual < 13){
      this.frasedoDia = 'Bom dia '
    }else if( this.dataAtual >= 13 && this.dataAtual < 19){
      this.frasedoDia = 'Boa tarde '
    }else{
      this.frasedoDia = 'Boa noite '
    }

    setInterval(()=>{
      this.ativarRefresh = true
    },10000)
    this.getStatusMenu()
    //this.chamado = localStorage.getItem('chamado');
    //this.maniSeq = localStorage.getItem('maniSeq');
    //this.empresaSessao = localStorage.getItem('empresa');
    //this.getLogoCliente();

  }

  init(): void {
		
	}	
  public sair(): void {

    this.auth.sair()
  }

  /*public getLogoCliente(){

    this.homeServices.obterLogo(this.empresaSessao)
      .then((retorno:any) =>{
        this.strBase64 = retorno.logoBase64;
      }
    );

  }*/

  public atualizarBadges(){
    this.ativarRefresh = false
    this.getStatusMenu()
  }

  public getStatusMenu(){
    //acessa o service para buscar a lista de status
    //this.ativarRefresh = false
    this.homeServices.getlistStatus().then((retorno:any)=>{
      this.lstStatusTemp= retorno;
      // console.log(this.lstStatus)
      //LIMPA O ARRAY PARA NÃO FICAR ACRESCENTANDO INDICES
      this.lstStatus = []
       this.lstStatus = this.lstStatusTemp;

       if(this.permissao == 'P' || this.permissao == 'PP'){
         this.lstStatus.splice(0,1)
       }

       
      // console.log(this.permissao)
      //SE A PERMISSÃO DO USUARIO FOR P OU PP, REMOVER O STATUS 'N/A NÃO AGENDADO' ID 1
      // if(this.permissao == "P" || this.permissao == "PP"){

      //   for (let index = 0; index < this.lstStatusTemp.length; index++) {
            
      //     if(this.lstStatusTemp[index].stat_id_cd_status != "1"){
            
      //       //ADICIONA O INDICE NO ARRAY
      //       this.lstStatus.push(this.lstStatusTemp[index])
      //     }
      //   }
      
      // }else{
      //   this.lstStatus = this.lstStatusTemp;
      // }
    })
  }


  public rotear(nomeRetear:string){
    
    
    // }else if(nomeRetear =='followups'){
    // this.router.navigate(['/home/followups']);
    
    // }else if(nomeRetear.indexOf('agendamento') > -1 ){
    //   this.router.navigate(['/home/'+nomeRetear]);    
    // }else if(nomeRetear.indexOf('relatorios') > -1 ){
    //   this.router.navigate(['/home/'+nomeRetear]);    
    
    // }else if(nomeRetear.indexOf('busca') > -1 ){
    //   this.router.navigate(['/home/'+nomeRetear]);

    // }else if(nomeRetear.indexOf('cadastrocombo') > -1 ){
    //   this.router.navigate(['/home/'+nomeRetear]);
    // }
      this.router.navigate(['/home/'+nomeRetear]);

  }

}
