import { Component, OnInit } from '@angular/core';


export interface Tile {
  color: string;
  cols: number;
  rows: number;
  text: string;
}

@Component({
  selector: 'app-inicial',
  templateUrl: './inicial.component.html',
  styleUrls: ['./inicial.component.css']
})
export class InicialComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  /*tiles: Tile[] = [
    {text: 'informações', cols: 3, rows: 3, color: 'lightblue'},
    {text: 'Links Uteis', cols: 1, rows: 6, color: 'lightgreen'},
    {text: 'Planilhas2', cols: 1, rows: 3, color: 'lightpink'},
    {text: 'Niveis de procesamento dos servers', cols: 2, rows: 3, color: '#DDBDF1'},
  ];*/

  tiles: Tile[] = [
    {text: 'informações', cols: 4, rows: 6, color: 'rgb(225,241,247)'}
  ];

}
