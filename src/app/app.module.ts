import { BrowserModule } from '@angular/platform-browser';
import { NgModule, LOCALE_ID } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms'
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';


import { AcessoComponent } from './acesso/acesso.component';
import { BannerComponent } from './acesso/banner/banner.component';
import { LoginComponent } from './acesso/login/login.component';

import { Autenticacao } from './autenticacao.service';
import { HomeComponent } from './home/home.component'
import { RouterModule } from '@angular/router'
import { ROUTES } from './app.routes'
import { AutenticacaoGuard } from './autenticacao-guard.service';
import { ToastrModule } from 'ngx-toastr';


import {MatAutocompleteModule} from '@angular/material/autocomplete';
import {MatBadgeModule} from '@angular/material/badge';
import {MatBottomSheetModule} from '@angular/material/bottom-sheet';
import {MatButtonModule} from '@angular/material/button';
import {MatButtonToggleModule} from '@angular/material/button-toggle';
import {MatCardModule} from '@angular/material/card';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatChipsModule} from '@angular/material/chips';
import {MatStepperModule} from '@angular/material/stepper';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatDialogModule} from '@angular/material/dialog';
import {MatDividerModule} from '@angular/material/divider';
import {MatExpansionModule} from '@angular/material/expansion';
import {MatGridListModule} from '@angular/material/grid-list';
import {MatIconModule} from '@angular/material/icon';
import {MatInputModule} from '@angular/material/input';
import {MatListModule} from '@angular/material/list';
import {MatMenuModule} from '@angular/material/menu';
import {MatNativeDateModule, MatRippleModule} from '@angular/material/core';
import {MatPaginatorModule} from '@angular/material/paginator';
import {MatProgressBarModule} from '@angular/material/progress-bar';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import {MatRadioModule} from '@angular/material/radio';
import {MatSelectModule} from '@angular/material/select';
import {MatSidenavModule} from '@angular/material/sidenav';
import {MatSliderModule} from '@angular/material/slider';
import {MatSlideToggleModule} from '@angular/material/slide-toggle';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import {MatSortModule} from '@angular/material/sort';
import {MatTableModule} from '@angular/material/table';
import {MatTabsModule} from '@angular/material/tabs';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatTooltipModule} from '@angular/material/tooltip';
import {MatTreeModule} from '@angular/material/tree';

import { HttpClientModule } from '@angular/common/http';

//import { FoupDialog } from './agendamento/agendamento.component'

import { InicialComponent } from './inicial/inicial.component';
import { AgendamentoComponent,  FoupDetalhearDialog, CopyPastDialog, TransportadoraDialog,  FoupDialog, FoupHistDialog, ClienteDialog, AgendaDialog, AdiarLeadTimeComponent, SkuDetalhearDialog, notasDetalhesDialog, StatusDialog, EmailDialog } from './agendamento/agendamento.component';
import { UsuarioComponent, UsuarioCanalDialog } from './usuario/usuario.component';
import { ClienteComponent } from './cliente/cliente.component';
import { TransportadoraComponent } from './transportadora/transportadora.component';
import { StatusComponent } from './status/status.component';
import { FollowupComponent } from './followup/followup.component';
import localept from '@angular/common/locales/pt';
import { registerLocaleData} from '@angular/common';
import { CadastroStatusComponent } from './cadastro-status/cadastro-status.component';
import { RelatoriosComponent } from './relatorios/relatorios.component';
import { BuscaComponent } from './busca/busca.component';
import { CadastroCombosComponent } from './cadastro-combos/cadastro-combos.component';
import { CanalComponent } from './canal/canal.component';
import { LoadingComponent } from './loading/loading.component';
import { RegiaoComponent } from './regiao/regiao.component';
import { ContatoComponent } from './contato/contato.component';
// import { LOCALE_ID } from '@angular/core'
//import * as moment from 'moment';
//import 'moment/locale/pt-br';
registerLocaleData(localept, 'pt');


@NgModule({
  declarations: [
    AppComponent,
    AcessoComponent,
    BannerComponent,
    LoginComponent,
    HomeComponent,
    InicialComponent,
    AgendamentoComponent,
    FoupDetalhearDialog,
    CopyPastDialog,
    SkuDetalhearDialog,
    notasDetalhesDialog,
    UsuarioComponent,
    ClienteComponent,
    TransportadoraComponent,
    StatusComponent,
    FollowupComponent,
    FoupDialog,
    FoupHistDialog,
    ClienteDialog,
    AgendaDialog,
    CadastroStatusComponent,
    RelatoriosComponent,
    AdiarLeadTimeComponent,
    TransportadoraDialog,
    BuscaComponent,
    CadastroCombosComponent,
    CanalComponent,
    UsuarioCanalDialog,
    StatusDialog,
    LoadingComponent,
    RegiaoComponent,
    EmailDialog,
    ContatoComponent
    
  ],
  imports: [
    ToastrModule.forRoot(),
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    ReactiveFormsModule,
    RouterModule.forRoot(ROUTES,  {useHash: true }),
    HttpClientModule,
    MatAutocompleteModule,
    MatBadgeModule,
    MatBottomSheetModule,
    MatButtonModule,
    MatButtonToggleModule,
    MatCardModule,
    MatCheckboxModule,
    MatChipsModule,
    MatStepperModule,
    MatDatepickerModule,
    MatDialogModule,
    MatDividerModule,
    MatExpansionModule,
    MatGridListModule,
    MatIconModule,
    MatInputModule,
    MatListModule,
    MatMenuModule,
    MatNativeDateModule,
    MatPaginatorModule,
    MatProgressBarModule,
    MatProgressSpinnerModule,
    MatRadioModule,
    MatRippleModule,
    MatSelectModule,
    MatSidenavModule,
    MatSliderModule,
    MatSlideToggleModule,
    MatSnackBarModule,
    MatSortModule,
    MatTableModule,
    MatTabsModule,
    MatToolbarModule,
    MatTooltipModule,
    MatTreeModule,
    
    
  ],
  providers: [ Autenticacao, AutenticacaoGuard, {provide: LOCALE_ID, useValue: 'pt'}],
  bootstrap: [AppComponent]
})
export class AppModule { }
