import { ContatoModel } from './contato.model';

export class ClienteModel {

    constructor(
        public clie_id_cd_cliente : string,
        public clie_ds_nomecliente : string,
        public clie_id_cd_codigocliente : string,
        public clie_ds_cnpj : string,
        public clie_dh_registro : string,
        public clie_in_inativo : string,
        public clie_id_cd_usuarioatualizacao : string,
        public clie_dh_atualizacao : string,
        public clie_ds_pa : string,
        public clie_ds_cidade : string,
        public clie_ds_uf : string,
        public clie_ds_regiao : string,
        public clie_ds_agrupamento : string,
        public clie_ds_endereco : string,
        public clie_ds_cep : string,
        public clie_ds_nfobrigatoria : string,
        public clie_ds_agendamentoobrigatorio : string,
        public clie_ds_diasrecebimento : string,
        public clie_ds_horariorecebimento : string,
        public clie_ds_veiculodedicado : string,
        public clie_ds_taxadescarga : string,
        public clie_ds_fefoespecial : string,
        public clie_ds_paletizacaoespecial : string,
        public clie_ds_paletizacaomondelez : string,
        public clie_ds_tipopaletizacao : string,
        public clie_ds_codveiculomax : string,
        public clie_ds_tipoveiculomax : string,
        public clie_ds_pesoveiculo : string,
        public clie_ds_temagendamento : string,
        public clie_ds_tipoagendamento : string,
        public clie_ds_esperacliente : string,
        public clie_ds_pagataxaveiculodedicado : string,
        public clie_ds_tipodescarga : string,
        public clie_ds_clienteexigepalete : string,
        public clie_ds_alturpalete : string,
        public clie_cd_veiculomax : string,
        public clie_ds_datarevisao : string,
        public clie_ds_resprevisao : string,
        public clie_ds_observacao : string,
        public cli_ds_restricaocirculacao : string,
        public clie_ds_tiporestricao : string,
        public clie_ds_horariorestricao : string,
        public clie_nm_arquivoprocessado : string,
        
        public  csCdtbContatoclienteCoclBean : ContatoModel
    ){}
    
}
