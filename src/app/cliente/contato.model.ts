export class ContatoModel {

    constructor(
        public cocl_id_cd_contatocliente: string,
        public cocl_id_cd_codigocliente: string,
        public cocl_ds_emailcontatocliente: string,
        public cocl_id_cd_usuarioatualizacao: string,
        public cocl_id_cd_usuariocriacao: string,
        public cocl_dh_registro: string,
        public cocl_dh_atualizacao: string,
        public cocl_in_inativo: string,
        public cocl_ds_nomecliente: string,
        public cocl_ds_telefonecliente: string,
        public cocl_ds_cepcliente: string,
        public cocl_ds_bairrocliente: string,
        public cocl_ds_cidadecliente: string,
        public cocl_ds_enderecocliente: string,
        public cocl_ds_cnjcliente: string,
        public cocl_ds_grupocliente: string,
        public cocl_ds_ufcliente: string,
        
    ){}
    
}
