import { Injectable } from '@angular/core'
import { environment } from 'src/environments/environment'
import { HttpClient} from '@angular/common/http'
import { Observable } from 'rxjs';
import { ClienteModel } from './cliente.model';

@Injectable()

export class ClienteService {

    public dataAtual = new Date();
    private url: string = environment.urlBackend;
    private environmentGetClientes: string = environment.getCliente;
    private environmentGetClientesFiltro: string = environment.getClienteFiltro;
    private environmentPostCreateUpdateCliente: string = environment.postCreateUpdateCliente;
    private environmentGetClientesByIdEmbarque: string = environment.getClientesByIdEmbarque;
    
    constructor(
        private http: HttpClient
    ){ }

    public getCliente():Promise<any> {

        const tokenString = localStorage.getItem('access_token');
        const token = JSON.parse(tokenString);

        const headers = {
            'Authorization' : 'Bearer ' + token.access_token,
            'Content-Type' : 'application/json'
        }

        return this.http.get(this.url +this.environmentGetClientes+"/null", { headers : headers})
        .toPromise().then((resposta:any) => resposta);
        
    }

    public getClienteFiltro(criterio:string, valor:string):Promise<any> {

        const tokenString = localStorage.getItem('access_token');
        const token = JSON.parse(tokenString);

        const headers = {
            'Authorization' : 'Bearer ' + token.access_token,
            'Content-Type' : 'application/json'
        }

        return this.http.get(this.url +this.environmentGetClientesFiltro+"/"+criterio+"/"+valor, { headers : headers})
        .toPromise().then((resposta:any) => resposta);
        
    }

    public getClientePorIdEmbarque(idEmbarque):Promise<any> {

        const tokenString = localStorage.getItem('access_token');
        const token = JSON.parse(tokenString);
        
        const headers = {
            'Authorization' : 'Bearer ' + token.access_token,
            'Content-Type' : 'application/json'
        }
        
        return this.http.get(this.url +this.environmentGetClientesByIdEmbarque+"/"+idEmbarque, { headers : headers})
        .toPromise().then((resposta:any) => resposta);
        
    }
   
    public createUpdateCliente(cliente : ClienteModel):Observable<any> {

        const tokenString = localStorage.getItem('access_token');
        const token = JSON.parse(tokenString);

        let json = { 
            "clie_id_cd_cliente" : cliente.clie_id_cd_cliente,
            "clie_ds_nomecliente" : cliente.clie_ds_nomecliente,
            "clie_id_cd_codigocliente" : cliente.clie_id_cd_codigocliente,
            "clie_ds_cnpj" : cliente.clie_ds_cnpj,
            "clie_dh_registro" : cliente.clie_dh_registro,
            "clie_in_inativo" : cliente.clie_in_inativo,
            "clie_id_cd_usuarioatualizacao" : localStorage.getItem('idFuncionario'),
            "clie_dh_atualizacao" : cliente.clie_dh_atualizacao,
            "clie_ds_pa" : cliente.clie_ds_pa,
            "clie_ds_cidade" : cliente.clie_ds_cidade,
            "clie_ds_uf" : cliente.clie_ds_uf,
            "clie_ds_regiao" : cliente.clie_ds_regiao,
            "clie_ds_agrupamento" : cliente.clie_ds_agrupamento,
            "clie_ds_endereco" : cliente.clie_ds_endereco,
            "clie_ds_cep" : cliente.clie_ds_cep,
            "clie_ds_nfobrigatoria" : cliente.clie_ds_nfobrigatoria,
            "clie_ds_agendamentoobrigatorio" : cliente.clie_ds_agendamentoobrigatorio,
            "clie_ds_diasrecebimento" : cliente.clie_ds_diasrecebimento,
            "clie_ds_horariorecebimento" : cliente.clie_ds_horariorecebimento,
            "clie_ds_veiculodedicado" : cliente.clie_ds_veiculodedicado,
            "clie_ds_taxadescarga" : cliente.clie_ds_taxadescarga,
            "clie_ds_fefoespecial" : cliente.clie_ds_fefoespecial,
            "clie_ds_paletizacaoespecial" : cliente.clie_ds_paletizacaoespecial,
            "clie_ds_paletizacaomondelez" : cliente.clie_ds_paletizacaomondelez,
            "clie_ds_tipopaletizacao" : cliente.clie_ds_tipopaletizacao,
            "clie_ds_codveiculomax" : cliente.clie_ds_codveiculomax,
            "clie_ds_tipoveiculomax" : cliente.clie_ds_tipoveiculomax,
            "clie_ds_pesoveiculo" : cliente.clie_ds_pesoveiculo,
            "clie_ds_temagendamento" : cliente.clie_ds_temagendamento,
            "clie_ds_tipoagendamento" : cliente.clie_ds_tipoagendamento,
            "clie_ds_esperacliente" : cliente.clie_ds_esperacliente,
            "clie_ds_pagataxaveiculodedicado" : cliente.clie_ds_pagataxaveiculodedicado,
            "clie_ds_tipodescarga" : cliente.clie_ds_tipodescarga,
            "clie_ds_clienteexigepalete" : cliente.clie_ds_clienteexigepalete,
            "clie_ds_alturpalete" : cliente.clie_ds_alturpalete,
            "clie_cd_veiculomax" : cliente.clie_cd_veiculomax,
            "clie_ds_datarevisao" : cliente.clie_ds_datarevisao,
            "clie_ds_resprevisao" : cliente.clie_ds_resprevisao,
            "clie_ds_observacao" : cliente.clie_ds_observacao,
            "cli_ds_restricaocirculacao" : cliente.cli_ds_restricaocirculacao,
            "clie_ds_tiporestricao" : cliente.clie_ds_tiporestricao,
            "clie_ds_horariorestricao" : cliente.clie_ds_horariorestricao,
            "clie_nm_arquivoprocessado" : cliente.clie_nm_arquivoprocessado,
            
        }

        const headers = {
            'Authorization' : 'Bearer ' + token.access_token,
            'Content-Type' : 'application/json'
        }
        
        return this.http.post(this.url + this.environmentPostCreateUpdateCliente, json, { headers : headers});

    }

    


    
}
