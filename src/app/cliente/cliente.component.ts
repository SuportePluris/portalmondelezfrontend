import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { NotificationService } from '../notification.service';
import { ClienteService } from '../cliente/cliente.service';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ClienteModel } from './cliente.model';

@Component({
  selector: 'app-cliente',
  templateUrl: './cliente.component.html',
  styleUrls: ['./cliente.component.css'],
  providers: [ClienteService]
})
export class ClienteComponent implements OnInit {

  constructor(
    private toastr: ToastrService,
    private router: Router,
    private clienteService: ClienteService,
    private notifyService : NotificationService

  ) { }


  panelOpenState = false;
  
  public isPiscar : boolean = false
  public mostrarTabela : boolean = false
  public lstClientes: ClienteModel[] = []
  public loginError : boolean = false
  public msgError : string = "";

  public formCadastro: FormGroup = new FormGroup({
      'idCliente': new FormControl(null, null),
      'nome': new FormControl(null, null),
      'codigo': new FormControl(null, null),
      'cnpj': new FormControl(null, null),
      'inativo': new FormControl(null, null),
      'pa': new FormControl(null, null),
      'cidade': new FormControl(null, null),
      'registro': new FormControl(null, null),
      'cd_usuarioatualizacao': new FormControl(null, null),
      'atualizacao': new FormControl(null, null),
      
      
      'uf': new FormControl(null, null),
      'regiao': new FormControl(null, null),
      'agrupamento': new FormControl(null, null),
      'endereco': new FormControl(null, null),
      'cep': new FormControl(null, null),
      'nfobrigatoria': new FormControl(null, null),
      'agendamentoobrigatorio': new FormControl(null, null),
      'diasrecebimento': new FormControl(null, null),
      'horariorecebimento': new FormControl(null, null),
      'veiculodedicado': new FormControl(null, null),
      'taxadescarga': new FormControl(null, null),
      'fefoespecial': new FormControl(null, null),
      'paletizacaoespecial': new FormControl(null, null),
      'paletizacaomondelez': new FormControl(null, null),
      'tipopaletizacao': new FormControl(null, null),
      'codveiculomax': new FormControl(null, null),
      'tipoveiculomax': new FormControl(null, null),
      'pesoveiculo': new FormControl(null, null),
      'temagendamento': new FormControl(null, null),
      'tipoagendamento': new FormControl(null, null),
      'esperacliente': new FormControl(null, null),
      'pagataxaveiculodedicado': new FormControl(null, null),
      'tipodescarga': new FormControl(null, null),
      'clienteexigepalete': new FormControl(null, null),
      'alturapalete': new FormControl(null, null),
      'veiculomax': new FormControl(null, null),
      'datarevisao': new FormControl(null, null),
      'resprevisao': new FormControl(null, null),
      'observacao': new FormControl(null, null),
      'restricaocirculacao': new FormControl(null, null),
      'tiporestricao': new FormControl(null, null),
      'horariorestricao': new FormControl(null, null),
      'nomeArquivo': new FormControl(null, null),
      
  })


  ngOnInit(): void {
  }

  public formBusca: FormGroup = new FormGroup({
    'criterioBusca': new FormControl(null, Validators.required),
    'valorBusca': new FormControl(null, Validators.required)
  })

  public busca(){

    if(this.formBusca.get("criterioBusca").value == null || this.formBusca.get("valorBusca").value == null){
      return;
    
    }else{
      this.preencheLista()
    }
  }

  public preencheLista():void{
    this.mostrarTabela = false;

    this.clienteService.getClienteFiltro(this.formBusca.get("criterioBusca").value, this.formBusca.get("valorBusca").value).then((retorno:ClienteModel[])=>{
        this.mostrarTabela = true;
        this.lstClientes = retorno

      }).catch((retorno:any) => {
        console.log(retorno);
        if(retorno.error.error_description.indexOf("Access token expired")>-1){
          alert("Sessão Expirada");
          this.router.navigate(['']);
        }               
    });

  }


  public limparCadastro(){
      this.formCadastro.reset()
   }

   

   //PREENCHE NO FORM OS DADOS PARA ATUALIZAR
   public editar(colunas : ClienteModel){

        
        // PISCAR EDIÇÃO
        let element = document.getElementById('idBodyCadastro')
        if(!element.classList.contains('fa-blink')){
          element.classList.add('fa-blink'); 
        }else{
          element.classList.remove('fa-blink'); 
          if(!element.classList.contains('fa-blink')){
            element.classList.add('fa-blink'); 
          }else{
            element.classList.remove('fa-blink');  
          }
        }

        //TRATATIVA CAMPOS BOOLEANOS
        let inativoAuxiliar:boolean = false ;
        if(colunas.clie_in_inativo != null &&
            (colunas.clie_in_inativo.trim() == "SIM" || colunas.clie_in_inativo.trim() == "S")){
           inativoAuxiliar = true
        }
       

        let nfObrigatoriaAuxiliar:boolean = false ;
        if(colunas.clie_ds_nfobrigatoria != null &&
             (colunas.clie_ds_nfobrigatoria.trim() == "SIM" || colunas.clie_ds_nfobrigatoria.trim() == "S")){
          nfObrigatoriaAuxiliar = true
        }
        
        let agendamentoobrigatorioAuxiliar:boolean = false ;
        if(colunas.clie_ds_agendamentoobrigatorio != null &&
             (colunas.clie_ds_agendamentoobrigatorio.trim() == "SIM" || colunas.clie_ds_agendamentoobrigatorio.trim() == "S")){
              agendamentoobrigatorioAuxiliar = true
        }

        let taxadescargaAuxiliar:boolean = false ;
        if(colunas.clie_ds_taxadescarga != null &&
             (colunas.clie_ds_taxadescarga.trim() == "SIM" || colunas.clie_ds_taxadescarga.trim() == "S")){
              taxadescargaAuxiliar = true
        }


        let veiculodedicadoAuxiliar:boolean = false ;
        if(colunas.clie_ds_veiculodedicado != null &&
             (colunas.clie_ds_veiculodedicado.trim() == "SIM" || colunas.clie_ds_veiculodedicado.trim() == "S")){
              veiculodedicadoAuxiliar = true
        }

        let pagataxaveiculodedicadoAuxiliar:boolean = false ;
        if(colunas.clie_ds_pagataxaveiculodedicado != null &&
             (colunas.clie_ds_pagataxaveiculodedicado.trim() == "SIM" || colunas.clie_ds_pagataxaveiculodedicado.trim() == "S")){
              pagataxaveiculodedicadoAuxiliar = true
        }


        let fefoespecialAuxiliar:boolean = false ;
        if(colunas.clie_ds_fefoespecial != null &&
             (colunas.clie_ds_fefoespecial.trim() == "SIM" || colunas.clie_ds_fefoespecial.trim() == "S")){
              fefoespecialAuxiliar = true
        }

        let paletizacaoespecialAuxiliar:boolean = false ;
        if(colunas.clie_ds_paletizacaoespecial != null &&
             (colunas.clie_ds_paletizacaoespecial.trim() == "SIM" || colunas.clie_ds_paletizacaoespecial.trim() == "S")){
              paletizacaoespecialAuxiliar = true
        }

        let paletizacaomondelezAuxiliar:boolean = false ;
        if(colunas.clie_ds_paletizacaomondelez != null &&
             (colunas.clie_ds_paletizacaomondelez.trim() == "SIM" || colunas.clie_ds_paletizacaomondelez.trim() == "S")){
              paletizacaomondelezAuxiliar = true
        }
        
        let clienteexigepaleteAuxiliar:boolean = false ;
        if(colunas.clie_ds_clienteexigepalete != null &&
             (colunas.clie_ds_clienteexigepalete.trim() == "SIM" || colunas.clie_ds_clienteexigepalete.trim() == "S")){
              clienteexigepaleteAuxiliar = true
        }
        
        let temagendamentoAuxiliar:boolean = false ;
        if(colunas.clie_ds_temagendamento != null &&
             (colunas.clie_ds_temagendamento.trim() == "SIM" || colunas.clie_ds_temagendamento.trim() == "S")){
              temagendamentoAuxiliar = true
        }
            


        this.formCadastro.setValue({
            idCliente: colunas.clie_id_cd_cliente == null ? '' : colunas.clie_id_cd_cliente.trim(),
            nome: colunas.clie_ds_nomecliente == null ? '' : colunas.clie_ds_nomecliente.trim(),
            codigo: colunas.clie_id_cd_codigocliente == null ? '' : colunas.clie_id_cd_codigocliente.trim(),
            cnpj: colunas.clie_ds_cnpj == null ? '' : colunas.clie_ds_cnpj.trim(),
            registro: colunas.clie_dh_registro == null ? '' : colunas.clie_dh_registro.trim(),
            inativo: inativoAuxiliar,
            cd_usuarioatualizacao: colunas.clie_id_cd_usuarioatualizacao == null ? '' : colunas.clie_id_cd_usuarioatualizacao.trim(),
            atualizacao: colunas.clie_dh_atualizacao  == null ? '' : colunas.clie_dh_atualizacao.trim(),
            pa: colunas.clie_ds_pa == null ? '' : colunas.clie_ds_pa.trim(),
            cidade: colunas.clie_ds_cidade == null ? '' : colunas.clie_ds_cidade.trim(),
            uf: colunas.clie_ds_uf == null ? '' : colunas.clie_ds_uf.trim(),
            regiao: colunas.clie_ds_regiao == null ? '' : colunas.clie_ds_regiao.trim(),
            agrupamento: colunas.clie_ds_agrupamento == null ? '' : colunas.clie_ds_agrupamento.trim(),
            endereco: colunas.clie_ds_endereco == null ? '' : colunas.clie_ds_endereco.trim(),
            cep: colunas.clie_ds_cep == null ? '' : colunas.clie_ds_cep.trim(),
            nfobrigatoria: nfObrigatoriaAuxiliar,
            agendamentoobrigatorio: agendamentoobrigatorioAuxiliar,
            diasrecebimento: colunas.clie_ds_diasrecebimento == null ? '' : colunas.clie_ds_diasrecebimento.trim(),
            horariorecebimento: colunas.clie_ds_horariorecebimento == null ? '' : colunas.clie_ds_horariorecebimento.trim(),
            veiculodedicado: veiculodedicadoAuxiliar,
            taxadescarga: taxadescargaAuxiliar,
            fefoespecial: fefoespecialAuxiliar,
            paletizacaoespecial: paletizacaoespecialAuxiliar,
            paletizacaomondelez: paletizacaomondelezAuxiliar,
            tipopaletizacao: colunas.clie_ds_tipopaletizacao == null ? '' : colunas.clie_ds_tipopaletizacao.trim(),
            codveiculomax: colunas.clie_ds_codveiculomax == null ? '' : colunas.clie_ds_codveiculomax.trim(),
            tipoveiculomax: colunas.clie_ds_tipoveiculomax == null ? '' : colunas.clie_ds_tipoveiculomax.trim(),
            pesoveiculo: colunas.clie_ds_pesoveiculo == null ? '' : colunas.clie_ds_pesoveiculo.trim(),
            temagendamento: temagendamentoAuxiliar,
            tipoagendamento: colunas.clie_ds_tipoagendamento == null ? '' : colunas.clie_ds_tipoagendamento.trim(),
            esperacliente: colunas.clie_ds_esperacliente == null ? '' : colunas.clie_ds_esperacliente.trim(),
            pagataxaveiculodedicado: pagataxaveiculodedicadoAuxiliar,
            tipodescarga: colunas.clie_ds_tipodescarga == null ? '' : colunas.clie_ds_tipodescarga.trim(),
            clienteexigepalete: clienteexigepaleteAuxiliar,
            alturapalete: colunas.clie_ds_alturpalete == null ? '' : colunas.clie_ds_alturpalete.trim(),
            veiculomax: colunas.clie_cd_veiculomax== null ? '' : colunas.clie_cd_veiculomax.trim(),
            datarevisao: colunas.clie_ds_datarevisao == null ? '' : colunas.clie_ds_datarevisao.trim(),
            resprevisao: colunas.clie_ds_resprevisao == null ? '' : colunas.clie_ds_resprevisao.trim(),
            observacao: colunas.clie_ds_observacao == null ? '' : colunas.clie_ds_observacao.trim(),
            restricaocirculacao: colunas.cli_ds_restricaocirculacao == null ? '' : colunas.cli_ds_restricaocirculacao.trim(),
            tiporestricao: colunas.clie_ds_tiporestricao == null ? '' : colunas.clie_ds_tiporestricao.trim(),
            horariorestricao: colunas.clie_ds_horariorestricao == null ? '' : colunas.clie_ds_horariorestricao.trim(),
            nomeArquivo : colunas.clie_nm_arquivoprocessado == null ? '' : colunas.clie_nm_arquivoprocessado.trim()
        })

        this.toastr.warning('Modo de edição.','CADASTRO');

      }


      
  public criarCliente(){

    let inativoAux = "N";
    if(this.formCadastro.value.inativo == true){
      inativoAux = "S"
    }
    
    //TRATATIVA CAMPOS BOOLEANOS
    let nfobrigatoriaAuxiliar : string = "NÃO" ;
    if(this.formCadastro.value.nfobrigatoria == true){
      nfobrigatoriaAuxiliar = "SIM"
    }
    
    let agendamentoobrigatorioAuxiliar : string = "NÃO" ;
    if(this.formCadastro.value.agendamentoobrigatorio == true){
      agendamentoobrigatorioAuxiliar = "SIM"
    }

    let taxadescargaAuxiliar : string = "NÃO" ;
    if(this.formCadastro.value.taxadescarga == true){
          taxadescargaAuxiliar = "SIM"
    }

    let pagataxaveiculodedicadoAuxiliar : string = "NÃO" ;
    if(this.formCadastro.value.pagataxaveiculodedicado == true){
          pagataxaveiculodedicadoAuxiliar = "SIM"
    }

    let paletizacaoespecialAuxiliar : string = "NÃO" ;
    if(this.formCadastro.value.paletizacaoespecial == true){
          paletizacaoespecialAuxiliar = "SIM"
    }

    let paletizacaomondelezAuxiliar : string = "NÃO" ;
    if(this.formCadastro.value.paletizacaomondelez == true){
          paletizacaomondelezAuxiliar = "SIM"
    }
  
    let veiculodedicadoAuxiliar : string = "NÃO" ;
    if(this.formCadastro.value.veiculodedicado == true){
          veiculodedicadoAuxiliar = "SIM"
    }

    let fefoespecialAuxiliar : string = "NÃO" ;
    if(this.formCadastro.value.fefoespecial == true){
          fefoespecialAuxiliar = "SIM"
    }
    
    let clienteexigepaleteAuxiliar : string = "NÃO" ;
    if(this.formCadastro.value.clienteexigepalete == true){
          clienteexigepaleteAuxiliar = "SIM"
    }
    
    let temagendamentoAuxiliar : string = "NÃO" ;
    if(this.formCadastro.value.temagendamento == true){
          temagendamentoAuxiliar = "SIM"
    }

    
    //console.log(localStorage.getItem('idFuncionario'));
    let clienteModel : ClienteModel = new ClienteModel(
      this.formCadastro.value.idCliente,
      this.formCadastro.value.nome,
      this.formCadastro.value.codigo,
      this.formCadastro.value.cnpj,
      this.formCadastro.value.registro,
      this.formCadastro.value.inativo,
      this.formCadastro.value.cd_usuarioatualizacao,
      this.formCadastro.value.atualizacao,
      this.formCadastro.value.pa,
      this.formCadastro.value.cidade,
      this.formCadastro.value.uf,
      this.formCadastro.value.regiao,
      this.formCadastro.value.agrupamento,
      this.formCadastro.value.endereco,
      this.formCadastro.value.cep,
      nfobrigatoriaAuxiliar,
      agendamentoobrigatorioAuxiliar,
      this.formCadastro.value.diasrecebimento,
      this.formCadastro.value.horariorecebimento,
      veiculodedicadoAuxiliar,
      taxadescargaAuxiliar,
      fefoespecialAuxiliar,
      paletizacaoespecialAuxiliar,
      paletizacaomondelezAuxiliar,
      this.formCadastro.value.tipopaletizacao,
      this.formCadastro.value.codveiculomax,
      this.formCadastro.value.tipoveiculomax,
      this.formCadastro.value.pesoveiculo,
      temagendamentoAuxiliar,
      this.formCadastro.value.tipoagendamento,
      this.formCadastro.value.esperacliente,
      pagataxaveiculodedicadoAuxiliar,
      this.formCadastro.value.tipodescarga,
      clienteexigepaleteAuxiliar,
      this.formCadastro.value.alturapalete,
      this.formCadastro.value.veiculomax,
      this.formCadastro.value.datarevisao,
      this.formCadastro.value.resprevisao,
      this.formCadastro.value.observacao,
      this.formCadastro.value.restricaocirculacao,
      this.formCadastro.value.tiporestricao,
      this.formCadastro.value.horariorestricao,
      '',
      null
    )

    this.clienteService.createUpdateCliente(clienteModel).subscribe( response => {
              
      if(response.sucesso == true){
    
        this.preencheLista();
        this.limparCadastro(); 

        this.toastr.success('Cliente cadastrado.','CADASTRO')
        //this.msgError='Usuário cadastrado.';
                  
      }else{
        
        if(response.processo == "validacao"){
          this.toastr.error('Erro Cliente não cadastrado\nFavor preencher os campos:\n'+response.msg,'CADASTRO')
          //this.msgError='Erro Usuário não cadastrado\nFavor preencher os campos\n: '+response.msg;
          this.loginError = true;
        }else{
          this.toastr.error('Erro Cliente não cadastrado\n'+response.msg,'CADASTRO')
          //this.msgError='Erro Usuário não cadastrado: '+response.msg;
          this.loginError = true;
        }

      }

    }, errorresponse => {
        this.toastr.error('Erro ao cadastrar Cliente','CADASTRO');
        //this.msgError='Erro Usuário não cadastrado';
        this.loginError = true;

    }) 
                    
  }


}
