import { Injectable } from '@angular/core'
import { environment } from '../../environments/environment'
import { HttpClient} from '@angular/common/http'
import { HttpClientModule } from '@angular/common/http'; 
import { Observable } from 'rxjs';

@Injectable()
export class RelatoriosService {

    constructor(
        private http: HttpClient
    ){ }

    private url: string = environment.urlBackend;
    private environmentBuscaRelatorio: string = environment.buscaRelatorio;

    public buscarRelatorio(periodo:string, ano:string, transp:string):any{
        
        const tokenString = localStorage.getItem('access_token');
        const token = JSON.parse(tokenString);

        const headers = {
            'Authorization' : 'Bearer ' + token.access_token,
            'Content-Type' : 'application/json'
        }

        return this.http.get(this.url +this.environmentBuscaRelatorio+"/"+periodo+"/"+ano+"/"+transp, { headers : headers})
        .toPromise().then((resposta:any) => resposta);
    }
}