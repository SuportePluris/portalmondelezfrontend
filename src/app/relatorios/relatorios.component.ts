import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { RelatoriosService } from './relatorios.service'
import { Router } from '@angular/router';
import { RelatoriosModel } from './relatorios.model'
import * as XLSX from 'xlsx'
import { ExportExcelService } from './export-excel.service'
import { ArquivoService } from '../arquivo.service'
import { Transp } from './transp.model'
import { CancelarPedido } from './cancelar-pedido.model'


@Component({
  selector: 'app-relatorios',
  templateUrl: './relatorios.component.html',
  styleUrls: ['./relatorios.component.css'],
  providers: [RelatoriosService, ArquivoService]
})
export class RelatoriosComponent implements OnInit {

  public loginError: boolean = false
  public mostrarTabela: boolean = false
  public habilitarBotaoImportarCodTransp: string = 'true'
  public habilitarBotaoImportarPedidoCancelado: string = 'disabled'
  public habilitarBotaoImportar: string = 'disabled'
  public loading: boolean = false
  public headers: string[] = []
  public arquivoTransp: Transp[] = []
  public arquivoCancelarPedido: CancelarPedido[] = []

  // array de anos
  public anos: string[]

  public formCadastro: FormGroup = new FormGroup({
    'periodo': new FormControl(null, null),
    'transp': new FormControl(null, null),
    'anos': new FormControl(null, null),
  })

  constructor(private relatoriosService: RelatoriosService,
    public router: Router,
    private toastr: ToastrService,
    public arquivoService: ArquivoService,
    public ete: ExportExcelService) { }

  public lstRelatoriosModel: RelatoriosModel[] = []

  ngOnInit(): void {

    // PREENCHER ARRAY DE ANOS
    this.anos = this.preencherArrayAnos()

  }

  public buscarRelatorio(): void {

    // VALIDAR CAMPOS
    if (this.formCadastro.value.periodo == null) {
      this.toastr.error('Preencha o campo: Periodo', 'RELATÓRIO');
      return
    }

    if (this.formCadastro.value.anos == null) {
      this.toastr.error('Preencha o campo: Ano', 'RELATÓRIO');
      return
    }

    let transpAux = "N"
    if (this.formCadastro.value.transp == true) {
      transpAux = "S"
    }

    // EFETUAR BUSCA DE RELATORIO NO BACKEND
    this.relatoriosService.buscarRelatorio(this.formCadastro.value.periodo, this.formCadastro.value.anos, transpAux).then((retorno: RelatoriosModel[]) => {
      this.lstRelatoriosModel = retorno

      this.exportExcel(this.lstRelatoriosModel, this.formCadastro.value.periodo);

    }).catch((retorno: any) => {
      if (retorno.error.error_description.indexOf("Access token expired") > -1) {
        alert("Sessão Expirada");
        this.router.navigate(['']);
      }
    });
  }

  public preencherArrayAnos(): string[] {

    var max = new Date().getFullYear()
    var min = max - 4
    var years = []

    for (var i = max; i >= min; i--) {
      years.push(i)
    }

    return years

  }

  public importarDados(tipo :string): void {

    if(tipo == 'transp') {
      this.habilitarBotaoImportarCodTransp = 'true'
    } else if(tipo == 'pedidocancelado') {
      this.habilitarBotaoImportarPedidoCancelado = 'true'
    }

    this.loading = true

    let retorno : any;   

    retorno = this.arquivoService.importarDadosArquivo(tipo)
      .subscribe((retorno: any) => {
        if (retorno.sucesso) {
          this.loading = false  
                  
          if(tipo == 'transp') {
            this.habilitarBotaoImportarCodTransp = 'true'
            this.toastr.success('Códigos de transportadoras atualizados com sucesso', 'Atualização Códigos Transportadoras');
          } else if(tipo == 'pedidocancelado') {
            this.habilitarBotaoImportarPedidoCancelado = 'true'
            this.toastr.success('Pedidos cancelados com sucesso', 'Cancelar Pedidos');
          }    
          
        } else {
          this.loading = false

          if(tipo == 'pedidocancelado') {
            this.habilitarBotaoImportarCodTransp = 'true'
            this.toastr.error('Não foi possível atualizar os códigos de transportadoras', 'Atualização Código Transportadora')
          } else if(tipo == 'pedidocancelado') {
            this.habilitarBotaoImportarPedidoCancelado = 'true'
            this.toastr.error('Não foi possível cancelar os pedidos', 'Atualização Código Transportadora')
          }          
        }
      })

      console.log(retorno)

  }

  onFileChange(ev, tipoarq) {
    let workBook = null;
    let jsonData = null;
    const reader = new FileReader();
    const file = ev.target.files[0];
    reader.onload = (event) => {
      const data = reader.result;
      workBook = XLSX.read(data, { type: 'binary', cellText: false, cellDates: true });
      jsonData = workBook.SheetNames.reduce((initial, name) => {
        const sheet = workBook.Sheets[name];
        this.headers.push(this.get_header_row(sheet))

        initial[name] = XLSX.utils.sheet_to_json(sheet, { raw: false, dateNF: 'yyyy-mm-dd' });
        return initial;
      }, {});

      console.log(jsonData.Planilha1)
      const dataString = JSON.stringify(jsonData)    
      
      if(tipoarq == 'transp') {
        if (jsonData.Planilha1 != undefined) {
          this.arquivoTransp = this.arquivoService.previaArquivo(jsonData.Planilha1, 'transp', this.headers)
  
          if(this.arquivoTransp.length > 0){        
            this.mostrarTabela = true
            this.habilitarBotaoImportarCodTransp = 'false'
          }else{
            this.toastr.error('Favor verificar a planilha de importação e tentar novamente', 'Atualização Código Transportadora')
          }
  
        } else {
          this.toastr.error('Favor verificar a planilha de importação e tentar novamente', 'Atualização Código Transportadora')
        }
      } else if(tipoarq == 'pedidocancelado') {
        if (jsonData.Planilha1 != undefined) {
          this.arquivoCancelarPedido = this.arquivoService.previaArquivo(jsonData.Planilha1, 'pedidocancelado', this.headers)
  
          if(this.arquivoCancelarPedido.length > 0){
            this.mostrarTabela = true
            this.habilitarBotaoImportarPedidoCancelado = 'false'
          }else{
            this.toastr.error('Favor verificar a planilha de importação e tentar novamente', 'Cancelar Pedido')
          }
  
        } else {
          this.toastr.error('Favor verificar a planilha de importação e tentar novamente', 'Cancelar Pedido')
        }
      }
    }

    reader.readAsBinaryString(file);

  }

  public get_header_row(sheet): any {
    let headers = [];
    let range = XLSX.utils.decode_range(sheet['!ref']);
    let C, R = range.s.r;

    for (C = range.s.c; C <= range.e.c; ++C) {
      let cell = sheet[XLSX.utils.encode_cell({ c: C, r: R })]

      /* find the cell in the first row */
      let hdr = "UNKNOWN " + C; // <-- replace with your desired default 
      if (cell && cell.t)
        hdr = XLSX.utils.format_cell(cell);

      headers.push(hdr);
    }
    return headers;
  }

  exportExcel(lstRelatoriosModel: RelatoriosModel[], periodo: string) {
    console.log(lstRelatoriosModel)
    this.ete.exportExcel(lstRelatoriosModel, periodo)
  }

}
