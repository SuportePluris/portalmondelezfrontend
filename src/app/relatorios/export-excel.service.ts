import { Injectable } from '@angular/core';
import { RelatoriosModel } from './relatorios.model';

import * as Excel from "exceljs/dist/exceljs.min.js";
import * as ExcelProper from "exceljs";
import * as fs from 'file-saver';

@Injectable({
  providedIn: 'root'
})
export class ExportExcelService {

  constructor() { }

  exportExcel(excelData, periodo:string) {

    //Title, Header & Data
    const title = "Relatório Mondelez "+periodo;
    //const header = excelData.headers
    const data = excelData;

    //Create a workbook with a worksheet
    let workbook: ExcelProper.Workbook = new Excel.Workbook();
    let worksheet = workbook.addWorksheet(title);

    let i: number = 0;

    const rowValues = [];
   
   //rowValues[i++] = 'CODIGO EMBARQUE'
    rowValues[i++] = '3PL'
    rowValues[i++] = 'TIPO_AGENDAMENTO'
    rowValues[i++] = 'PEDIDO_MDLZ_557'
    rowValues[i++] = 'PEDIDO_CLIENTE'
    rowValues[i++] = 'DELIVERY_NOTE_558'
    rowValues[i++] = 'TIPO_PEDIDO_DE_VENDA'
    rowValues[i++] = 'NOTA_FISCAL'
    rowValues[i++] = 'SERIE_NF'
    rowValues[i++] = 'DATA_EMISSAO_NOTA'
    rowValues[i++] = 'TRANSPORTADORA_NF'
    rowValues[i++] = 'TRANSPORTADORA'
    rowValues[i++] = 'TRANSP_CONFIRMADA'
    rowValues[i++] = 'TIPO_DE_CARGA'
    //rowValues[i++] = 'VEICULO'
    rowValues[i++] = 'TIPO_DE_VEICULO'
    rowValues[i++] = 'SHIPMENT'
    rowValues[i++] = 'DATA_SHIPMENT'
    rowValues[i++] = 'CODIGO_CLIENTE'
    rowValues[i++] = 'CNPJ_ENTREGA'
    rowValues[i++] = 'RAZAO_SOCIAL'
    rowValues[i++] = 'CIDADE'
    rowValues[i++] = 'UF'
    rowValues[i++] = 'CD_ORIGEM'
    rowValues[i++] = 'CANAL'
    rowValues[i++] = 'REGIONAL'
    rowValues[i++] = 'SOMA_PESO_LIQUIDO'
    rowValues[i++] = 'SOMA_CUBAGEM_LIQUIDA'
    rowValues[i++] = 'SOMA_VALOR_LIQUIDO'
    rowValues[i++] = 'QTD_VOLUMES'
    rowValues[i++] = 'DIAS_LEAD_TIME'
    rowValues[i++] = 'LEADTIME'
    rowValues[i++] = 'PRAZO_REALIZAR_AGENDAMENTO'
    rowValues[i++] = 'AGING_AGENDAMENTO'
    rowValues[i++] = 'ON_TIME_SCHEDULING'
    rowValues[i++] = 'PRIMEIRO_CONTATO'
    rowValues[i++] = 'STATUS_SOLICITACAO'
    rowValues[i++] = 'INPUT_PLURIS_AGENDAMENTO'
    rowValues[i++] = 'DATA_DA_AGENDA'
    //rowValues[i++] = 'HORA AGENDMENTO'
    rowValues[i++] = 'SENHA'
    rowValues[i++] = 'TMCA'
    rowValues[i++] = 'DATA_SOLICIT_NOVA_AGENDA'
    rowValues[i++] = 'NOVA_AGENDA'
    //rowValues[i++] = 'HORA RETIFICAÇÃO'
    
    
    
    //rowValues[i++] = 'HORA REAGENDAMENTO'
    
    
    rowValues[i++] = 'AGING_REAGENDAMENTO'
    rowValues[i++] = 'PREV_FATURAMENTO'
    rowValues[i++] = 'DESVIO_LEADTIME_AGENDA'
    rowValues[i++] = 'MOTIVO_DESVIO'
    rowValues[i++] = 'DT_OCR_AGENDAMENTO'
    rowValues[i++] = 'MOTIVO_OCR_AGEND'
    rowValues[i++] = 'RESPONSAVEL_PENDENCIA'
    rowValues[i++] = 'MOTIVO_REAGENDA'
    rowValues[i++] = 'RESPONSAVEL_REAGENDA'

    rowValues[i++] = 'CUTOFF'
    rowValues[i++] = 'MOTIVO_OCR_CUTOFF'
    rowValues[i++] = 'RESPONSAVEL_CUTOFF'
    rowValues[i++] = 'PASCOA'
    rowValues[i++] = 'DATA_FINALIZACAO'
    rowValues[i++] = 'STATUS_NF'
    rowValues[i++] = 'PERIODO'
    rowValues[i++] = 'CODIGO_RTM'
    rowValues[i++] = 'GERENTE_NACIONAL'
    rowValues[i++] = 'GERENTE_REGIONAL'
    rowValues[i++] = 'GERENTE_DE_AREA'
    rowValues[i++] = 'VENDEDOR'
    rowValues[i++] = 'DATA_EMISSAO_DELIVERY' 

    // rowValues[i++] = 'SENHA_NOVA_AGENDA'
    // rowValues[i++] = 'MOTIVO_NOVA_AGENDA'
    // rowValues[i++] = 'RESPONSAVELNOVA_AGENDA'
    // rowValues[i++] = 'DATA_SOLICIT_REAGENDA'
    
    // rowValues[i++] = 'DATA_REAGENDA'
    // rowValues[i++] = 'SENHA_REAGENDA'
    
    
    

    let headerRow = worksheet.addRow(rowValues);
    headerRow.eachCell((cell, number) => {
        cell.fill = {
        type: 'pattern',
        pattern: 'solid',
        fgColor: { argb: '4167B8' },
        bgColor: { argb: '' }
        }
        cell.font = {
        bold: true,
        color: { argb: 'FFFFFF' },
        size: 12
        }
    })

    for (let index = 0; index < data.length; index++) {
        const element = data[index];

        let i: number = 0;

        const rowValues = [];
        rowValues[i++] = 'PLURIS'
        rowValues[i++] = element.emba_ds_tipoagendamento
        rowValues[i++] = element.skud_ds_pedidomondelez
        rowValues[i++] = element.skud_ds_pedidocliente
        rowValues[i++] = element.skud_id_cd_delivery
        rowValues[i++] = element.skud_ds_tipovenda
        rowValues[i++] = element.nofi_nr_notafiscal
        rowValues[i++] = element.nofi_ds_serienota
        rowValues[i++] = element.nofi_dh_emissaonota
        rowValues[i++] = element.emba_ds_cod_transportadora
        rowValues[i++] = element.tran_nm_nometransportadora
        rowValues[i++] = element.emba_in_transportadoraconfirmada
        rowValues[i++] = element.emba_ds_tipocarga
        rowValues[i++] = element.emba_ds_veiculo + ' ' + element.emba_ds_tipoveiculo
        // rowValues[i++] = element.emba_ds_tipoveiculo
        rowValues[i++] = element.emba_id_cd_shipment
        rowValues[i++] = element.emba_dh_shipment
        rowValues[i++] = element.emba_id_cd_codcliente
        rowValues[i++] = element.clie_ds_cnpj
        rowValues[i++] = element.clie_ds_nomecliente
        rowValues[i++] = element.clie_ds_cidade
        rowValues[i++] = element.clie_ds_uf
        rowValues[i++] = element.emba_ds_planta
        rowValues[i++] = element.canal
        rowValues[i++] = element.regional
        rowValues[i++] = element.skud_ds_pesoliquido
        rowValues[i++] = element.skud_ds_volumecubico
        rowValues[i++] = element.skud_ds_valor
        rowValues[i++] = element.skud_nr_quantidade
        rowValues[i++] = element.dias_leadtime
        rowValues[i++] = element.emba_dh_leadtime
        rowValues[i++] = element.calc_prazo_realizar_agendamento
        rowValues[i++] = element.calc_aging_agendamento
        rowValues[i++] = element.calc_ontime_scheduling
        rowValues[i++] = element.calc_primeiro_contato
        rowValues[i++] = element.stat_ds_status
        rowValues[i++] = element.calc_input_dhl_agendamento
        rowValues[i++] = element.data_agendamento + ' ' + element.daag_ds_horaagendamento
        //rowValues[i++] = element.daag_ds_horaagendamento
        rowValues[i++] = element.senha_agendamento
        rowValues[i++] = element.tmca
        rowValues[i++] = element.data_solicit_nova_agenda
        rowValues[i++] = element.data_retificacao_agenda + '' + element.daag_ds_horaretificacaoagenda
        //rowValues[i++] = element.daag_ds_horaretificacaoagenda
        //rowValues[i++] = element.daag_ds_horareagendamento
        rowValues[i++] = element.calc_aging_reagendamento
        rowValues[i++] = ''
        rowValues[i++] = element.calc_desvio_leadtime
        rowValues[i++] = element.foup_motivo_desvio_leadtime
        rowValues[i++] = element.dt_ocr_agendamento
        rowValues[i++] = element.motivo_ocr_agend
        rowValues[i++] = element.responsavel_pendencia
        rowValues[i++] = element.motivo_reagendamento
        rowValues[i++] = element.responsavel_reagendamento
        rowValues[i++] = element.emba_in_cutoff
        rowValues[i++] = element.emba_ds_motivocutoff
        rowValues[i++] = element.emba_ds_responsavelcutoff
        rowValues[i++] = ''
        rowValues[i++] = ''
        rowValues[i++] = ''
        rowValues[i++] = element.emba_ds_periodo
        rowValues[i++] = ''
        rowValues[i++] = element.hqcl_ds_gerentenacional
        rowValues[i++] = element.hqcl_ds_gerenteregional
        rowValues[i++] = element.hqcl_ds_gerentearea
        rowValues[i++] = element.hqcl_ds_vendedor
        rowValues[i++] = element.skud_dh_delivery
        
        // rowValues[i++] = element.senha_retificacao_agenda
        // rowValues[i++] = element.motivo_retificacao_agenda
        // rowValues[i++] = element.responsavel_retificacao_agenda
        // rowValues[i++] = element.data_solicit_reagendamento
        // rowValues[i++] = element.data_reagendamento + '' + element.daag_ds_horareagendamento
        // rowValues[i++] = element.senha_reagendamento
        // rowValues[i++] = element.foup_responsavel_desvio_leadtime
        
        
        
        

        
        let row = worksheet.addRow(rowValues);

        if(index%2==0){
            row.eachCell((cell, number) => {
                cell.fill = {
                    type: 'pattern',
                    pattern: 'solid',
                    fgColor: { argb: 'FFFFFF' },
                    bgColor: { argb: '' }
                }
                cell.font = {
                bold: false,
                color: { argb: '' },
                size: 12
                }
            })

        }else{
            row.eachCell((cell, number) => {
                cell.fill = {
                    type: 'pattern',
                    pattern: 'solid',
                    fgColor: { argb: 'D6D6D6' },
                    bgColor: { argb: '' }
                }
                cell.font = {
                bold: false,
                color: { argb: '' },
                size: 12
                }
            })
        }

    }
    
    //Generate & Save Excel File
    workbook.xlsx.writeBuffer().then((data) => {
      let blob = new Blob([data], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' });
      fs.saveAs(blob, title + '.xlsx');
    })

  }
}