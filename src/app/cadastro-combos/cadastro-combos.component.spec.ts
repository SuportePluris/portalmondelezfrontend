import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CadastroCombosComponent } from './cadastro-combos.component';

describe('CadastroCombosComponent', () => {
  let component: CadastroCombosComponent;
  let fixture: ComponentFixture<CadastroCombosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CadastroCombosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CadastroCombosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
