export class MotivoModel{

    constructor(
        public moti_id_cd_motivo:string,
        public moti_ds_motivo:string,
        public moti_in_inativo:string,
        public moti_dh_registro:string,
        public tipo_id_cd_tipo:string
    ){}
}