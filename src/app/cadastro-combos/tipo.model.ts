export class TipoModel {

    constructor(
        public tipo_id_cd_tipo: string,
        public tipo_ds_tipo: string,
        public tipo_in_inativo: string,
        public tipo_dh_registro: string
    ){}
}