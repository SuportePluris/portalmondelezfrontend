import { Injectable } from '@angular/core'
import { environment } from 'src/environments/environment'
import { HttpClient} from '@angular/common/http'
import { Observable } from 'rxjs';
import { TipoModel } from './tipo.model'
import { MotivoModel } from './motivo.model'
import { ResponsavelModel } from './responsavel.model'

@Injectable()

export class CadastroService {

    private url: string = environment.urlBackend;
    private environmentGetlisTipo: string = environment.getLisTipo;
    private environmentCreateUpdateTipo: string = environment.createUpdateTipo;
    private environmentGetlisMotivo: string = environment.getLisMotivo;
    private environmentCreateUpdateMotivo: string = environment.createUpdateMotivo;
    private environmentGetlisResponsaveis: string = environment.getLisResponsaveis;
    private environmentCreateUpdateResponsavel: string = environment.createUpdateResponsavel;

    constructor(
        private http: HttpClient
    ){ }

    public getLstTipo(ativo:string):Promise<any> {

        const tokenString = localStorage.getItem('access_token');
        const token = JSON.parse(tokenString);

        const headers = {
            'Authorization' : 'Bearer ' + token.access_token,
            'Content-Type' : 'application/json'
        }

        return this.http.get(this.url +this.environmentGetlisTipo+"/"+ativo, { headers : headers})
        .toPromise().then((resposta:any) => resposta);
        
    }

    public getLstMotivo(tipo:string, ativo:string):Promise<any> {

        const tokenString = localStorage.getItem('access_token');
        const token = JSON.parse(tokenString);

        const headers = {
            'Authorization' : 'Bearer ' + token.access_token,
            'Content-Type' : 'application/json'
        }

        return this.http.get(this.url +this.environmentGetlisMotivo+"/"+tipo+"/"+ativo, { headers : headers})
        .toPromise().then((resposta:any) => resposta);
        
    }

    public createUpdateTipo(tipoModel:TipoModel):Observable<any> {
        
        const tokenString = localStorage.getItem('access_token');
        const token = JSON.parse(tokenString);

        let json = JSON.stringify(tipoModel);

        const headers = {
            'Authorization' : 'Bearer ' + token.access_token,
            'Content-Type' : 'application/json'
        }
        
        return this.http.post(this.url + this.environmentCreateUpdateTipo, json, { headers : headers});
    }

    public createUpdateMotivo(motivoModel:MotivoModel):Observable<any> {
        
        const tokenString = localStorage.getItem('access_token');
        const token = JSON.parse(tokenString);

        let json = JSON.stringify(motivoModel);

        const headers = {
            'Authorization' : 'Bearer ' + token.access_token,
            'Content-Type' : 'application/json'
        }
        
        return this.http.post(this.url + this.environmentCreateUpdateMotivo, json, { headers : headers});
    }

    public getLstResponsavel(inativo: string):Promise<any> {

        const tokenString = localStorage.getItem('access_token');
        const token = JSON.parse(tokenString);

        const headers = {
            'Authorization' : 'Bearer ' + token.access_token,
            'Content-Type' : 'application/json'
        }

        return this.http.get(this.url +this.environmentGetlisResponsaveis+"/"+inativo, { headers : headers})
        .toPromise().then((resposta:any) => resposta);
        
    }

    public createUpdateResponsavel(responsavelModel:ResponsavelModel):Observable<any> {
        
        const tokenString = localStorage.getItem('access_token');
        const token = JSON.parse(tokenString);

        let json = JSON.stringify(responsavelModel);

        const headers = {
            'Authorization' : 'Bearer ' + token.access_token,
            'Content-Type' : 'application/json'
        }
        
        return this.http.post(this.url + this.environmentCreateUpdateResponsavel, json, { headers : headers});
    }
}
