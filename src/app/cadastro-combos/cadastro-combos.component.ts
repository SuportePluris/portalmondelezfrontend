import { ReturnStatement } from '@angular/compiler';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { CadastroService } from './cadastro.service'
import { ToastrService } from 'ngx-toastr';
import { TipoModel } from './tipo.model';
import { MotivoModel } from './motivo.model';
import { ResponsavelModel } from './responsavel.model'

@Component({
  selector: 'app-cadastro-combos',
  templateUrl: './cadastro-combos.component.html',
  styleUrls: ['./cadastro-combos.component.css'],
  providers: [CadastroService]
})
export class CadastroCombosComponent implements OnInit {

  constructor(
    public cadastroService: CadastroService,
    public router: Router,
    private toastr: ToastrService
  ) {}

  ngOnInit(): void {
  }

  public lstTipoModel: TipoModel[]
  public comboTipoMotivoModel: TipoModel[]
  public lstMotivoModel: MotivoModel[]
  public lstResponsavelModel: ResponsavelModel[]
  public comboCadastro: string;

  public formPrincipal: FormGroup = new FormGroup({
    'cadastro': new FormControl(null, Validators.required),
    'dsMotivo': new FormControl(null, Validators.required),
    'tpMotivo': new FormControl(null, Validators.required),
    'motivo': new FormControl(null, Validators.required),
    'idTipo': new FormControl(null, Validators.required),
    'responsavel': new FormControl(null, Validators.required)

  })

  public getValue():void{
    this.comboCadastro = this.formPrincipal.get("cadastro").value;

    this.lstTipoModel = null
    this.comboTipoMotivoModel = null
    this.lstMotivoModel = null
    this.lstResponsavelModel = null

    if(this.comboCadastro == "tpMotivo"){
      this.lstTipoModel = TipoModel[("")];
      
      this.cadastroService.getLstTipo("N")
      .then((retorno:TipoModel[])=>{
        this.lstTipoModel = retorno
      
      }).catch((retorno:any) => {
        if(retorno.error.error_description.indexOf("Access token expired")>-1){
          alert("Sessão Expirada");
          this.router.navigate(['']);
        }               
      });

    }else if(this.comboCadastro == "motivo"){
      
      this.cadastroService.getLstTipo("N")
      .then((retorno:TipoModel[])=>{
        this.comboTipoMotivoModel = retorno

      }).catch((retorno:any) => {
        if(retorno.error.error_description.indexOf("Access token expired")>-1){
          alert("Sessão Expirada");
          this.router.navigate(['']);
        }               
      });

      this.listMotivos()

    }else if(this.comboCadastro == "responsavel"){
      
      this.cadastroService.getLstResponsavel("N")
      .then((retorno:ResponsavelModel[])=>{
        this.lstResponsavelModel = retorno

      }).catch((retorno:any) => {
        if(retorno.error.error_description.indexOf("Access token expired")>-1){
          alert("Sessão Expirada");
          this.router.navigate(['']);
        }               
      });
    }

  }

  public listMotivos(){

    let tipo_id_cd_tipo = this.formPrincipal.get("idTipo").value;

    if(tipo_id_cd_tipo != null){

    
      this.cadastroService.getLstMotivo(tipo_id_cd_tipo, "N")
      .then((retorno:MotivoModel[])=>{
        this.lstMotivoModel = retorno

      }).catch((retorno:any) => {
        if(retorno.error.error_description.indexOf("Access token expired")>-1){
          alert("Sessão Expirada");
          this.router.navigate(['']);
        }               
      });

    }
  }

  public criar():void{

    if(this.comboCadastro == "tpMotivo"){
      
      if(this.formPrincipal.get("motivo").value != ""){

        let tipoModel:TipoModel = new TipoModel("",this.formPrincipal.get("tpMotivo").value,"N","");

        this.cadastroService.createUpdateTipo(tipoModel).subscribe( response => {
          if(response.sucesso){
            this.toastr.success('Criado com sucesso', 'Cadastro')
          }else{
            this.toastr.error('Erro ao criar', 'Cadastro')
          }
        }, errorresponse => {
          console.log(errorresponse)
            this.toastr.error('Erro conexao backend\nContate o suporte.','Cadastro');
        
        })
        this.comboCadastro = "tpMotivo"
        this.getValue()

      }

    }else if(this.comboCadastro == "motivo"){
      
      let motivoModel:MotivoModel = new MotivoModel("",this.formPrincipal.get("dsMotivo").value,"N","", this.formPrincipal.get("idTipo").value);

        this.cadastroService.createUpdateMotivo(motivoModel).subscribe( response => {
          if(response.sucesso){
            this.toastr.success('Criado com sucesso', 'Cadastro')
          }else{
            this.toastr.error('Erro ao criar', 'Cadastro')
          }
        }, errorresponse => {
          console.log(errorresponse)
            this.toastr.error('Erro conexao backend\nContate o suporte.','Cadastro');
        
        })
        this.comboCadastro == "motivo"
        this.getValue()
        this.listMotivos()
      
    }else if(this.comboCadastro == "responsavel"){
      
      let responsavelModel:ResponsavelModel = new ResponsavelModel("", this.formPrincipal.get("responsavel").value ,"N","");

      this.cadastroService.createUpdateResponsavel(responsavelModel).subscribe( response => {
        if(response.sucesso){
          this.toastr.success('Criado com sucesso', 'Cadastro')
        }else{
          this.toastr.error('Erro ao criar', 'Cadastro')
        }
      }, errorresponse => {
        console.log(errorresponse)
          this.toastr.error('Erro conexao backend\nContate o suporte.','Cadastro');
      
      })

      this.comboCadastro == "responsavel";
      this.getValue()
    }

  }

  public delete(id:string, desc:string, criterio: string){

    if(criterio == "tipo"){

      let tipoModel:TipoModel = new TipoModel(id, desc ,"S","");

        this.cadastroService.createUpdateTipo(tipoModel).subscribe( response => {
          if(response.sucesso){
            this.toastr.success('Excluido com sucesso', 'Cadastro')
          }else{
            this.toastr.error('Erro ao excluir', 'Cadastro')
          }
        }, errorresponse => {
          console.log(errorresponse)
            this.toastr.error('Erro conexao backend\nContate o suporte.','Cadastro');
        
        })
        this.getValue()

    } else if(criterio == "motivo"){
    
      let motivoModel:MotivoModel = new MotivoModel(id, desc ,"S","", "");

        this.cadastroService.createUpdateMotivo(motivoModel).subscribe( response => {
          if(response.sucesso){
            this.toastr.success('Excluido com sucesso', 'Cadastro')
          }else{
            this.toastr.error('Erro ao excluir', 'Cadastro')
          }
        }, errorresponse => {
          console.log(errorresponse)
            this.toastr.error('Erro conexao backend\nContate o suporte.','Cadastro');
        
        })

        this.listMotivos()

    }else if(criterio == "responsavel"){
    
      let responsavelModel:ResponsavelModel = new ResponsavelModel(id, desc ,"S","");

        this.cadastroService.createUpdateResponsavel(responsavelModel).subscribe( response => {
          if(response.sucesso){
            this.toastr.success('Excluido com sucesso', 'Cadastro')
          }else{
            this.toastr.error('Erro ao excluir', 'Cadastro')
          }
        }, errorresponse => {
          console.log(errorresponse)
            this.toastr.error('Erro conexao backend\nContate o suporte.','Cadastro');
        
        })

        this.getValue()
    }

  }
  public abreOpcao():void{

  }

}
