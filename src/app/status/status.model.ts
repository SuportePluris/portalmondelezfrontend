export class StatusModel {
/*
    constructor(
        public ds_status: string,
        public dh_registro: string,
        public dh_atualizacao: string,
    ){}
*/
    constructor(
        public stat_id_cd_status: string,
        public status_ds_descricaostatus: string,
        public status_dh_registro: string,
        public status_in_inativo: string,
        public status_id_cd_usuarioatualizacao: string,
        public status_dh_atualizacao: string
    ){}
}