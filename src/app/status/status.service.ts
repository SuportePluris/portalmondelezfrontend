import { HttpClient, HttpParams } from '@angular/common/http'
import { Injectable } from '@angular/core'
import { environment } from '../../environments/environment'
import { Observable } from 'rxjs';
import { StatusModel } from './status.model';
import { ShipmentModel } from '../shipment.model';
import { EmbarqueModel } from '../agendamento/embarque.model'

@Injectable()
export class  StatusService{

    //objeto Mockado referente aos embarques pelo status de agendamento -- Alterar para o retorno do WS

    
    public embarques: EmbarqueModel[] = [
        {skud_id_cd_shipment:"123",emba_id_cd_embarque:"1",  skud_ds_pesobruto:'', skud_ds_pesoliquido:'',skud_ds_volumecubico:'',skud_ds_valor:'',skud_nr_quantidade:'',emba_id_cd_shipment:'',emba_ds_veiculo:'',emba_ds_tipoveiculo:'',emba_in_transportadoraconfirmada:'',emba_ds_tipocarga:'',emba_ds_cod_transportadora:'',emba_dh_leadtime:'18/05/2021', stat_ds_status:'', emba_id_cd_codcliente:'',clie_ds_nomecliente:'', emba_id_cd_status: '', retDelivery:[],skud_dh_registro: '', multistop:''},
        {skud_id_cd_shipment:"123",emba_id_cd_embarque:"1",  skud_ds_pesobruto:'', skud_ds_pesoliquido:'',skud_ds_volumecubico:'',skud_ds_valor:'',skud_nr_quantidade:'',emba_id_cd_shipment:'',emba_ds_veiculo:'',emba_ds_tipoveiculo:'',emba_in_transportadoraconfirmada:'',emba_ds_tipocarga:'',emba_ds_cod_transportadora:'',emba_dh_leadtime:'18/05/2021', stat_ds_status:'', emba_id_cd_codcliente:'',clie_ds_nomecliente:'', emba_id_cd_status: '', retDelivery:[],skud_dh_registro: '', multistop:''},
        {skud_id_cd_shipment:"123",emba_id_cd_embarque:"1",  skud_ds_pesobruto:'', skud_ds_pesoliquido:'',skud_ds_volumecubico:'',skud_ds_valor:'',skud_nr_quantidade:'',emba_id_cd_shipment:'',emba_ds_veiculo:'',emba_ds_tipoveiculo:'',emba_in_transportadoraconfirmada:'',emba_ds_tipocarga:'',emba_ds_cod_transportadora:'',emba_dh_leadtime:'18/05/2021', stat_ds_status:'', emba_id_cd_codcliente:'',clie_ds_nomecliente:'', emba_id_cd_status: '', retDelivery:[],skud_dh_registro: '', multistop:''}
    ]

    //objeto Mockado referente ao status para apresentar no titulo -- Alterar para o retorno do WS
    public status: StatusModel = {status_ds_descricaostatus : 'Aguardando Agendamento', status_dh_registro: '', status_dh_atualizacao: '', stat_id_cd_status: '', status_in_inativo: '', status_id_cd_usuarioatualizacao:''}
    private url: string = environment.urlBackend;
    private environmentGetliststatus: String = environment.getliststatus;
    private environmentGetlistembarquestatus: string = environment.getEmbarquesByStatus
    constructor(
        private http: HttpClient,
    ){ }

    public buscarAgendamentosStatus(idStatus:string):Observable<any>{
        //Executar aqui chamada http para buscar os dados referente ao status e PA
        const tokenString = localStorage.getItem('access_token');
        const token = JSON.parse(tokenString);

        const idFuncionario = localStorage.getItem('idFuncionario')


        const headers = {
            'Authorization' : 'Bearer ' + token.access_token,
            'Content-Type' : 'application/json'
        }

        let json = {
            'emba_id_cd_status' : idStatus,
            'idFuncionario' : idFuncionario
        }
        return this.http.post(this.url + this.environmentGetlistembarquestatus, json, { headers : headers});
    }

    public buscarDadosStatus(idStatus:number):StatusModel{
        //Executar aqui chamada http para buscar os dados referente ao status especifico
        return this.status
    }

    public getListStatus(inativo: string):Promise<any> {

        const tokenString = localStorage.getItem('access_token');
        const token = JSON.parse(tokenString);

        const headers = {
            'Authorization' : 'Bearer ' + token.access_token,
            'Content-Type' : 'application/json'
        }
        

        return this.http.get(this.url +this.environmentGetliststatus+"/"+inativo, { headers : headers})
        .toPromise().then((resposta:any) => resposta);

        
        
    }
    
}