import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { StatusService } from './status.service'
import { StatusModel } from './status.model';
import { ShipmentModel } from '../shipment.model';
import { EmbarqueModel } from '../agendamento/embarque.model';
@Component({
  selector: 'app-status',
  templateUrl: './status.component.html',
  styleUrls: ['./status.component.css'],
  providers:[ StatusService ]
})
export class StatusComponent implements OnInit {
  public status: string
  public mostrarStatus : boolean = false
  public lstEmbarques: EmbarqueModel[] = []
  public intervalo: any
  constructor(private route: ActivatedRoute, private statusService: StatusService) { }

  ngOnInit(): void {
    //recupera o parametro da URL para fazer a busca
    

    this.route.params.subscribe((parametro: any)=>{
      //chama o service para pegar o título da página
      //this.status = this.statusService.buscarDadosStatus(parametro)
      // console.log(parametro)


      // this.intervalo = setInterval(()=>{
      //   this.buscarAgendamentosStatus(parametro.idstatus)
      // },5000)
      this.buscarAgendamentosStatus(parametro.idstatus)
      
        
    })
  }

  ngOnDestroy(): void{
    if(this.intervalo){
      clearInterval(this.intervalo)
    }
    
  }

  public buscarAgendamentosStatus(idStatus: string){
    //busca lista de shipments do status selecionado para apresentar em tela
    this.statusService.buscarAgendamentosStatus(idStatus).subscribe( response => {
      this.lstEmbarques = response;
      console.log(this.lstEmbarques)
      if(this.lstEmbarques.length > 0){
        this.status = this.lstEmbarques[0]["stat_ds_status"]
      }
      

    })
  }

  

}
