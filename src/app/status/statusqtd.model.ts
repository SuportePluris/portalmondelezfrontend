export class StatusModelQtd {
    /*
        constructor(
            public ds_status: string,
            public dh_registro: string,
            public dh_atualizacao: string,
        ){}
    */
        constructor(
            public stat_id_cd_status: string,
            public stat_ds_status: string,
            public qtd: number
        ){}
    }