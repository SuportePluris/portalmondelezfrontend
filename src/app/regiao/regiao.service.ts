import { Injectable } from '@angular/core'
import { environment } from 'src/environments/environment'
import { HttpClient} from '@angular/common/http'
import { Observable } from 'rxjs';

@Injectable()

export class RegiaoService{
    private url: string = environment.urlBackend;
    private environmentGetListRegiao: string = environment.getlistregiao;

    constructor(
        private http: HttpClient
    ){ }

    public getLstRegiao(inativo : string):Promise<any> {

        const tokenString = localStorage.getItem('access_token');
        const token = JSON.parse(tokenString);

        const headers = {
            'Authorization' : 'Bearer ' + token.access_token,
            'Content-Type' : 'application/json'
        }
        
        return this.http.get(this.url +this.environmentGetListRegiao+"/"+inativo, { headers : headers})
        .toPromise().then((resposta:any) => resposta);
        
    }
}