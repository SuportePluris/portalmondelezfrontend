export class RegiaoModel {

    constructor(
        public id_regi_cd_regiao: string,
        public regi_ds_regiao: string,
        public regi_in_inativo: string,
        public regi_dh_registro: string
    ){}
    
}