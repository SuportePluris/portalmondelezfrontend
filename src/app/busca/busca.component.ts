import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { BuscaService } from './busca.service'
import { BuscaModel } from './busca.model'
import { Router } from '@angular/router';
import { StatusModel } from '../status/status.model'
import { StatusService } from '../status/status.service'
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-busca',
  templateUrl: './busca.component.html',
  styleUrls: ['./busca.component.css'],
  providers: [BuscaService, StatusService]
})
export class BuscaComponent implements OnInit {

  constructor(
    public buscaService: BuscaService,
    public router: Router,
    private toastr: ToastrService,
    private statusService: StatusService
  ) {}

  ngOnInit(): void {
    this.getListStatus();
  }

  public lstBuscaModel: BuscaModel[]
  public lstStatusModel: StatusModel[] = []

  public formBusca: FormGroup = new FormGroup({
    'criterioBusca': new FormControl(null, Validators.required),
    'valorBusca': new FormControl(null, Validators.required),
    'statusBusca' : new FormControl(null),
  })

  public formResultado: FormGroup = new FormGroup({
  })

  public busca(){

    if(this.formBusca.get("criterioBusca").value == null || this.formBusca.get("valorBusca").value == null){
      return;
    
    }else{

      this.buscaService.getLstAgendamento(this.formBusca.get("criterioBusca").value, this.formBusca.get("valorBusca").value, this.formBusca.get("statusBusca").value)
        .then((retorno:BuscaModel[])=>{
          this.lstBuscaModel = retorno

          //DADOS DE TESTE
          //this.mock();
        
      }).catch((retorno:any) => {
        if(retorno.error.error_description.indexOf("Access token expired")>-1){
          alert("Sessão Expirada");
          this.router.navigate(['']);
        }               
    });
    }
  }

  public mock(){
    this.lstBuscaModel = [new BuscaModel(
      'skud_id_cd_shipment',
      '1',//'emba_id_cd_embarque',
      'skud_ds_pesobruto',
      'skud_ds_pesoliquido',
      'skud_ds_volumecubico',
      'skud_ds_valor',
      'skud_nr_quantidade',
      '123',//emba_id_cd_shipment',
      'emba_ds_veiculo',
      'emba_ds_tipoveiculo',
      'emba_in_transportadoraconfirmada',
      'emba_ds_tipocarga',
      'emba_ds_cod_transportadora',
      '01/06/2021',//emba_dh_leadtime',
      'stat_ds_status',
      'emba_id_cd_codcliente',
      'Hipermercado Carrefour'//clie_ds_nomecliente'
    ),
    new BuscaModel(
      'skud_id_cd_shipment',
      '2',//'emba_id_cd_embarque',
      'skud_ds_pesobruto',
      'skud_ds_pesoliquido',
      'skud_ds_volumecubico',
      'skud_ds_valor',
      'skud_nr_quantidade',
      '456',//emba_id_cd_shipment',
      'emba_ds_veiculo',
      'emba_ds_tipoveiculo',
      'emba_in_transportadoraconfirmada',
      'emba_ds_tipocarga',
      'emba_ds_cod_transportadora',
      '01/06/2021',//emba_dh_leadtime',
      'stat_ds_status',
      'emba_id_cd_codcliente',
      'Hipermercado Carrefour'//clie_ds_nomecliente'
    )]
  }

  public abreOpcao(){
    alert(this.formResultado);
  }

  public getListStatus(){

    this.statusService.getListStatus('N').then((retorno:StatusModel[])=>{
      this.lstStatusModel = retorno;
      //console.log(this.lstStatusModel)
   
    }, errorresponse => {
      //console.log(errorresponse)
        this.toastr.error('Erro conexao backend\nContate o suporte.','COMBO STATUS');
    
    }).catch((retorno:any) => {
      if(retorno.error.error_description.indexOf("Access token expired")>-1){
        alert("Sessão Expirada");
        this.router.navigate(['']);
      }               
    });

  }
}
