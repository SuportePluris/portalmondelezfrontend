export class BuscaModel {

    constructor(
        public skud_id_cd_shipment: string,
        public emba_id_cd_embarque: string,
        public skud_ds_pesobruto: string,
        public skud_ds_pesoliquido: string,
        public skud_ds_volumecubico: string,
        public skud_ds_valor: string,
        public skud_nr_quantidade: string,
        public emba_id_cd_shipment: string,
        public emba_ds_veiculo: string,
        public emba_ds_tipoveiculo: string,
        public emba_in_transportadoraconfirmada: string,
        public emba_ds_tipocarga: string,
        public emba_ds_cod_transportadora: string,
        public emba_dh_leadtime: string,
        public stat_ds_status: string,
        public emba_id_cd_codcliente: string,
        public clie_ds_nomecliente: string
    ){}
}