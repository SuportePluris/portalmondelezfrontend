import { Injectable } from '@angular/core'
import { environment } from 'src/environments/environment'
import { HttpClient} from '@angular/common/http'

@Injectable()

export class BuscaService {

    private url: string = environment.urlBackend;
    private environmentBuscaListStatus: string = environment.buscaListStatus;

    constructor(
        private http: HttpClient
    ){ }

    public getLstAgendamento(criterioBusca: string, valorBusca:string, stausBusca:string):Promise<any> {

        const tokenString = localStorage.getItem('access_token');
        const token = JSON.parse(tokenString);

        const headers = {
            'Authorization' : 'Bearer ' + token.access_token,
            'Content-Type' : 'application/json'
        }

        valorBusca = valorBusca.replace("/","-").replace("/","-");

        return this.http.get(this.url +this.environmentBuscaListStatus+"/"+criterioBusca+"/"+valorBusca+"/"+stausBusca, { headers : headers})
        .toPromise().then((resposta:any) => resposta);
        
    }
}
