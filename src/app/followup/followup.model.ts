export class FollowUpModel {

    constructor(
        public foup_id_cd_followup: string,
        public foup_ds_followup: string,
        public foup_dh_registro: string,
        public foup_in_inativo: string,
        public foup_id_cd_usuarioatualizacao: string,
        public foup_dh_atualizacao: string,
        public foup_id_cd_usuariocriacao: string,
        public stat_id_cd_status: string,
        public stat_ds_status: string
    ){}
    
}
