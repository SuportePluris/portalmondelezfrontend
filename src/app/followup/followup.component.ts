import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { NotificationService } from '../notification.service';
import { FollowUpService } from '../followup/followup.service';
import { FollowUpModel } from './followup.model';
import { StatusModel } from '../status/status.model';
import { StatusService } from '../status/status.service';

@Component({
  selector: 'app-followup',
  templateUrl: './followup.component.html',
  styleUrls: ['./followup.component.css'],
  providers: [FollowUpService, StatusService]
})
export class FollowupComponent implements OnInit {

  constructor(
    private toastr: ToastrService,
    private router: Router,
    private followupService: FollowUpService,
    private notifyService : NotificationService,
    private statusService: StatusService

  ) { 
    
  }


  panelOpenState = false;
  
  public isPiscar : boolean = false
  public mostrarTabela : boolean = false
  public lstFollowups: FollowUpModel[] = []
  public loginError : boolean = false
  public msgError : string = "";
  public listStatus: StatusModel[];

  public formCadastro: FormGroup = new FormGroup({
      'idFollowup': new FormControl(null, null),
      'nome': new FormControl(null, null),
      'inativo': new FormControl(null, null),
      'stat_id_cd_status':new FormControl(null, null)
  })


  ngOnInit(): void {
    this.preencheLista();
    this.preencheStatus();
  }

  public preencheStatus(){

    this.statusService.getListStatus('N').then((retorno:StatusModel[])=>{
      this.listStatus = retorno;

    }, errorresponse => {
      console.log(errorresponse)
        this.toastr.error('Erro conexao backend\nContate o suporte.','COMBO STATUS');
    
    }).catch((retorno:any) => {
      console.log(retorno);
      if(retorno.error.error_description.indexOf("Access token expired")>-1){
        alert("Sessão Expirada");
        this.router.navigate(['']);
      }               
    });
  }


  public preencheLista():void{

    this.mostrarTabela = false;

    this.followupService.getFollowup().then((retorno:FollowUpModel[])=>{
        this.mostrarTabela = true;
        this.lstFollowups = retorno

        console.log(this.lstFollowups)
       
      }).catch((retorno:any) => {
        console.log(retorno);
        if(retorno.error.error_description.indexOf("Access token expired")>-1){
          alert("Sessão Expirada");
          this.router.navigate(['']);
        }               
    });

  }


  public limparCadastro(){
      this.formCadastro.reset()
   }

   

   //PREENCHE NO FORM OS DADOS PARA ATUALIZAR
   public editar( foup_id_cd_followup: string,
                  foup_ds_followup: string,
                  foup_in_inativo: string,
                  stat_id_cd_status: string
                  ){

        //this.isPiscar = true;
        // LIMPAR CLASS
        let element = document.getElementById('idBodyCadastro')
        if(!element.classList.contains('fa-blink')){
          element.classList.add('fa-blink'); 
        }else{
          element.classList.remove('fa-blink'); 
          if(!element.classList.contains('fa-blink')){
            element.classList.add('fa-blink'); 
          }else{
            element.classList.remove('fa-blink');  
          }
        }

        let inativoAuxiliar:boolean = false ;
        if(foup_in_inativo.trim() == "SIM"){
           inativoAuxiliar = true
        }

        this.formCadastro.setValue({
          idFollowup: foup_id_cd_followup, 
          nome: foup_ds_followup.trim(),
          inativo : inativoAuxiliar,
          stat_id_cd_status: stat_id_cd_status
        })

        this.toastr.warning('Modo de edição.','CADASTRO');

      }


      
  public criarFollowup(){

    let inativoAux = "N";
    if(this.formCadastro.value.inativo == true){
      inativoAux = "S"
    }
    
    //console.log(localStorage.getItem('idFuncionario'));
    let followupModel : FollowUpModel = new FollowUpModel(
      this.formCadastro.value.idFollowup, //tran_id_cd_transportadora
      this.formCadastro.value.nome,                                //tran_nm_nometransportadora
      '',     //tran_dh_registro
      inativoAux,     //tran_in_inativo
      localStorage.getItem('idFuncionario'),        //tran_id_cd_usuarioatualizacao
      '', //tran_dh_atualizacao
      localStorage.getItem('idFuncionario'),
      this.formCadastro.value.stat_id_cd_status,
      ""
    )



    this.followupService.createUpdateFollowup(followupModel).subscribe( response => {
              
      if(response.sucesso == true){
    
        this.preencheLista();
        this.limparCadastro(); 

        this.toastr.success('Transportadora cadastrada.','CADASTRO')
        //this.msgError='Usuário cadastrado.';
                  
      }else{
        
        if(response.processo == "validacao"){
          this.toastr.error('Erro Transportadora não cadastrada\nFavor preencher os campos:\n'+response.msg,'CADASTRO')
          //this.msgError='Erro Usuário não cadastrado\nFavor preencher os campos\n: '+response.msg;
          this.loginError = true;
        }else{
          this.toastr.error('Erro Transportadora não cadastrada\n'+response.msg,'CADASTRO')
          //this.msgError='Erro Usuário não cadastrado: '+response.msg;
          this.loginError = true;
        }

      }

    }, errorresponse => {
        this.toastr.error('Erro ao cadastrar Transportadora','CADASTRO');
        //this.msgError='Erro Usuário não cadastrado';
        this.loginError = true;

    }) 
                    
  }

}
