import { Injectable } from '@angular/core'
import { environment } from 'src/environments/environment'
import { HttpClient} from '@angular/common/http'
import { Observable } from 'rxjs';
import { FollowUpModel } from './followup.model';

@Injectable()

export class FollowUpService {

    public dataAtual = new Date();
    private url: string = environment.urlBackend;
    private environmentGetFollowup: string = environment.getFollowups;
    private environmentPostCreateUpdateFollowu: string = environment.postCreateUpdateFollowup;
    
    constructor(
        private http: HttpClient
    ){ }

    public getFollowup():Promise<any> {

        
        const tokenString = localStorage.getItem('access_token');
        const token = JSON.parse(tokenString);

        const headers = {
            'Authorization' : 'Bearer ' + token.access_token,
            'Content-Type' : 'application/json'
        }
       
        return this.http.get(this.url +this.environmentGetFollowup+'/null', { headers : headers})
        .toPromise().then((resposta:any) => resposta);
        
    }
   
    public createUpdateFollowup(followup : FollowUpModel):Observable<any> {

        const tokenString = localStorage.getItem('access_token');
        const token = JSON.parse(tokenString);

        let json = { 
                    "foup_id_cd_followup" : followup.foup_id_cd_followup,
                    "foup_ds_followup" : followup.foup_ds_followup,
                    "foup_dh_registro" : followup.foup_dh_registro,
                    "foup_in_inativo" : followup.foup_in_inativo,
                    "foup_id_cd_usuarioatualizacao" : followup.foup_id_cd_usuarioatualizacao,
                    "foup_dh_atualizacao" : followup.foup_dh_atualizacao,
                    "foup_id_cd_usuariocriacao" : followup.foup_id_cd_usuariocriacao,
                    "stat_id_cd_status": followup.stat_id_cd_status
                }

        const headers = {
            'Authorization' : 'Bearer ' + token.access_token,
            'Content-Type' : 'application/json'
        }
        
        return this.http.post(this.url + this.environmentPostCreateUpdateFollowu, json, { headers : headers});

    }

    


    
}
