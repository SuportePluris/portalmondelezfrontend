import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { TransportadoraModel } from './transportadora.model';
import { TransportadoraService } from './transportadora.service';
import { ToastrService } from 'ngx-toastr';
import { NotificationService } from '../notification.service';

@Component({
  selector: 'app-transportadora',
  templateUrl: './transportadora.component.html',
  styleUrls: ['./transportadora.component.css'],
  providers: [TransportadoraService]
})
export class TransportadoraComponent implements OnInit {

  constructor(
    private toastr: ToastrService,
    private router: Router,
    private transportadoraService: TransportadoraService,
    private notifyService : NotificationService

  ) { 
    
  }


  panelOpenState = false;
  
  public isPiscar : boolean = false
  public mostrarTabela : boolean = false
  public lstTransportadoras: TransportadoraModel[] = []
  public loginError : boolean = false
  public msgError : string = "";

  public formCadastro: FormGroup = new FormGroup({
      'idTransportadora': new FormControl(null, null),
      'nome': new FormControl(null, null),
      'codigo': new FormControl(null, null),
      'inativo': new FormControl(null, null),
      'cnpj' :   new FormControl(null, null)
  })


  ngOnInit(): void {
    this.preencheLista();
  }


  public preencheLista():void{

    this.mostrarTabela = false;

    this.transportadoraService.getTransportadora().then((retorno:TransportadoraModel[])=>{
        this.mostrarTabela = true;
        this.lstTransportadoras = retorno

        console.log(retorno)
       
      }).catch((retorno:any) => {
        
        if(retorno.error.error_description.indexOf("Access token expired")>-1){
          alert("Sessão Expirada");
          this.router.navigate(['']);
        }               
    });

  }


  public limparCadastro(){
      this.formCadastro.reset()
   }

   

   //PREENCHE NO FORM OS DADOS PARA ATUALIZAR
   public editar( tran_id_cd_transportadora: string, 
                  tran_ds_codigotransportadora: string, 
                  tran_nm_nometransportadora: string,
                  tran_in_inativo: string,
                  tran_ds_cnpj: string
                  ){

        //this.isPiscar = true;
        // LIMPAR CLASS
        console.log(tran_ds_cnpj)
        let element = document.getElementById('idBodyCadastro')
        if(!element.classList.contains('fa-blink')){
          element.classList.add('fa-blink'); 
        }else{
          element.classList.remove('fa-blink'); 
          if(!element.classList.contains('fa-blink')){
            element.classList.add('fa-blink'); 
          }else{
            element.classList.remove('fa-blink');  
          }
        }

        let inativoAuxiliar:boolean = false ;
        if(tran_in_inativo.trim() == "SIM"){
           inativoAuxiliar = true
        }

        this.formCadastro.setValue({

          idTransportadora: tran_id_cd_transportadora, 
          codigo: tran_ds_codigotransportadora,
          nome: tran_nm_nometransportadora,
          cnpj: tran_ds_cnpj,
          inativo : inativoAuxiliar


        })

        this.toastr.warning('Modo de edição.','CADASTRO');

      }


      
  public criarTransportadora(){

    let inativoAux = "N";
    if(this.formCadastro.value.inativo == true){
      inativoAux = "S"
    }
    
    
    //console.log(localStorage.getItem('idFuncionario'));
    let transportadoraModel : TransportadoraModel = new TransportadoraModel(
      this.formCadastro.value.idTransportadora, //tran_id_cd_transportadora
      this.formCadastro.value.nome,                                //tran_nm_nometransportadora
      this.formCadastro.value.codigo,      //tran_ds_codigotransportadora
      '',     //tran_dh_registro
      //this.formCadastro.value.tipoCarga,     //tran_ds_tipocargatransportadora
      inativoAux,     //tran_in_inativo
      localStorage.getItem('idFuncionario'),        //tran_id_cd_usuarioatualizacao
      '', //tran_dh_atualizacao
      this.formCadastro.value.cnpj

    )

    console.log(transportadoraModel)

    this.transportadoraService.createUpdateTransportadora(transportadoraModel).subscribe( response => {
              
      if(response.sucesso == true){
    
        this.preencheLista();
        this.limparCadastro(); 

        this.toastr.success('Transportadora cadastrada.','CADASTRO')
        //this.msgError='Usuário cadastrado.';
                  
      }else{
        
        if(response.processo == "validacao"){
          this.toastr.error('Erro Transportadora não cadastrada\nFavor preencher os campos:\n'+response.msg,'CADASTRO')
          //this.msgError='Erro Usuário não cadastrado\nFavor preencher os campos\n: '+response.msg;
          this.loginError = true;
        }else{
          this.toastr.error('Erro Transportadora não cadastrada\n'+response.msg,'CADASTRO')
          //this.msgError='Erro Usuário não cadastrado: '+response.msg;
          this.loginError = true;
        }

      }

    }, errorresponse => {
        this.toastr.error('Erro ao cadastrar Transportadora','CADASTRO');
        //this.msgError='Erro Usuário não cadastrado';
        this.loginError = true;

    }) 
                    
  }

}
