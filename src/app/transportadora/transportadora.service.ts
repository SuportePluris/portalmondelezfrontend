import { Injectable } from '@angular/core'
import { environment } from 'src/environments/environment'
import { HttpClient} from '@angular/common/http'
import { Observable } from 'rxjs';
import { TransportadoraModel } from './transportadora.model';

@Injectable()

export class TransportadoraService {

    public dataAtual = new Date();
    private url: string = environment.urlBackend;
    private environmentGetTransportadoras: string = environment.getTransportadora;
    private environmentPostCreateUpdateTransportadora: string = environment.postCreateUpdateTransportadora;
    private environmentGetEmbarqueTransportadoras: string = environment.getGetEmbarqueTransportadoras;
    private environmentGetListContatosTransportadora: string = environment.getGetListContatosTransportadora;
    
    
    
    constructor(
        private http: HttpClient
    ){ }

    public getTransportadora():Promise<any> {

        
        const tokenString = localStorage.getItem('access_token');
        const token = JSON.parse(tokenString);

        const headers = {
            'Authorization' : 'Bearer ' + token.access_token,
            'Content-Type' : 'application/json'
        }
        console.log(this.url +this.environmentGetTransportadoras+'/null')
        return this.http.get(this.url +this.environmentGetTransportadoras+'/null', { headers : headers})
        .toPromise().then((resposta:any) => resposta);
        
    }


    
    
    public getListContatosByCodTransportadora(codTransportadora : string):Promise<any> {

        
        const tokenString = localStorage.getItem('access_token');
        const token = JSON.parse(tokenString);

        const headers = {
            'Authorization' : 'Bearer ' + token.access_token,
            'Content-Type' : 'application/json'
        }
        
        return this.http.get(this.url +this.environmentGetListContatosTransportadora+'/' +codTransportadora, { headers : headers})
        .toPromise().then((resposta:any) => resposta);
        
    }

   
    public createUpdateTransportadora(transportadora : TransportadoraModel):Observable<any> {

        const tokenString = localStorage.getItem('access_token');
        const token = JSON.parse(tokenString);

        let json = { 
                    "tran_id_cd_transportadora" : transportadora.tran_id_cd_transportadora,
                    "tran_nm_nometransportadora" : transportadora.tran_nm_nometransportadora,
                    "tran_ds_codigotransportadora" : transportadora.tran_ds_codigotransportadora,
                    "tran_dh_registro" : transportadora.tran_dh_registro,
                    //s"tran_ds_tipocargatransportadora" : transportadora.tran_ds_tipocargatransportadora,
                    "tran_in_inativo" : transportadora.tran_in_inativo,
                    "tran_id_cd_usuarioatualizacao" : transportadora.tran_id_cd_usuarioatualizacao,
                    "tran_dh_atualizacao" : transportadora.tran_dh_atualizacao,
                    "tran_ds_cnpj": transportadora.tran_ds_cnpj
                }

        const headers = {
            'Authorization' : 'Bearer ' + token.access_token,
            'Content-Type' : 'application/json'
        }
        
        return this.http.post(this.url + this.environmentPostCreateUpdateTransportadora, json, { headers : headers});

    }


    
    public getEmbarqueTransportadora(codTransportadora : string ):Promise<any> {

        const tokenString = localStorage.getItem('access_token');
        const token = JSON.parse(tokenString);

        const headers = {
            'Authorization' : 'Bearer ' + token.access_token,
            'Content-Type' : 'application/json'
        }

        return this.http.get(this.url +this.environmentGetEmbarqueTransportadoras+'/'+ codTransportadora, { headers : headers})
        .toPromise().then((resposta:any) => resposta);
        
    }


    
}
