export class TransportadoraModel {

    constructor(
        public tran_id_cd_transportadora:string,
        public tran_nm_nometransportadora:string,
        public tran_ds_codigotransportadora:string,
        public tran_dh_registro:string,
        public tran_in_inativo:string,
        public tran_id_cd_usuarioatualizacao:string,
        public tran_dh_atualizacao:string,
        public tran_ds_cnpj:string,
    ){}


    
}
