import { RequestOptions }  from '@angular/http'
import { Injectable } from '@angular/core'
import { Transp } from './relatorios/transp.model'
import { CancelarPedido } from './relatorios/cancelar-pedido.model'
import { environment } from '../environments/environment'
import { HttpClient, HttpHeaders } from '@angular/common/http'
import { Observable } from 'rxjs'

@Injectable()
export class ArquivoService {
    
    constructor(public http:HttpClient){
        
    }

    private url: string = environment.urlBackend;
    public arquivoTransp: Transp[] = []
    public arquivoCancelarPedido: CancelarPedido[] = []
    
    public importarDadosArquivo(tipoArquivo: string): Observable<any>{

        let retorno:any;

            if(tipoArquivo === 'transp'){                
                
                retorno = this.chamarServico(this.arquivoTransp,'transp');
            
            } else if(tipoArquivo === 'pedidocancelado'){

                retorno = this.chamarServico(this.arquivoCancelarPedido, 'pedidocancelado')

            } else{
                //regra inválida
                alert("Regra Inválida : " + tipoArquivo);
            }
        
        return retorno;

    }

    //** MÉTODO PARA CHAMAR OS SERVIÇOS ENVIANDO O BEAN E O MÉTODO
    public chamarServico(beanArquivo: any, arquivoTipo: string): Observable<any>{

        const tokenString = localStorage.getItem('access_token');
        const token = JSON.parse(tokenString);

        const headers = {
            'Authorization' : 'Bearer ' + token.access_token,
            'Content-Type' : 'application/json'
        }       
        
        console.log(JSON.stringify(beanArquivo))       
        let json = JSON.stringify(beanArquivo)       
        
        if(arquivoTipo == 'transp') {
            return this.http.post(this.url + '/portalpluris/updatecodtranspbyimport/', json, { headers : headers})           
        } else if(arquivoTipo == 'pedidocancelado') {
            return this.http.post(this.url + '/portalpluris/updatecancelarpedidobyimport/', json, { headers : headers})           
        }        
    
    }

    public previaArquivo(arquivo: any, tipo: string, headers: string[]): any[] {

        // LOOPING NO OBJETO RECEBIDO COM O CONTEUDO DO ARQUIVO A SER IMPORTADO DE ACORDO COM O TIPO DE ARQUIVO
        let header = headers[0]
    
        // IDENTIFICAR O TIPO DE ARQUIVO A SER IMPORTADO
        if (tipo == 'transp') {
          this.arquivoTransp = []
          for (let i = 0; i < arquivo.length; i++) {
    
            // OBTER LINHA DO ARQUIVO
            let linha = arquivo[i]
            
            // PREENCHER BEAN DE ACORDO COM AS INFORMACOES DO ARQUIVO
            let dadosLinha: Transp = new Transp(linha["cod_transp"], linha["id_shipment"])

            if(dadosLinha.cod_transp != undefined && dadosLinha.id_shipment != undefined){
                this.arquivoTransp.push(dadosLinha)
            }          
    
          }

          return this.arquivoTransp    

        } else if(tipo == 'pedidocancelado') {

            this.arquivoCancelarPedido = []
            for (let i = 0; i < arquivo.length; i++) {
    
                // OBTER LINHA DO ARQUIVO
                let linha = arquivo[i]
                
                // PREENCHER BEAN DE ACORDO COM AS INFORMACOES DO ARQUIVO
                let dadosLinha: CancelarPedido = new CancelarPedido(linha["pedido_mondelez"])

                if(dadosLinha.pedido_mondelez != undefined) {
                    this.arquivoCancelarPedido.push(dadosLinha)
                }               
        
              }            

              return this.arquivoCancelarPedido    
        }
    
    }

}