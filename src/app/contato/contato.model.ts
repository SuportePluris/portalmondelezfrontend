export class ContatoModel {

    constructor(
        public cont_ds_tipocontato:string,
        public cont_nm_nomecontato:string,
        public cont_ds_emailcontato:string,
        public cont_ds_telefonecontato:string,
        public cont_ds_codigocontato:string
    ){}
}
