import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { ContatoModel } from '../contato/contato.model';
import { ContatoService } from './contato.service';

@Component({
  selector: 'app-contato',
  templateUrl: './contato.component.html',
  styleUrls: ['./contato.component.css'],
  providers: [ContatoService]
})
export class ContatoComponent implements OnInit {

  constructor(
    private contatoService: ContatoService,
    private toastr: ToastrService
  ) { }

  public msgError : string = "";
  public loginError : boolean = false

  ngOnInit(): void {
  }

  public formCadastro: FormGroup = new FormGroup({
    'nome': new FormControl(null, null),
    'telefone': new FormControl(null, null),
    'email': new FormControl(null, null),
    'tipocontato': new FormControl(null, null),
    'codigocontato': new FormControl(null, null)
  })

  public criarContato(){

    let contatoModel : ContatoModel = new ContatoModel(
      this.formCadastro.value.tipocontato,                          
      this.formCadastro.value.nome,      
      this.formCadastro.value.email,     
      this.formCadastro.value.telefone,      
      this.formCadastro.value.codigocontato 
    )

    if(this.formCadastro.value.codigocontato == null) {
      this.msgError = 'O campo Código de Contato é obrigatório'
    }

    if(this.formCadastro.value.tipocontato == null) {
      this.msgError = 'O campo Tipo de Contato é obrigatório'  
    }        

    if(this.formCadastro.value.email == null) {
      this.msgError = 'O campo E-mail é obrigatório'
    }   

    if(this.formCadastro.value.telefone == null) {
      this.msgError = 'O campo Telefone é obrigatório'
    }

    if(this.formCadastro.value.nome == null) {
      this.msgError = 'O campo Nome é obrigatório'
    }

    if(this.msgError == '') {
      this.contatoService.createUpdateContato(contatoModel).subscribe( response => {
              
        if(response.idErro == 0){
      
          this.limparCadastro()
  
          this.toastr.success('Sucesso (' + response.menssagem + ')','CADASTRO')
          this.msgError='Usuário cadastrado.'
                    
        }else{       
          this.toastr.error('Erro (' + response.msg + ')', 'CADASTRO')          
          this.loginError = true    
        }
  
      }, errorresponse => {
          this.toastr.error('Erro ao cadastrar Contato','CADASTRO')
          this.msgError='Erro Contato não cadastrado'
          this.loginError = true
      })  
    } else {
      this.toastr.error('Erro (' + this.msgError + ')', 'CADASTRO')
    }     

  }

  public limparCadastro(){
    this.formCadastro.reset()
  }


}
