import { Injectable } from '@angular/core'
import { HttpClient} from '@angular/common/http'
import { environment } from '../../environments/environment'
import { ContatoModel } from '../contato/contato.model';
import { Observable } from 'rxjs';
import { HttpClientModule } from '@angular/common/http'; 

@Injectable()
export class ContatoService { 

    private environmentPostCreateContato: string = environment.postCreateContato;
    private url: string = environment.urlBackend;

    constructor(
        private http: HttpClient
    ){ }

    public createUpdateContato(contato : ContatoModel):Observable<any> { 

        const tokenString = localStorage.getItem('access_token');
        const token = JSON.parse(tokenString);
        const idFuncionario = localStorage.getItem('idFuncionario');

        let json = { 
            "cont_ds_tipocontato" : contato.cont_ds_tipocontato,
            "cont_nm_nomecontato" : contato.cont_nm_nomecontato,
            "cont_ds_emailcontato" : contato.cont_ds_emailcontato,
            "cont_ds_telefonecontato" : contato.cont_ds_telefonecontato,
            "cont_ds_codigocontato" : contato.cont_ds_codigocontato,
            "user_id_cd_usuario" : idFuncionario
        }

        const headers = {
            'Authorization' : 'Bearer ' + token.access_token,
            'Content-Type' : 'application/json'
        }

        return this.http.post(this.url + this.environmentPostCreateContato, json, { headers : headers});

    }

}
