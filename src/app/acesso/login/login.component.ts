import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Autenticacao } from '../../autenticacao.service'
import { ToastrService } from 'ngx-toastr';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router'
import { NotificationService } from 'src/app/notification.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  @Output() public exibirPainel: EventEmitter<string> = new EventEmitter<string>()

  public formLogin: FormGroup = new FormGroup({
    'usuario': new FormControl(null, [Validators.required]),
    'senha': new FormControl(null, [Validators.required])
  })
  hide = true;
  
  
  constructor(
    private auth: Autenticacao, 
    private router: Router,
    private route: ActivatedRoute,
    private notifyService : NotificationService){
      
    }

  ngOnInit(): void {
    
  }
  
  public entrar(): void{
    //this.router.navigate(['/home/inicial_path']);
    this.auth.autenticar(this.formLogin.value.usuario, this.formLogin.value.senha);
  }  
}
