export class StatusModel {

    constructor(
        public status_id_cd_usuario:string,
        public status_dh_registro:string,
        public status_ds_descricaostatus:string,
        public status_in_inativo:string,
        public status_id_cd_usuarioatualizacao:string,
        public status_dh_atualizacao:string,
        public stat_id_cd_status: number
    ){}
}
