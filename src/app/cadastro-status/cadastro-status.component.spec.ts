import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CadastroStatusComponent } from './cadastro-status.component';

describe('CadastroStatusComponent', () => {
  let component: CadastroStatusComponent;
  let fixture: ComponentFixture<CadastroStatusComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CadastroStatusComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CadastroStatusComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
