import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { StatusModel } from './cadastro-status.model'
import { StatusService } from './status.service'
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-cadastro-status',
  templateUrl: './cadastro-status.component.html',
  styleUrls: ['./cadastro-status.component.css'],
  providers: [StatusService]
})
export class CadastroStatusComponent implements OnInit {

  public mostrarTabela : boolean = false
  public lstStatus: StatusModel[] = []
  public loginError: boolean = false

  constructor(
    private statusService: StatusService,
    private toastr: ToastrService,
  ) { }

  public formCadastro: FormGroup = new FormGroup({
    'idStatus': new FormControl(null, null),
    'descricao': new FormControl(null, null),
    'inativo': new FormControl(null, null) 
})

  ngOnInit(): void {
    this.preencheLista();
  }

  public preencheLista():void{
    this.mostrarTabela = false;

    this.statusService.getStatus().then((retorno:StatusModel[])=>{
      this.lstStatus = retorno
    })
    // this.usuariosService.getUsers().then((retorno:UsuarioModel[])=>{
    //     this.mostrarTabela = true;
    //     this.lstUsuarios = retorno
      
    //   }).catch((retorno:any) => {
    //     console.log(retorno);
    //     if(retorno.error.error_description.indexOf("Access token expired")>-1){
    //       alert("Sessão Expirada");
    //       this.router.navigate(['']);
    //     }               
    // });

  }

  public limparCadastro(){
    this.formCadastro.reset()
 }

 public criarStatus(){
    let inativo = 'N'
    if(this.formCadastro.value.inativo){
      inativo = 'S'
    }
   let statusModel: StatusModel = new StatusModel(
    localStorage.getItem('idFuncionario'),
    '',
    this.formCadastro.value.descricao,
    inativo,
    localStorage.getItem('idFuncionario'),
    '',
    this.formCadastro.value.idStatus
   )

    this.statusService.createUpdateStatus(statusModel).subscribe( response =>{
     
      if(response.sucesso){
        this.toastr.success('Status criado com sucesso','CADASTRO');
        this.limparCadastro()
      }else{
        this.toastr.success('Não foi possivel cadastrar o Status','CADASTRO');
        this.limparCadastro()
      }
      
      
    })
 }

 public editar(stat_id_cd_status:number,
              status_dh_atualizacao: string,
              status_dh_registro: string,
              status_ds_descricaostatus: string,
              status_in_inativo:string){
              
                let element = document.getElementById('idBodyCadastro')
                if(!element.classList.contains('fa-blink')){
                  element.classList.add('fa-blink'); 
                }else{
                  element.classList.remove('fa-blink'); 
                  if(!element.classList.contains('fa-blink')){
                    element.classList.add('fa-blink'); 
                  }else{
                    element.classList.remove('fa-blink');  
                  }
                }
                let inativoAux: boolean = false
                if(status_in_inativo == 'N'){
                  inativoAux = false
                }else{
                  inativoAux = true
                }

                this.formCadastro.setValue({
                  idStatus: stat_id_cd_status,
                  descricao: status_ds_descricaostatus,
                  inativo: inativoAux 
                })

                

                
              }

}
