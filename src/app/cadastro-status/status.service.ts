import { Injectable } from '@angular/core'
import { environment } from '../../environments/environment'
import { HttpClient} from '@angular/common/http'
import { Observable } from 'rxjs';
import { StatusModel } from './cadastro-status.model';

@Injectable()
export class StatusService {
    public dataAtual = new Date();
    private url: string = environment.urlBackend;
    private environmentGetStatus: string = environment.getListStatus;
    private environmentPostCreateUpdateStatus: string = environment.postCreateUpdateStatus;
    
    constructor(
        private http: HttpClient
    ){ }
    
    //DESCOMENTAR QUANDO PLUGAR NO BACKEND PRA RETORNAR UM OBSERVABLE
    public createUpdateStatus(status : StatusModel):Observable<any> {
    //public createUpdateStatus(status : StatusModel):any {
        //BUSCA OS TOKENS DE AUTENTICACAO DO BACKEND
        const tokenString = localStorage.getItem('access_token');
        const token = JSON.parse(tokenString);

        
        const headers = {
            'Authorization' : 'Bearer ' + token.access_token,
            'Content-Type' : 'application/json'
        }


        return this.http.post(this.url + this.environmentPostCreateUpdateStatus, status, { headers : headers});
    }

    public getStatus():Promise<any>{
        const tokenString = localStorage.getItem('access_token');
        const token = JSON.parse(tokenString);

        const headers = {
            'Authorization' : 'Bearer ' + token.access_token,
            'Content-Type' : 'application/json'
        }

        return this.http.get(this.url +this.environmentGetStatus + '/N', { headers : headers})
        .toPromise().then((resposta:any) => resposta);
    }

}