
import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import {CanalService} from './canal.service'
import { CanalModel } from './canal.model';
import { ToastrService } from 'ngx-toastr';
import { NotificationService } from '../notification.service';

@Component({
  selector: 'app-canal',
  templateUrl: './canal.component.html',
  styleUrls: ['./canal.component.css'],
  providers: [CanalService]
})
export class CanalComponent implements OnInit {

  constructor(
    private toastr: ToastrService,
    private router: Router,
    private canalService: CanalService,
    private notifyService : NotificationService

  ) { }


  
  panelOpenState = false;
  
  public isPiscar : boolean = false
  public mostrarTabela : boolean = false
  public lstCanal: CanalModel[] = []
  public loginError : boolean = false
  public msgError : string = "";

  public formCadastro: FormGroup = new FormGroup({
      'idCanal': new FormControl(null, null),
      'nome': new FormControl(null, null),
      'inativo': new FormControl(null, null)
  })


  ngOnInit(): void {
    this.preencheLista();
  }


  public preencheLista():void{

    this.mostrarTabela = false;

    this.canalService.geListCanal().then((retorno:CanalModel[])=>{
        this.mostrarTabela = true;
        this.lstCanal = retorno
       
      }).catch((retorno:any) => {
        
        if(retorno.error.error_description.indexOf("Access token expired")>-1){
          alert("Sessão Expirada");
          this.router.navigate(['']);
        }               
    });

  }


  public limparCadastro(){
      this.formCadastro.reset()
   }

   

   //PREENCHE NO FORM OS DADOS PARA ATUALIZAR
   public editar( cana_id_cd_canal: string, 
                  cana_ds_canal: string, 
                  cana_in_inativo: string
                  ){
                   

        //this.isPiscar = true;
        // LIMPAR CLASS
        let element = document.getElementById('idBodyCadastro')
        if(!element.classList.contains('fa-blink')){
          element.classList.add('fa-blink'); 
        }else{
          element.classList.remove('fa-blink'); 
          if(!element.classList.contains('fa-blink')){
            element.classList.add('fa-blink'); 
          }else{
            element.classList.remove('fa-blink');  
          }
        }

        let inativoAuxiliar:boolean = false ;
        if(cana_in_inativo.trim() == "SIM"){
           inativoAuxiliar = true
        }

        this.formCadastro.setValue({

          idCanal: cana_id_cd_canal, 
          nome: cana_ds_canal.trim(),
          inativo : inativoAuxiliar

        })

        this.toastr.warning('Modo de edição.','CADASTRO');

      }

  public criarCanal(){

    let inativoAux = "N";
    if(this.formCadastro.value.inativo == true){
      inativoAux = "S"
    }
    
    
    //console.log(localStorage.getItem('idFuncionario'));
    let canalModel : CanalModel = new CanalModel(
      this.formCadastro.value.idCanal, //cana_id_cd_canal
      this.formCadastro.value.nome,                                //cana_ds_canal
      inativoAux,      //cana_in_inativo
      '',     //cana_dh_registro
     
    )
     
    this.canalService.createUpdateCliente(canalModel).subscribe( response => {
              
      if(response.sucesso == true){
    
        this.preencheLista();
        this.limparCadastro(); 

        this.toastr.success('Canal cadastrado.','CADASTRO')
        //this.msgError='Usuário cadastrado.';
                  
      }else{
        
        if(response.processo == "validacao"){
          this.toastr.error('Erro Canal não cadastrado\nFavor preencher os campos:\n'+response.msg,'CADASTRO')
          //this.msgError='Erro Usuário não cadastrado\nFavor preencher os campos\n: '+response.msg;
          this.loginError = true;
        }else{
          this.toastr.error('Erro Canal não cadastrado\n'+response.msg,'CADASTRO')
          //this.msgError='Erro Usuário não cadastrado: '+response.msg;
          this.loginError = true;
        }

      }

    }, errorresponse => {
        this.toastr.error('Erro ao cadastrar Canal','CADASTRO');
        //this.msgError='Erro Usuário não cadastrado';
        this.loginError = true;

    }) 
                    
  }

}
