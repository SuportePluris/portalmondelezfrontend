import { Injectable } from '@angular/core'
import { environment } from 'src/environments/environment'
import { HttpClient} from '@angular/common/http'
import { Observable } from 'rxjs';
import { CanalModel } from './canal.model';

@Injectable()

export class CanalService {
    
    getTransportadora() {
      throw new Error('Method not implemented.');
    }

    public dataAtual = new Date();
    private url: string = environment.urlBackend;
    private environmentGetClientes: string = environment.getCliente;
    private environmentPostCreateUpdateCanal: string = environment.postCreateUpdateCanal;
    private environmentGeListtCanal: string = environment.getlistcanal;
    
    constructor(
        private http: HttpClient
    ){ }

    public getCliente():Promise<any> {

        const tokenString = localStorage.getItem('access_token');
        const token = JSON.parse(tokenString);

        const headers = {
            'Authorization' : 'Bearer ' + token.access_token,
            'Content-Type' : 'application/json'
        }

        return this.http.get(this.url +this.environmentGetClientes+"/null", { headers : headers})
        .toPromise().then((resposta:any) => resposta);
        
    }

    public geListCanal():Promise<any> {

        const tokenString = localStorage.getItem('access_token');
        const token = JSON.parse(tokenString);
        
        const headers = {
            'Authorization' : 'Bearer ' + token.access_token,
            'Content-Type' : 'application/json'
        }
        
        return this.http.get(this.url +this.environmentGeListtCanal+"/null/0", { headers : headers})
        .toPromise().then((resposta:any) => resposta);
        
    }
   
    public createUpdateCliente(cliente : CanalModel):Observable<any> {

        const tokenString = localStorage.getItem('access_token');
        const token = JSON.parse(tokenString);

        let json = { 
            "cana_id_cd_canal" : cliente.cana_id_cd_canal,
            "cana_ds_canal" : cliente.cana_ds_canal,
            "cana_in_inativo" : cliente.cana_in_inativo,
            "cana_dh_registro" : cliente.cana_dh_registro

        }

        const headers = {
            'Authorization' : 'Bearer ' + token.access_token,
            'Content-Type' : 'application/json'
        }
        
        return this.http.post(this.url + this.environmentPostCreateUpdateCanal, json, { headers : headers});

    }

    


    
}
