import { Routes } from '@angular/router'

import { AcessoComponent } from './acesso/acesso.component'
import { HomeComponent } from './home/home.component'

import { AutenticacaoGuard } from './autenticacao-guard.service'
import { InicialComponent } from './inicial/inicial.component'


import { AgendamentoComponent } from './agendamento/agendamento.component';

import { UsuarioComponent } from './usuario/usuario.component';
import { ClienteComponent } from './cliente/cliente.component';
import { TransportadoraComponent } from './transportadora/transportadora.component';
import { StatusComponent } from './status/status.component';
import { FollowupComponent } from './followup/followup.component';
import { CadastroStatusComponent } from './cadastro-status/cadastro-status.component'
import { RelatoriosComponent } from './relatorios/relatorios.component'
import { CanalComponent } from './canal/canal.component'

import {BuscaComponent } from './busca/busca.component'
import { CadastroCombosComponent } from './cadastro-combos/cadastro-combos.component'
import { ContatoComponent } from './contato/contato.component'

export const ROUTES: Routes = [
    { path: '', component: AcessoComponent },
    { path: 'home', component: HomeComponent,
        children: [
            {path : 'inicial_path' ,  component : InicialComponent},
            
            //AGENDAMENTOS
            {path : 'agendamento/:id_embarque' ,  component : AgendamentoComponent},

            //CADASTROS
            {path : 'usuarios' ,  component : UsuarioComponent},
            {path : 'clientes' ,  component : ClienteComponent},
            {path : 'transportadoras' ,  component : TransportadoraComponent},
            {path : 'status/:idstatus' ,  component : StatusComponent},
            {path : 'followups' ,  component : FollowupComponent},
            {path : 'cadastrostatus' ,  component : CadastroStatusComponent},
            {path : 'relatorios' ,  component : RelatoriosComponent},
            {path : 'cadastrocombo', component: CadastroCombosComponent},
            {path : 'contatos', component: ContatoComponent},

            //BUSCA
            {path : 'busca' ,  component : BuscaComponent},
            {path : 'canal' ,  component : CanalComponent},

        ]

    },
]