import { Injectable } from '@angular/core'
import { Router } from '@angular/router'
import { environment } from 'src/environments/environment'
import { Observable } from 'rxjs'
import { HttpClient, HttpParams } from '@angular/common/http'
import { NotificationService } from '../app/notification.service';
import { ToastrService } from 'ngx-toastr';

@Injectable()
export class Autenticacao {
    
    tokenURL: string = environment.urlBackend + environment.obterTokenURL
    loginURL: string = environment.urlBackend + environment.login
  
    clientID: string = environment.clientId
    clientSecret: string = environment.clientSecret
    errors: String[] = [""]

    public token_id: string
    
    constructor(
        private toastr: ToastrService, 
        private router: Router,
        private http: HttpClient,
        protected notification: NotificationService
    ){ }

    tentarLogar( userName: string, password: string) :  Observable<any>{
       
        //PARAMETROS QUE VAI NO BODY DO POST
        const params = new HttpParams()
            .set('username',  userName)
            .set('password', password)
            .set('grant_type', 'password')

        //HEADER PARA O POST                                
        const headers = {
            'Authorization' : 'Basic ' + btoa(`${this.clientID}:${this.clientSecret}`),
            'Content-Type' : 'application/x-www-form-urlencoded'
        }
       
        //return this.http.post(this.tokenURL, params.toString, { headers : headers});
        return this.http.post(this.tokenURL, params, { headers : headers});
        
    }   

    public autenticar(usuario: string, senha: string): void{

        /*console.log("entrou");
        this.router.navigate(['/home/empresa_path']);
        console.log("saiu");
        */
            
        this.tentarLogar(usuario, senha).subscribe(
            
            response => { //CASO SUCESSO

                localStorage.clear();

                //ARMAZENA O TOKEN NO BROWSER 
                const access_token = JSON.stringify(response);
                localStorage.setItem('access_token', access_token);

                this.storageLogin(usuario, senha).subscribe(
                  
                   
                    response => {
                        console.log(response);
                         if(response.user_id_cd_usuario > 0){

                            console.log(response);

                            //ARMAZENA INFORMAÇÕES DO USUÁRIO NO BROWSER 
                            localStorage.setItem('idFuncionario',   response.user_id_cd_usuario);
                            localStorage.setItem('nomeFuncionario', response.user_ds_nomeusuario);
                            localStorage.setItem('permissao', response.user_ds_permissionamento);
                            //localStorage.setItem('canal', response.user_id_cd_canal);
                            //localStorage.setItem('regiao', response.user_id_regi_cd_regiao);

                            //localStorage.setItem('dsCanal', response.cana_ds_canal);
                            //localStorage.setItem('dsRegiao', response.regi_ds_regiao);

                            //ABRIR A TELA INICIAL
                            this.router.navigate(['/home/inicial_path']);

                        }else{
                            this.toastr.error('Usuário/Senha incorreto(s) ou sem permissão.\n'+response.msg,'LOGIN')
                            //this.errors = ['Usuário/Senha incorreto(s) ou sem permissão.']
                            //this.notification.showError('',this.errors);
                        }                    
                    },errorResponse => {
                        this.toastr.error('Usuário e/ou Senha incorreto(s).\n'+response.msg,'LOGIN')
                        //this.errors = ['Usuário e/ou Senha incorreto(s)']
                        //this.notification.showError('',this.errors);
                    }
                )

            }, 
            errorResponse => { //CASO ERRO
                this.toastr.error('Erro ao conectar Backend.\nComunique suporte sistemas.','LOGIN')
                //this.errors = ['Usuário e/ou Senha incorreto(s)']
                //this.notification.showError('',this.errors);
            }
        )
    }

    public storageLogin( userName: string, password: string ) :  Observable<any>{
        
        const tokenString = localStorage.getItem('access_token');
        const token = JSON.parse(tokenString);
        console.log(password)
        //PARAMETROS QUE VAI NO BODY DO POST
         let json = { "user_ds_login" : userName,
                    "user_ds_senha" : password
                     }
        //HEADER PARA O POST                                
        const headers = {
            'Authorization' : 'Bearer ' + token.access_token,
            'Content-Type' : 'application/json'
        }
       
        //return this.http.post(this.tokenURL, params.toString, { headers : headers});
        return this.http.post(this.loginURL, json, { headers : headers});
        
    }   


    public autenticado(): boolean{
        if(this.token_id === undefined && localStorage.getItem('access_token') !== null){
            this.token_id = localStorage.getItem('access_token')
        }else if (this.token_id === undefined && localStorage.getItem('access_token') == null){
            this.router.navigate(['/'])
        }
        return this.token_id !== undefined
    }

    public sair(): void {

        localStorage.removeItem('access_token')
        this.token_id = undefined

        /*localStorage.removeItem('idFuncionario');
        localStorage.removeItem('nomeFuncionario');
        localStorage.removeItem('permissao');
        localStorage.removeItem('pa');*/
        
        
        localStorage.setItem('idFuncionario', "0");
        localStorage.setItem('nomeFuncionario', "");
        localStorage.setItem('permissao', "");
        localStorage.setItem('pa', "0");
        

        this.router.navigate([""]);

    }
    
}